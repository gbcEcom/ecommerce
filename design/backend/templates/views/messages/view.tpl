{capture name="mainbox"}

    {assign var="page_title" value=__("messages")}

    {capture name="sidebar"}
        {include file="views/messages/components/sidebar.tpl"}
    {/capture}

    {if empty($_REQUEST['mode']) || is_null($_REQUEST['mode'])}
        {assign var="conversation_with" value="{$message.firstname} {$message.lastname}"}
        {assign var="recipient_type" value="V"}
    {else}
        {assign var="conversation_with" value="{$message.firstname} {$message.lastname}"}
        {assign var="recipient_type" value="C"}
    {/if}

    <div class="message-box" style="background-color: #f9f9f9; padding: 15px; border: 1px solid #d9d9d9">
        <div class="message-header" style="border-bottom: 1px solid #eee">
            <div class="row-fluid">
                <div class="span6">
                    <h6 style="margin: 0px">Percakapan dengan {$conversation_with}</h6>
                </div>
                <div class="span6">
                    <small class="pull-right">{date('d F Y, H:i', $message.timestamp)}</small>
                </div>
            </div>
        </div>
        <div class="message-body" style="padding-top: 15px;">
            <div class="message-content">
                {if is_null($message.parent_id) || $message.parent_id != 0}
                    <small style="font-style: italic">{$conversation_with} Say:</small>
                    <pre>{strip_tags($message.parent_message)}</pre>
                {/if}
                {if empty($_REQUEST['mode'])}
                    <small style="font-style: italic">{$conversation_with} Say:</small>
                {else}
                    <small style="font-style: italic">{$message.sender} Say:</small>
                {/if}
                <pre>{strip_tags($message.message)}</pre>
            </div>
            <div class="reply-message">
                <form method="post" action="{"messages.send"|fn_url}" class="form">
                    <input type="hidden" name="parent_id" value="{$message.message_id}" />
                    <input type="hidden" name="sender_id" value="{$message.recipient_id}" />
                    <input type="hidden" name="recipient" value="{$message.from}" />
                    <input type="hidden" name="recipient_type" value="C" />
                    <div class="control-group">
                        <textarea class="cm-wysiwyg input-large" name="message" rows="5" placeholder="Ketik pesan..." required="required" autofocus="autofocus"></textarea>
                    </div>
                    <div class="controls">
                        <button class="btn btn-default" type="button" onclick="self.history.back(-1)">Kembali</button>
                        <button class="btn btn-primary pull-right" type="submit">Kirim</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

{/capture}

{include file="common/mainbox.tpl" title=$page_title sidebar=$smarty.capture.sidebar content=$smarty.capture.mainbox buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons content_id="messsages_view"}