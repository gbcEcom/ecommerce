<div class="sidebar-row" id="views">
    <h6>Menu</h6>
    <ul class="nav nav-list nav-messages">
        <li><a href="{"messages.inbox"|fn_url}"><i class="icon-inbox"></i>Inbox <small class="label label-info">{$count}</small></a></li>
        <li><a href="{"messages.sent"|fn_url}"><i class="icon-envelope"></i>Sent</a></li>
        <li><a href="#"><i class="icon-remove"></i>Trash</a></li>
    </ul>
</div>