{capture name="mainbox"}

    {assign var="page_title" value=__("messages")}

    {capture name="sidebar"}
        {include file="views/messages/components/sidebar.tpl"}
    {/capture}

    {include file="common/pagination.tpl"}
    <div class="message-box">
        <table class="table table-hover" style="font-size: 12px;">
            <thead>
            <tr style="background-color: #f9f9f9">
                {if $auth['user_type'] == 'A'}
                <th width="15%">From</th>
                {/if}
                <th width="15%">To</th>
                <th width="55%">Message</th>
                <th width="15%">Received</th>
            </tr>
            </thead>

            <tbody>
            {if count($messages)}
                {foreach from=$messages item="message"}
                    {assign var="msg_id" value=$message.message_id}
                    <tr class="" style="cursor: pointer" onclick="location.href='{"messages.view&mode=outbox&msg_id=$msg_id"|fn_url}';">
                        {if $auth['user_type'] == 'A'}
                        <td>{$message.sender}</td>
                        {/if}
                        <td>{$message.firstname} {$message.lastname}</td>
                        <td>{$message.message}</td>
                        <td>{date('F d, Y H:i', $message.timestamp)}</td>
                    </tr>
                {/foreach}
            {/if}
            </tbody>
        </table>
        {include file="common/pagination.tpl"}
    </div>

{/capture}

{include file="common/mainbox.tpl" title=$page_title sidebar=$smarty.capture.sidebar content=$smarty.capture.mainbox buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons content_id="messsages_inbox"}