{capture name="mainbox"}

    {include file="common/pagination.tpl"}
    <div class="message-box">
        <table class="table table-middle table-hover" style="font-size: 12px;">
            <thead>
            <tr style="background-color: #f9f9f9">
                <th width="15%">From</th>
                {if $auth['user_type'] == 'A'}
                <th width="15%">To</th>
                {/if}
                <th width="55%">Message</th>
                <th width="15%">Received</th>
            </tr>
            </thead>

            <tbody>
            {if count($messages)}
                {foreach from=$messages item="message"}
                    {assign var="msg_id" value=$message.message_id}
                    {if empty($message.is_read)}
                        <tr style="cursor: pointer; font-weight: bold" onclick="location.href='{"messages.view&msg_id=$msg_id"|fn_url}';">
                            <td>{$message.firstname} {$message.lastname}</td>
                            {if $auth['user_type'] == 'A'}
                            <td>{$message.recipient}</td>
                            {/if}
                            <td>{$message.message}</td>
                            <td>{date('F d, Y H:i', $message.timestamp)}</td>
                        </tr>
                    {else}
                        <tr class="" style="cursor: pointer" onclick="location.href='{"messages.view&msg_id=$msg_id"|fn_url}';">
                            <td>{$message.firstname}</td>
                            {if $auth['user_type'] == 'A'}
                            <td>{$message.recipient}</td>
                            {/if}
                            <td>{$message.message}</td>
                            <td>{date('F d, Y H:i', $message.timestamp)}</td>
                        </tr>
                    {/if}
                {/foreach}
            {/if}
            </tbody>
        </table>
        {include file="common/pagination.tpl"}
    </div>

    {capture name="sidebar"}
        {include file="views/messages/components/sidebar.tpl" count=$intInbox}
    {/capture}

{/capture}

{include file="common/mainbox.tpl" title=$page_title content=$smarty.capture.mainbox sidebar=$smarty.capture.sidebar buttons=$smarty.capture.buttons content_id="manage_orders"}