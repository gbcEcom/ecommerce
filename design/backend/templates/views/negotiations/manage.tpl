{capture name="mainbox"}

    {include file="common/pagination.tpl"}
    <table class="table table-middle table-striped table-hover" style="font-size: 12px;">
        <thead>
            <tr style="background-color: #e5e5e5">
                <th width="10%">Tanggal</th>
                <th width="15%">Customer</th>
                <th width="20%">Produk</th>
                <th width="10%">Harga</th>
                <th width="10%">Nego</th>
                <th width="5%">Status</th>
                <th width="10%"></th>
            </tr>
        </thead>
        <tbody>
            {if count($histories)}
                {foreach from=$histories item="nego"}
                    {assign var="product_id" value=$nego.product_id}
                    {assign var="nego_id" value=$nego.nego_id}
                    <tr>
                        <td>{date('d M Y', $nego.timestamp)}</td>
                        <td>{$nego.b_firstname}</td>
                        <td><a href="{"products.update&product_id=$product_id"|fn_url}" target="_blank">{$nego.product}</a></td>
                        <td>Rp. {number_format($nego.price,0,',','.')}</td>
                        <td>Rp. {number_format($nego.amount,0,',','.')}</td>
                        <td><span class="label label-info">{if $nego.status == 0} Menunggu {elseif $nego.status == 1} Diterima {else} Ditolak {/if}</span></td>
                        <td style="text-align: right">
                            <a href="{"negotiations.process&nego_id=`$nego_id`&status=2"|fn_url}" class="btn btn-danger btn-small {($nego.status) ? 'disabled' : ''}">TOLAK</a>
                            <a href="{"negotiations.process&nego_id=`$nego_id`&status=1"|fn_url}" class="btn btn-success btn-small {($nego.status) ? 'disabled' : ''}">TERIMA</a>
                        </td>
                    </tr>
                {/foreach}
            {else}
                <tr>
                    <td colspan="7">Tidak ada sejarah tawar produk</td>
                </tr>
            {/if}
        </tbody>
    </table>
    {include file="common/pagination.tpl"}

{/capture}

{include file="common/mainbox.tpl" title="Nego Harga" content=$smarty.capture.mainbox sidebar=$smarty.capture.sidebar buttons=$smarty.capture.buttons content_id="manage_negotiations"}