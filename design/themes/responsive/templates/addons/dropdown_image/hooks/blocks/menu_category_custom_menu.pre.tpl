{assign var="dropdown_image" value=$item1.category_id|fn_get_dropdown_image_by_categ:$item1.company_id}
{assign var="cat_image" value=$item1.category_id|fn_get_image_pairs:'category':'M':true:true}

{if $cat_image.pair_id && $dropdown_image.custom_menu_settings == "Y" && $dropdown_image.dd_status == "Y"}
	{assign var="et_dd_menu" value="width:`$dropdown_image.menu_width`px; min-height:`$dropdown_image.menu_min_height`px;"}
{else}
	{assign var="et_dd_menu" value=""}
{/if}
{$et_dd_menu}