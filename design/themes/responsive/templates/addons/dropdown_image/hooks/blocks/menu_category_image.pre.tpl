{assign var="cat_image" value=$item1.category_id|fn_get_image_pairs:'category':'M':true:true}

{assign var="dropdown_image" value=$item1.category_id|fn_get_dropdown_image_by_categ:$item1.company_id}

{if $cat_image.pair_id}

	{if $dropdown_image.custom_menu_settings == "Y"}
		{assign var="offset" value="right:`$dropdown_image.offset_right`px; bottom:`$dropdown_image.offset_bottom`px;"}
	{else}
		{assign var="offset" value="right:0px; bottom:0px;"}
	{/if}

	{if $dropdown_image.dd_status == "Y"}
		<div class="dropdown-image">
			<div style="position: absolute;{$offset}">
			{if isset($dropdown_image.url)}
				<a href="{$dropdown_image.url}">
			{/if}
				{if $dropdown_image.image_width == 0}
					{assign var="image_width" value=""}
				{else}
					{assign var="image_width" value=$dropdown_image.image_width}
				{/if}

				{if $dropdown_image.image_height == 0}
					{assign var="image_height" value=""}
				{else}
					{assign var="image_height" value=$dropdown_image.image_height}
				{/if}

			    {include file="common/image.tpl" images=$cat_image.detailed image_width=$image_width image_height=$image_height}

			{if isset($dropdown_image.url)}
		    	</a>
		    {/if}
		    </div>
		</div>
	{/if}

{/if}