{if $addons.social_buttons.email_enable == "Y"  && $provider_settings.email.data}

<a id="opener_elm_email_sharing" 
class="cm-dialog-opener cm-dialog-auto-size email-sharing" 
data-ca-target-id="content_elm_email_sharing" 
rel="nofollow">
    <span>{__("sb_share")}</span>
    <i class="ty-icon-mail"></i>
</a>

{/if}


