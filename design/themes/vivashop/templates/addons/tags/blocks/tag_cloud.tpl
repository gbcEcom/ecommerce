{** block-description:tag_cloud **}

{if $items}
<div class="ty-tag-cloud">
    <ul>
    {foreach from=$items item="tag"}
        {$tag_name = $tag.tag|escape:url}
        <li style="
            float: left;
            background-color: #1BBC9B;
            padding-top: 2px;
            padding-bottom: 2px;
            padding-right: 4px;
            padding-left: 4px;
            text-transform: lowercase;
            margin: 2px;
            border-radius: 3px;
            ">
            <a style="color:white;" href="{"tags.view?tag=`$tag_name`"|fn_url}">
                {$tag.tag}
            </a>
        </li>
        
    {/foreach}
    </ul>
</div>
{/if}
