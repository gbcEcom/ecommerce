{assign var="dropdown_image" value=$item1.category_id|fn_get_dropdown_image_by_categ:$item1.company_id}
{assign var="cat_image" value=$item1.category_id|fn_get_image_pairs:'category':'M':true:true}

{assign var="et_dd_cols" value="4"}
{if $cat_image.pair_id && $dropdown_image.custom_menu_settings == "Y" && $dropdown_image.dd_status == "Y"}
	{if $et_dd_cols_viva}
		{if ($dropdown_image.menu_width >= 539) && ($dropdown_image.menu_width < 717)}
			{assign var="et_dd_cols" value="3"}
		{elseif ($dropdown_image.menu_width >= 361) && ($dropdown_image.menu_width < 539)}
			{assign var="et_dd_cols" value="2"}
		{elseif $dropdown_image.menu_width < 361}
			{assign var="et_dd_cols" value="1"}
		{/if}
	{else}
		{if ($dropdown_image.menu_width >= 473) && ($dropdown_image.menu_width < 637)}
			{assign var="et_dd_cols" value="3"}
		{elseif ($dropdown_image.menu_width >= 309) && ($dropdown_image.menu_width < 473)}
			{assign var="et_dd_cols" value="2"}
		{elseif $dropdown_image.menu_width < 309}
			{assign var="et_dd_cols" value="1"}
		{/if}
	{/if}
{/if}
{$et_dd_cols}