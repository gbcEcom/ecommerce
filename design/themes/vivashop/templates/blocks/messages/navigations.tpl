<div class="item1-wrapper">
    <a href="{"messages.inbox"|fn_url}" class="item1">Inbox {if !empty($intInbox)}<small class="ty-tags-label ty-float-right">({$intInbox})</small>{/if}</a>
</div>
<div class="item1-wrapper">
    <a href="{"messages.sent"|fn_url}" class="item1">Outbox</a>
</div>
<div class="item1-wrapper">
    <a href="{"messages.trash"|fn_url}" class="item1">Trash</a>
</div>