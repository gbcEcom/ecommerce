<div>
    <img src="http://localhost/gbc_bug/images/no_image.png" 
        style=" float: left;
                margin-right: 10px;
                margin-bottom: 10px;">
</div>
<div>
    <ul style="line-height: 1.5;">
        <li style="font-family: 'vs-icon';">
            <i class="vs-icon-shop-by-brand"> </i>
            <span>{$informasi.company}</span>
        </li>
        <li style="font-family: 'vs-icon';">
            <i class="vs-icon-location"> </i>
            {assign var=propinsi value=""|select_propinsi}
                {foreach from=$propinsi item=row}
                    {if $row.state_id == $informasi.state}
                        {assign var=kota value=$row.state}
                    {/if}
                {/foreach}
            <span>{$kota}</span>
        </li>
        <!--
        <li>
            <i class="vs-icon-mail"> </i>
            <span>{*$informasi.email*}</span>
        </li>
        -->
        <li style="font-family: 'vs-icon';">
            <i class="vs-icon-quick-view"> </i>
            <span>{$informasi.viewed} dilihat</span>
        </li>
        <li style="font-family: 'vs-icon';">
            <i class="vs-icon-info"> </i>
            <span>{$informasi.total_product} total produk</span>
        </li>
    </ul>
</div>
        
<div  style="clear: both">
    {if is_null($company_data.is_favorite) || empty($company_data.is_favorite) || $company_data.is_favorite == 0}
        {assign var="company_id" value=$company_data.company_id}
        <a href="{"companies.favorite?company_id=$company_id"|fn_url}" class="ty-btn ty-btn__primary" style="width: 97.4%;
    padding: 4px;
    font-size: 10px;
    font-weight: bold;
    font-family: 'vs-icon';
    padding-top: 7px;
    padding-bottom: 7px;
    margin-bottom: 10px;"><i class="vs-icon-wishlist" style="top:0"></i> Jadikan Favorit</a>
    {/if}
</div>