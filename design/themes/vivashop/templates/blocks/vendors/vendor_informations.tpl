{** block-description:block_vendor_information **}

<div class="">
    <div class="row-fluid">
        <div class="span8">
            <h1>
                <span style="color: #008000; font-size: 24px">{$company_data.company}</span>
            </h1>
        </div>
    </div>
    <div class="row-fluid shop-header-middle" style="padding: 10px 0px; border-top: 1px dotted #dedede; border-bottom: 1px dotted #dedede">
        <div class="span10">
            <div style="padding-top: 5px">
                <span style="margin-right: 20px"><i class="fa fa-clock-o"></i> Hari ini</span>
                <span style="margin-right: 20px"><i class="fa fa-map-marker"></i> Jakarta</span>
                <span style="margin-right: 20px"><i class="fa fa-home"></i> Hanya online</span>
                <span style="margin-right: 20px"><i class="fa fa-bank"></i> {date('F Y',$company_data.timestamp)}</span>
            </div>
        </div>
        <div class="span6 hidden-phone">
            <div style="float: right !important;">
                <a href="index.php?dispatch=messages.compose&company={$company_data.company_id}" class="ty-btn ty-btn__primary" style="padding: 4px; font-size: 10px; font-weight: bold; cursor: pointer"><i class="fa fa-envelope" style="top:0"></i> Kirim Pesan</a>
                {if is_null($company_data.is_favorite) || empty($company_data.is_favorite) || $company_data.is_favorite == 0}
                    {assign var="company_id" value=$company_data.company_id}
                    <a href="{"companies.favorite?company_id=$company_id"|fn_url}" class="ty-btn ty-btn__primary" style="padding: 4px; font-size: 10px; font-weight: bold"><i class="fa fa-heart" style="top:0"></i> Jadikan Favorit</a>
                {else}
                    <a href="#" class="ty-btn ty-btn__primary" style="padding: 4px; font-size: 10px; font-weight: bold"><i class="fa fa-heart" style="top:0"></i> Jadikan Favorit</a>
                {/if}

            </div>
        </div>
    </div>
    <div class="row-fluid shop-header-bottom hidden-phone" style="padding-top: 15px">
        <div class="span6">
            <div style="font-size: 12px;">{$company_data.company_description nofilter}</div>
        </div>
        <div class="span10" style="background-color: #f9f9f9; padding: 15px; border: 1px solid #dedede">
            <div class="row-fluid">
                <div class="span4" style="text-align: center">
                    <div style="font-size: 18px; color: #008000;">{$company_data.success_orders}</div>
                    <small>Transaksi Berhasil</small>
                </div>
                <div class="span4" style="text-align: center">
                    <div style="font-size: 18px; color: #008000;">{$company_data.product_sold}</div>
                    <small>Produk Terjual</small>
                </div>
                <div class="span4" style="text-align: center">
                    <div style="font-size: 18px; color: #008000;">{$company_data.total_products}</div>
                    <small>Total Produk</small>
                </div>
                <div class="span4" style="text-align: center">
                    <div style="font-size: 18px; color: #008000;">{(empty($company_data.follower)) ? '-' : $company_data.follower}</div>
                    <div><i class="fa fa-heart"></i></div>
                </div>
            </div>
        </div>
    </div>
</div>