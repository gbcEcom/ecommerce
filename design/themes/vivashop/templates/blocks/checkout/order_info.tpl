{if $completed_steps.step_two}
    <div class="ty-order-info">
        {assign var="profile_fields" value="I"|fn_get_profile_fields}
        {if $profile_fields.S}
            {foreach from=$negara item=row}
                {if $row.code == $order_pengiriman.s_country}
                    {assign var=s_negara value=$row.country}
                {/if}
            {/foreach}
            {foreach from=$propinsi item=row}
                {if $row.state_id == $order_pengiriman.s_state}
                    {assign var=s_propinsi value=$row.state}
                {/if}
            {/foreach}
            {foreach from=$kabupaten item=row}
                {if $row.id == $order_pengiriman.s_kabupaten}
                    {assign var=s_kabupaten value=$row.nama_kabupaten}
                {/if}
            {/foreach}
            {foreach from=$kota item=row}
                {if $row.id == $order_pengiriman.s_city}
                    {assign var=s_city value=$row.nama_kota}
                {/if}
            {/foreach}
            <h4 class="ty-order-info__title">{__("shipping_address")}:</h4>
               {$order_pengiriman.s_firstname}<br/>  
               {$s_negara}, {$s_propinsi}<br/>
               {$s_kabupaten}, {$s_city} <br/>
               {$order_pengiriman.s_address}
            
            <hr class="shipping-adress__delim" />
        {/if}

        {if !$cart.shipping_failed && !empty($cart.chosen_shipping) && $cart.shipping_required}
            <h4>{__("shipping_method")}:</h4>
            <ul id="tygh_shipping_method">
                {foreach from=$cart.chosen_shipping key="group_key" item="shipping_id"}
                    <li>{$product_groups[$group_key].shippings[$shipping_id].shipping}</li>
                {/foreach}
            </ul>
        {/if}
    </div>
{/if}
{assign var="block_wrap" value="checkout_order_info_`$block.snapping_id`_wrap" scope="parent"}
