
<div class="box" style="margin-top: 0px !important; margin-bottom: 15px">
    <div class="box-header">
        <div class="row-fluid">
            <div class="span8">
                <h4>Percakapan dengan <span style="color: #0092ef">{$message.company}</span></h4>
            </div>
            <div class="span8">
                <small class="ty-float-right">{date('d/m/Y H:i', $message.timestamp)}</small>
            </div>
        </div>
    </div>
    <div class="box-body">
        <!-- Sender Information -->
        <div class="" style="border-bottom: 1px solid #dedede; margin-bottom: 20px; display: none">
            <div>From: {$message.company}</div>
            <div style="display: none">Belanja Lebih Mudah dan Murah, Cuma di RB Fashion !</div>
        </div>

        <small style="font-style: italic; color: #888">{$message.company} Say:</small>
        <div class="message" style="border: 1px solid #dedede; padding: 15px; border-radius: 5px; background-color: #f9f9f9; margin-bottom: 20px;">
            {$message.message}
        </div>

        <form method="post" action="{"messages.send"|fn_url}" class="form">
            <input type="hidden" name="recipient" value="{$message.company_id}" />
            <input type="hidden" name="recipient_type" value="V" />
            <input type="hidden" name="parent_id" value="{$message.message_id}" />
            <div class="ty-control-group">
                <textarea class="ty-input-text-full ty-input-textarea" name="message" rows="5" placeholder="Ketik pesan..." required="required" autofocus="autofocus"></textarea>
            </div>
            <div class="controls">
                <button class="ty-btn ty-btn__secondary" type="button" onclick="self.history.back(-1)">Kembali</button>
                <button class="ty-btn ty-btn__primary ty-float-right" type="submit">Kirim</button>
            </div>
        </form>
    </div>
</div>