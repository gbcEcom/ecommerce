{assign var="_title" value="Inbox"}
<div class="message-box">
    <table class="ty-table" style="margin-top: 0px !important;">
        <thead>
        <tr>
            <th width="25%">Pengirim</th>
            <th width="55%">Pesan</th>
            <th width="20%">Tanggal</th>
        </tr>
        </thead>

        <tbody>
        {if count($messages)}
            {foreach from=$messages item="message"}
                {if empty($message.is_read)}
                    <tr style="cursor: pointer; font-weight: bold" onclick="location.href='index.php?dispatch=messages.view&msg_id={$message.message_id}';">
                        <td>{$message.company}</td>
                        <td>{$message.message}</td>
                        <td>{date('F d, Y H:i', $message.timestamp)}</td>
                    </tr>
                {else}
                    <tr class="" style="cursor: pointer" onclick="location.href='index.php?dispatch=messages.view&msg_id={$message.message_id}';">
                        <td>{$message.message_id} {$message.company}</td>
                        <td>{$message.message}</td>
                        <td>{date('F d, Y H:i', $message.timestamp)}</td>
                    </tr>
                {/if}
            {/foreach}
        {else}
            <tr>
                <td colspan="3">Data pesan belum ada !</td>
            </tr>
        {/if}
        </tbody>
    </table>
</div>
{capture name="mainbox_title"}{$_title}{/capture}