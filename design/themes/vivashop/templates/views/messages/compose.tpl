{assign var="_title" value="Inbox"}
<div class="box" style="margin-bottom: 15px">
    <div class="box-header">
        <h4>Percakapan dengan <span style="color: #0092ef">{$recipient.company}</span></h4>
    </div>
    <div class="box-body">
        <form method="post" action="{"messages.send"|fn_url}" class="form">
            <input type="hidden" name="recipient" value="{$recipient.company_id}" />
            <input type="hidden" name="recipient_type" value="{$recipient.type}" />
            <div class="ty-control-group">
                <textarea class="ty-input-text-full ty-input-textarea" name="message" rows="5" placeholder="Ketik pesan..." required="required" autofocus="autofocus"></textarea>
            </div>
            <div class="controls">
                <button class="ty-btn ty-btn__secondary" type="button">Kembali</button>
                <button class="ty-btn ty-btn__primary ty-float-right" type="submit">Kirim</button>
            </div>
        </form>
    </div>
</div>

{capture name="mainbox_title"}{$_title}{/capture}