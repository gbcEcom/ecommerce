{assign var="_title" value="Inbox"}
<div class="message-box" style="margin-bottom: 15px">
    <table class="ty-table" style="margin-top: 0px !important;">
        <thead>
        <tr>
            <th width="25%">Penerima</th>
            <th width="55%">Pesan</th>
            <th width="20%">Tanggal</th>
        </tr>
        </thead>

        <tbody>
        {if count($messages)}
            {foreach from=$messages item="message"}
                <tr style="cursor: pointer">
                    <td>{$message.company}</td>
                    <td>{$message.message}</td>
                    <td>{date('F d, Y H:i', $message.timestamp)}</td>
                </tr>
            {/foreach}
        {else}
            <tr>
                <td colspan="3">Data pesan belum ada !</td>
            </tr>
        {/if}
        </tbody>
    </table>
</div>

{capture name="mainbox_title"}{$_title}{/capture}