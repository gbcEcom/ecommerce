{foreach from=$negara item=row}
    {if $row.code == $order_pengiriman.s_country}
        {assign var=s_negara value=$row.country}
    {/if}
{/foreach}
{foreach from=$propinsi item=row}
    {if $row.state_id == $order_pengiriman.s_state}
        {assign var=s_propinsi value=$row.state}
    {/if}
{/foreach}
{foreach from=$kabupaten item=row}
    {if $row.id == $order_pengiriman.s_kabupaten}
        {assign var=s_kabupaten value=$row.nama_kabupaten}
    {/if}
{/foreach}
{foreach from=$kota item=row}
    {if $row.id == $order_pengiriman.s_kabupaten}
        {assign var=s_city value=$row.nama_kota}
    {/if}
{/foreach}
<ul>
    <li>{$order_pengiriman.s_firstname}</li>
    <li>{$s_negara}, {$s_propinsi}</li>
    <li>{$s_kabupaten}, {$s_city}</li>
    <li>{$order_pengiriman.s_address}</li>
</ul>
