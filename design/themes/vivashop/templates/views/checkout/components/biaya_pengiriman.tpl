<div class="ty-cart-content__top-buttons clearfix"  id="contentKurir" style="display:none;">
    <input type="hidden" value="{$user_data.s_city}" id="tujuan">
    <div class="ty-float-left ty-cart-content__left-buttons">
        <select class="ty-btn ty-btn__secondary " style="height: 34px" id="company">
            <option value="" selected="selected">- pilih kurir -</option>
            {foreach from=$company item=row}
                <option value="{$row.id}">{$row.perusahaan}</option>
            {/foreach}
        </select>
    </div>
    <div class="ty-float-left ty-cart-content__left-buttons">
        <select class="ty-btn ty-btn__secondary " style="height: 34px" id="tipe" >
            <option value="" selected="selected">- tipe pengiriman -</option>
            {foreach from=$tipe item=row}
                <option value="{$row.id}" class="{$row.company_id}">{$row.tipe}</option>
            {/foreach}
        </select>
    </div>
    <div class="ty-float-left ty-cart-content__left-buttons">
        <div class="ty-btn ty-btn__secondary">
            <!-- kun -->
            <input type="hidden" id="beratBarang" value="{$berat|ceil}"/>
            berat <b id="weight">{$berat|ceil}</b> kg 
        </div>
    </div>
    <input type="hidden" value="{$user_id_kirim.user_id}" id="user_id" name="user_id">
    <div class="ty-float-right ty-cart-content__right-buttons">
        <a id="submit_kirim" onclick="biaya_kirim()" class="ty-btn ty-btn__primary">
            CEK Biaya Pengiriman
        </a>
    </div>
</div>
    {literal}
        <script>
            $(function (){
		if($('#tujuan').val() === '' || $('#company').val() === '' || $('#tipe').val() ===''){
		    $("button[id='step_three_but']").css("display","none");
		    $("div[id='step_four']").css("display","none");
		} 
                $('#tipe').chained('#company');
                $("#radioJasa").on("click", function (){
                    $("#shipping_rates_list").fadeOut();
                    $("#contentKurir").fadeIn();
$("button[id='step_three_but']").hide();
                });
                $("#radioManual").on("click", function (){
                    $("#contentKurir").fadeOut();
                    $("#shipping_rates_list").fadeIn();
                    $("button[id='step_three_but']").show();
                });
            });
            
            function biaya_kirim()
            {
                var company = $('#company').val();
                var tipe = $('#tipe').val();
                var tujuan = $('#tujuan').val();
                var berat = $('#beratBarang').val();
                var user_id = $('#user_id').val();
                        
                //alert( company + '\n' + tipe + '\n' + tujuan + '\n' + berat );
                
                if(company === '' || tipe === '' || tujuan === '' ){
                    alert("gagal");
                }else{
	     	    $("button[id='step_three_but']").show();
                    $("#container_kirim").show();
                    
                    if($('#company').val() == 4){
                        var gambar = "http://localhost/gbc_bug/images/jne.gif";
                    } else {
                        var gambar = "http://localhost/gbc_bug/images/tiki.gif";
                    }
                    
                    $.ajax({
                       type: 'GET',
                       url: fn_url('checkout.select_biaya'),
                       dataType: 'json',
                       data: {
                            'company' : company ,
                            'tipe' : tipe,
                            'tujuan' : tujuan,
                            'berat' : berat,
                            'user_id' : user_id
                       },
                       success: function (data) {
		           //alert(JSON.stringify(data));
                           console.log(data.result);
                           $(".text_kurir").text($('#company option:selected').text() +" "+ $('#tipe option:selected').text()).formatCurrency({decimalSymbol:',',roundToDecimalPlace:0,symbol:'Rp '});
                           $("#text_tujuan").text(data.result.nama_kota);
                           $("#text_waktu").text(data.result.estimasi);
                           $("#text_barang").text(berat);
                           $("#text_kirim").text(data.result.biaya_pengiriman).formatCurrency({decimalSymbol:',',roundToDecimalPlace:0,symbol:'Rp '});
                           $('#text_gambar').attr('src', gambar);
                           $('#text_total_kirim').text(berat * data.result.biaya_pengiriman).formatCurrency({decimalSymbol:',',roundToDecimalPlace:0,symbol:'Rp '});
                        }
                    });
                }
                return false;
            }
            
            biaya_manual = function ()
            {
                var user_id = $('#user_id').val();
                $("#container_kirim").hide();
                //alert(manual + user_id);
                $.ajax({
                    type: 'GET',
                    url: fn_url('checkout.biaya_manual'),
                    data:{
                        'user_id' : user_id
                    },
                    success: function (data) {
                        //alert(data);
                    }
                });
            }
        </script>
    {/literal}
           
    <div id="container_kirim" style="display: none; margin-top:25px;">
        <table width="35%">
            <tr>
                <th colspan="2"><h3>Anda Memakai Jasa Kurir : <span class="text_kurir"></span></h3></th>
                <th></th>
            </tr>
            <tr>
                <th colspan="2"><img id="text_gambar" style="width: 100px;" src=""></th>
                <th></th>
            </tr>
            <tr>
                <td>Tujuan :</td>
                <td><span id="text_tujuan"></span></td>
            </tr>
            <tr>
                <td>Estimasi Waktu :</td>
                <td><span id="text_waktu"></span> hari</td>
            </tr>
            <tr>
                <td>Berat Barang :</td>
                <td><span id="text_barang"></span>kg</td>
            </tr>
            <tr>
                <td>Biaya Kirim/kg :</td>
                <td><span id="text_kirim"></span> <a target="_blank" style="text-decoration: underline;color: darkorange" href="http://www.jne.co.id/"><i class="text_kurir"></i></a></td>
            </tr>
            <tr>
                <td>Total Biaya Pengiriman :</td>
                <td><span id="text_total_kirim"></span></td>
            </tr>
        </table>
    </div>
        <div class="biaya_kirim" style="padding:20px;padding-bottom: 0px;display: none;"></div>
        <div class="text" style="padding:20px;display: none; padding-top: 0px;"><b>Biaya Pengiriman: </b><b class="number"></b></div>
        <p class="rule_pengiriman" style="
            /* padding: 0 20px; */
            background-color: red;
            color: white;
            padding: 5px 10px;
            text-align: center;
            text-transform: uppercase;
            display: none;">

            {__("delivery_times_text")}
        </p>
        
        
