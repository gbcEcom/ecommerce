{hook name="checkout:notes"}
	<div class="ty-customer-notes">
	    <p class="ty-customer-notes__title">{__("type_comments_here")}</p>
            <input type="hidden" value="{$user_id_kirim.user_id}" id="user_id" name="user_id">
	    <textarea class="ty-customer-notes__text cm-focus" name="customer_notes" cols="60" rows="3">{$cart.notes}</textarea>
	</div>
{/hook}
