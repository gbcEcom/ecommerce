<div class="brand-wrapper clearfix" style="padding: 10px; background-color: #f9f9f9; margin-bottom: 15px">
    <div class="advance-options-wrapper ty-float-left clearfix">
        <span class="meta-company" style="padding-right: 15px"><b>Toko:</b> {$product.company_name}</span>
        <span class="meta-stock" style="padding-right: 15px"><b>Stock:</b> {$product.amount} pcs</span>
        <span class="meta-stock"><b>Kode:</b> {$product.product_code} pcs</span>
    </div>
</div>

<div class="row-fluid" style="margin-bottom: 15px">
    <div class="span6">
        <div class="image-wrap">
            <img src="{$product.main_pair.detailed.http_image_path}" style="width: 282px; height: 171px" />
        </div>
    </div>
    <div class="span8">
        <div class="product-info" style="margin-bottom: 30px; max-width: 320px !important;">
            <div class="prices-container price-wrap clearfix product-detail-price">
                <div class="product-price" style="padding: 15px; text-align: center; font-size: 24px; background-color: #F5F5F5">
                    <span style="color: #2dcc70; font-weight: 600">Rp. {number_format($product.price,0,',','.')}</span>
                </div>
            </div>
        </div>
        <section>
            <form method="post" action="{"negotiations.submit"|fn_url}">
                <input type="hidden" name="company_id" value="{$product.company_id}" />
                <input type="hidden" name="product_id" value="{$product.product_id}" />
                <div class="ty-control-group">
                    <input style="padding: 8px" type="number" class="ty-input-text-medium" name="amount" placeholder="Tawar harga anda" />
                </div>
                <div class="controls">
                    <button type="submit" class="ty-btn ty-btn__primary">Tawar</button>
                </div>
            </form>
        </section>
    </div>
</div>

{capture name="mainbox_title"}<span>Tawar {$product.product}</span>{/capture}