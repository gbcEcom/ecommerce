<table class="ty-table">
    <thead>
        <tr>
            <th width="15%">Tanggal</th>
            <th width="25%">Produk</th>
            <th width="15%">Harga</th>
            <th width="15%">Nego</th>
            <th width="10%">Status</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        {if count($histories)}
            {foreach from=$histories item="nego"}
                {assign var="product_id" value=$nego.product_id}
                {assign var="nego_id" value=$nego.nego_id}
                <tr>
                    <td>{date('d M Y', $nego.timestamp)}</td>
                    <td>{$nego.product}</td>
                    <td>Rp. {number_format($nego.price,0,',','.')}</td>
                    <td>Rp. {number_format($nego.amount,0,',','.')}</td>
                    <td>{$nego.label}</td>
                    <td style="text-align: center">
                        {if $nego.status == 1}
                            <a href="{"products.view&product_id=$product_id&nego_to_buy=$nego_id"|fn_url}" class="ty-btn ty-btn__secondary">BELI</a>
                        {else}
                            <a href="{"negotiations.tawar&product_id=$product_id"|fn_url}" class="ty-btn ty-btn__secondary">NEGO ULANG</a>
                        {/if}
                    </td>
                </tr>
            {/foreach}
        {else}
            <tr>
                <td colspan="6">Tidak ada sejarah tawar produk</td>
            </tr>
        {/if}
    </tbody>
</table>

{capture name="mainbox_title"}<span>History Nego Produk</span>{/capture}