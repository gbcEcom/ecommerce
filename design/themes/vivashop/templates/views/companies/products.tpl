{assign var="_title" value=$category_data.category|default:__("vendor_products")}

<div id="products_search_{$block.block_id}">
    {*if $subcategories or $category_data.description || $category_data.main_pair}
        {math equation="ceil(n/c)" assign="rows" n=$subcategories|count c=$columns|default:"2"}
        {split data=$subcategories size=$rows assign="splitted_subcategories"}

    {if $category_data.description && $category_data.description != ""}
        <div class="ty-wysiwyg-content ty-mb-s">{$category_data.description nofilter}</div>
    {/if}

        {if $subcategories}
            <ul class="subcategories clearfix">
            {foreach from=$splitted_subcategories item="ssubcateg"}
                {foreach from=$ssubcateg item=category name="ssubcateg"}
                    {if $category}
                        <li class="ty-subcategories__item">
                            <a href="{"companies.products?category_id=`$category.category_id`&company_id=$company_id"|fn_url}">
                            {if $category.main_pair}
                                {include file="common/image.tpl"
                                    show_detailed_link=false
                                    images=$category.main_pair
                                    no_ids=true
                                    image_id="category_image"
                                    image_width=$settings.Thumbnails.category_lists_thumbnail_width
                                    image_height=$settings.Thumbnails.category_lists_thumbnail_height
                                    class="ty-subcategories-img"
                                }
                            {/if}
                            {$category.category}
                            </a>
                        </li>
                    {/if}
                {/foreach}
            {/foreach}
            </ul>
        {/if}
    {/if*}
    {capture name="tabsbox"}
    <div id="content_products" class="{if $selected_section && $selected_section != "products"}hidden{/if}">
        {if $products}
            {assign var="title_extra" value="{__("products_found")}: `$search.total_items`"}
            {assign var="layouts" value=""|fn_get_products_views:false:0}
            {if $category_data.product_columns}
                {assign var="product_columns" value=$category_data.product_columns}
            {else}
                {assign var="product_columns" value=$settings.Appearance.columns_in_products_list}
            {/if}

            {if $layouts.$selected_layout.template}
                {include file="`$layouts.$selected_layout.template`" columns=$product_columns show_qty=true}
            {/if}
        {elseif !$subcategories}
            {hook name="products:search_results_no_matching_found"}
                <p class="ty-no-items">{__("text_no_matching_products_found")}</p>
            {/hook}
        {/if}
    </div>

    <div id="content_information" class="{if $selected_section && $selected_section != "products"}hidden{/if}">
        <div class="store-information">
            <div class="row-fluid">
                <div class="span4 store-owner">
                    <div style="border: 1px solid #EEEEEE;">
                        <img class="img-rounded" style="overflow: hidden; width: 98%; height: 90%" src="http://www.bassboathq.com/wp-content/themes/simply-responsive-cp/images/no-thumb-250.jpg" />
                    </div>
                </div>
                <div class="span12">
                    <div style="border: 1px solid #EEEEEE; padding: 10px 15px; line-height: 24px">
                        <h4 style="border-bottom: 1px dotted #dedede">Dukungan Pengiriman</h4>
                        <div class="store-meta">
                            <table class="table" style="margin-top: 10px !important; width: 100%; margin-bottom: 10px">
                                <tr>
                                    <td width="20%" style="background-color: #f9f9f9" class="center"><img src="images/shippings/kurir-jne.png" /></td>
                                    <td width="80%">
                                        <span style="padding-right: 15px"><i class="fa fa-check"></i> Reguler</span>
                                        <span><i class="fa fa-check"></i> YES</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <h4 style="border-bottom: 1px dotted #dedede">Dukungan Pembayaran</h4>
                        <div class="store-meta" style="padding: 10px 0px;">
                            <div style="display: table-cell; height: 30px; padding-right: 15px">
                                <img style="padding: 5px 10px; border: 1px solid #EEEEEE;" src="images/payments/bank-bca.png" />
                            </div>
                            <div style="display: table-cell; height: 30px; padding-right: 15px">
                                <img style="padding: 5px 10px; border: 1px solid #EEEEEE;" src="images/payments/bank-mandiri.png" />
                            </div>
                        </div>
                        <h4 style="border-bottom: 1px dotted #dedede">Lokasi Toko</h4>
                        <div class="store-meta">
                            <h5>{$company_data.company}</h5>
                            <div>{$company_data.address}</div>
                            <div><i class="fa fa-envelope" style="padding-right: 10px"></i>{$company_data.email}</div>
                            <div><i class="fa fa-phone" style="padding-right: 10px"></i>{$company_data.phone}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {hook name="companies:tabs"}
    {/hook}
    {/capture}

    {include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox active_tab=$smarty.request.selected_section}
<!--products_search_{$block.block_id}--></div>

{hook name="products:search_results_mainbox_title"}
{capture name="mainbox_title"}<span class="ty-mainbox-title__left">{$_title}</span><span class="ty-mainbox-title__right" id="products_search_total_found_{$block.block_id}">{$title_extra nofilter}<!--products_search_total_found_{$block.block_id}--></span>{/capture}
{/hook}