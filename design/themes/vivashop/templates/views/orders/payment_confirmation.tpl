<div class="vs-mainbox-general clearfix" style="background-color: #e5e5e5; margin-bottom: 15px;">
    <h1 class="vs-mainbox-title" style="padding-left: 15px !important;">Payment Confirmation</h1>
    <div class="vs-mainbox-body" style="padding: 0px 15px 15px 15px">
        <div class="account">
            <form id="PaymentConfirmation2" method="post" action="{"orders.do_confirmation"|fn_url}" enctype="multipart/form-data">
                <div class="ty-control-group">
                    <label class="ty-control-group__label cm-required">No Order</label>
                    <input id="OrderId" class="ty-input-text-full" type="text" name="order_id" placeholder="Tulis no order transaksi/faktur belanja anda" required="required" autofocus="autofocus" />
                </div>
                <div class="control-group">
                    <label class="control-label cm-required">Nama Pemilik Rekening</label>
                    <input class="ty-input-text-full" type="text" name="account_name" placeholder="Tulis nama pemilik rekening pembayaran" required="required" />
                </div>
                <div class="row-fluid control-group">
                    <div class="span8">
                        <label class="control-label cm-required">Pembayaran dari bank</label>
                        <select name="from_bank" class="ty-input-text-full">
                            <option value="bca">Bank BCA</option>
                            <option value="mandiri">Bank Mandiri</option>
                        </select>
                    </div>
                    <div class="span8">
                        <label class="control-label cm-required">Ke Bank</label>
                        <select name="to_bank" class="ty-input-text-full">
                            <option value="bca">Bank BCA</option>
                            <option value="mandiri">Bank Mandiri</option>
                        </select>
                    </div>
                </div>
                <div class="row-fluid control-group">
                    <div class="span8">
                        <label class="control-label cm-required">Tanggal Transfer</label>
                        <div class="controls">
                            <select name="tgl" class="input-text-short">
                                {for $tgl=1 to 31 step 1}
                                    <option value="{$tgl}">{$tgl}</option>
                                {/for}
                            </select>
                            <select name="bln" class="input-text-medium">
                                {foreach from=$arrBln key=x item=bln}
                                    <option value="{$x}">{$bln}</option>
                                {/foreach}
                            </select>
                            <select name="thn" class="input-text-medium">
                                <option value="{date('Y')}">{date('Y')}</option>
                            </select>
                        </div>
                    </div>
                    <div class="span8">
                        <label class="control-label cm-required">Jumlah Transfer</label>
                        <input class="ty-input-text-full" type="text" name="amount" placeholder="misal: 500000" required="required" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label cm-required">Bukti Transfer</label>
                    <input class="input-text" type="file" name="payment_receipt" />
                </div>
                <div class="buttons-container left">
                    <span class="button-submit button-wrap-left"><span class="button-submit button-wrap-right"><input style="padding: 3px 10px; background-color: #0098d1; border: 1px solid #006699; font-size: 12px; color: #ffffff" type="submit" value="Konfirmasi"></span></span>
                </div>
            </form>
        </div>
    </div>
</div>