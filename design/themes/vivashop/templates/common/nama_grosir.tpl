{if "MULTIVENDOR"|fn_allowed_for && ($company_name || $company_id) && $settings.Vendors.display_vendor == "Y"}
    <div class="{if !$capture_options_vs_qty} product-list-field{/if}">
        <label style="padding: 6px 0;
                    width: 20%;
                    font-family: monospace;
                    font-size: 11px;">
            {__("vendor")}:</label>
        <span class="ty-control-group__item"><a style="font-family: monospace;
                font-weight: bolder;
                font-size: 11px;" href="{"companies.products?company_id=`$company_id`"|fn_url}">{if $company_name}
        {$company_name|substr:0:25} 
        {else}{$company_id|fn_get_company_name}{/if}</a></span>
    </div>
{/if}