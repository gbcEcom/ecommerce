{assign var=negara value=""|select_negara}
{foreach from=$negara item=row}
    {if $row.code == $user_data.b_country} 
        {assign var=b_negara value=$row.country}
    {/if}
{/foreach}
{assign var=propinsi value=""|select_propinsi}
{foreach from=$propinsi item=row}
    {if $row.state_id == $user_data.b_state} 
        {assign var=b_propinsi value=$row.state}
    {/if}
{/foreach}
{assign var=kabupaten value=""|select_kabupaten}
{foreach from=$kabupaten item=row}
    {if $row.id == $user_data.b_kabupaten} 
        {assign var=b_kabupaten value=$row.nama_kabupaten}
    {/if}
{/foreach}
{assign var=kota value=""|select_kota}
{foreach from=$kota item=row}
    {if $row.id == $user_data.b_city} 
        {assign var=b_kota value=$row.nama_kota}
    {/if}
{/foreach}

<h3> Alamat Tagih:</h3>
<p>
    First name: {$user_data.b_firstname} <br/>
    Last name: {$user_data.b_lastname} <br/>
    Phone: {$user_data.b_phone}<br/>
   Negara: {$b_negara}<br/>
   Propinsi: {$b_propinsi}<br/>
   Kabupaten :{$b_kabupaten}<br/>
   Kota: {$b_kota}<br/>
   Alamat: {$user_data.b_address}<br/>
   Zip/postal code: {$user_data.b_zipcode}<br/>
</p>