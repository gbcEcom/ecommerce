{assign var=negara value=""|select_negara}
{foreach from=$negara item=row}
    {if $row.code == $user_data.s_country} 
        {assign var=s_negara value=$row.country}
    {/if}
{/foreach}
{assign var=propinsi value=""|select_propinsi}
{foreach from=$propinsi item=row}
    {if $row.state_id == $user_data.s_state} 
        {assign var=s_propinsi value=$row.state}
    {/if}
{/foreach}
{assign var=kabupaten value=""|select_kabupaten}
{foreach from=$kabupaten item=row}
    {if $row.id == $user_data.s_kabupaten} 
        {assign var=s_kabupaten value=$row.nama_kabupaten}
    {/if}
{/foreach}
{assign var=kota value=""|select_kota}
{foreach from=$kota item=row}
    {if $row.id == $user_data.s_city} 
        {assign var=s_kota value=$row.nama_kota}
    {/if}
{/foreach}

<h3> Alamat Kirim:</h3>
<p>
    First name: {$user_data.s_firstname} <br/>
    Last name: {$user_data.s_lastname} <br/>
    Phone: {$user_data.s_phone}<br/>
   Negara: {$s_negara}<br/>
   Propinsi: {$s_propinsi}<br/>
   Kabupaten :{$s_kabupaten}<br/>
   Kota: {$s_kota}<br/>
   Alamat: {$user_data.s_address}<br/>
   Zip/postal code: {$user_data.s_zipcode}<br/>
</p>