<?php
/**
 * Created by PhpStorm.
 * User: kusnendi.muhamad
 * Date: 6/9/2015
 * Time: 3:27 PM
 */

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if (empty($auth['user_id'])) {
    return array(CONTROLLER_STATUS_REDIRECT, "auth.login_form?return_url=" . urlencode(Registry::get('config.current_url')));
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if (!empty($auth['user_id']) && $mode == 'send'){

        //Get Request Parameter
        $param = $_REQUEST;

        $message = array (
            'message'   => $param['message'],
            'parent_id' => $param['parent_id'],
            'timestamp' => time()
        );

        if ($auth['user_type'] == 'V'){
            $message['from']   = $auth['user_id'];
            $message['user_id']= $auth['user_id'];
        } else {
            $message['from']   = $param['sender_id'];
            $message['user_id']= $param['sender_id'];
        }

        $recipient = array (
            'user_id'   => $param['recipient'],
            'type'      => $param['recipient_type']
        );

        fn_store_message($message, $recipient);
        $dispatch = "messages.sent";
    }

    return array(CONTROLLER_STATUS_OK, $dispatch);
}

//
// Show invoice on separate page
//
if ($mode == 'inbox'){

    $fields = array(
        'm.message_id',
        'm.from',
        'm.message',
        'm.parent_id',
        'm.user_id',
        'm.timestamp',
        'mr.type',
        'ms.is_read',
        'u.firstname',
        'u.lastname',
        'u.email',
        'c.company as recipient',
        'c.company_id',
        'c.logos'
    );

    $join = "LEFT JOIN ?:users u ON m.from = u.user_id ";
    $join.= "LEFT JOIN ?:companies c ON mr.user_id = c.company_id ";
    $join.= "LEFT JOIN ?:message_statuses ms ON m.message_id = ms.message_id AND mr.user_id = ms.user_id ";

    $condition = db_quote(" AND mr.type = ?s", 'V');

    if ($auth['user_type'] == 'V')
    {
        $condition .= db_quote(" AND mr.user_id = ?i", $auth['company_id']);
    }

    list($messages,$search) = fn_get_messages($_REQUEST, $fields, $join, $condition, 15);

    $countCondition = '';

    if ($auth['user_type'] == 'V')
    {
        $countCondition .= db_quote("AND mr.user_id = ?i", $auth['company_id']);
    }

    $count = fn_get_message_count($countCondition);

    $view = Registry::get('view');

    //Generate View
    $view->assign('messages', $messages);
    $view->assign('search', $search);
    $view->assign('intInbox', $count);
    $view->assign('page_title', 'Messages');

//
// Create Compose Message
//
}
elseif ($mode == 'compose'){
    $param = $_REQUEST;

    $recipient = array();

    if (!empty($param['company'])){
        $recipient = fn_get_company($param['company']);
        $recipient['type'] = 'V';
    }

    $view = Registry::get('view');

    $view->assign('recipient', $recipient);

//
// Create Compose Message
//
}
elseif ($mode == 'sent'){

    $fields = array(
        'm.message_id',
        'm.from',
        'm.message',
        'm.parent_id',
        'm.user_id',
        'm.timestamp',
        'mr.type',
        'u.firstname',
        'u.lastname',
        'u.email',
        'c.company as sender',
        'c.company_id as sender_id',
        'c.logos'
    );

    $join = "LEFT JOIN ?:users u ON mr.user_id = u.user_id ";
    $join.= "LEFT JOIN ?:companies c ON m.from = c.company_id ";

    $condition = db_quote(" AND mr.type = ?s", 'C');

    if ($auth['user_type'] == 'V')
    {
        $condition .= db_quote(" AND m.user_id = ?i", $auth['company_id']);
    }

    list($messages, $search) = fn_get_messages($_REQUEST, $fields, $join, $condition, 15);

    $view = Registry::get('view');
    $view->assign('messages', $messages);
    $view->assign('search', $search);

//
// Create Compose Message
//
}
elseif ($mode == 'view'){

    $condition = '';

    if (empty($param['mode']) || $param['mode'] != 'outbox'){
        if ($auth['user_type'] == 'V')
        {
            $condition .= db_quote("AND ?:mr.user_id = ?i", $auth['company_id']);
        }
    } else {
        //
    }

    $param = $_REQUEST;

    $view = Registry::get('view');

    list($message, $search) = fn_get_message_by_id($param, $condition);

    if (is_null($message['is_read']) || empty($message['is_read']) || $message['is_read'] == 0 && $auth['user_type'] == 'V'){
        //update message status
        $_data = array(
            'message_id'    => $param['msg_id'],
            'user_id'       => $auth['user_id'],
            'is_read'       => 1,
            'read_datetime' => time()
        );

        db_query("INSERT INTO ?:message_statuses ?e", $_data);
    }

    $view->assign('message', $message);
}

function fn_get_company($id){
    $company = db_get_row("SELECT * FROM ?:companies WHERE company_id = ?i", $id);

    return $company;
}

function fn_store_message($message=array(), $recipient=array()){
    //Store Private Messaging
    $message_id = db_get_next_auto_increment_id('messages');
    db_query("INSERT INTO ?:messages ?e", $message);

    $data = array (
        'message_id'    => $message_id,
        'user_id'       => $recipient['user_id'],
        'type'          => $recipient['type']
    );

    db_query("INSERT INTO ?:message_recipients ?e", $data);

    return false;
}

function fn_get_messages($params,$fields,$joins,$conditions,$items_per_page = 0){
    //set default parameter
    $default_params = array(
        'page'  => 1,
        'items_per_page'  => $items_per_page
    );

    $params = array_merge($default_params, $params);

    $condition = 1;

    if (!empty($conditions)) {
        $condition .= $conditions;
    }

    $limit = '';
    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field("SELECT COUNT(*) FROM ?:messages m LEFT JOIN ?:message_recipients mr ON m.message_id = mr.message_id WHERE ?p", $condition);
        $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
    }

    $join = "LEFT JOIN ?:message_recipients mr ON m.message_id = mr.message_id ";
    $join.= $joins;

    $messages = db_get_array("SELECT " . implode(', ',$fields) . " FROM ?:messages m $join WHERE ?p ORDER BY m.message_id DESC $limit", $condition);

    return array($messages, $params);
}

function fn_get_message_by_id($params, $conditions){

    $fields = array(
        'm.message_id',
        'm.from',
        'm.message',
        'm.parent_id',
        'm.timestamp',
        'mr.user_id as recipient_id',
        'mr.type',
        'ms.*'
    );

    $condition = 1;

    if (!empty($conditions)){
        $condition .= $conditions;
    }

    $default_join = "LEFT JOIN ?:message_recipients mr ON m.message_id = mr.message_id ";
    $default_join.= "LEFT JOIN ?:message_statuses ms ON m.message_id = ms.message_id ";

    $join = $default_join;

    if (empty($params['mode']) || $params['mode'] != 'outbox'){ //view message from inbox
        $add_fields = array(
            'u.firstname',
            'u.lastname',
            'u.email',
            'c.company as recipient',
            'c.logos'
        );

        $fields = array_merge($add_fields, $fields);

        $join  .= "LEFT JOIN ?:users u ON m.from = u.user_id ";
        $join  .= "LEFT JOIN ?:companies c ON mr.user_id = c.company_id ";
    } else { //view message from outbox
        $add_fields = array(
            'u.firstname',
            'u.lastname',
            'u.email',
            'c.company as sender',
            'c.logos',
            'mp.message as parent_message'
        );

        $fields = array_merge($add_fields, $fields);

        $join  .= "LEFT JOIN ?:users u ON mr.user_id = u.user_id ";
        $join  .= "LEFT JOIN ?:companies c ON m.from = c.company_id ";
        $join  .= "LEFT JOIN ?:messages mp ON m.parent_id = mp.message_id ";
    }

    $message = db_get_row(
        "SELECT ".implode(', ',$fields)." FROM ?:messages m " . $join ."WHERE " . $condition ." AND m.message_id = ?i", $params['msg_id']
    );

    return array($message, $params);
}

function fn_get_message_count($condition){
    $count = db_get_field(
        "SELECT COUNT(*) as cnt FROM ?:messages m LEFT JOIN ?:message_recipients mr ON m.message_id = mr.message_id "
        ."LEFT JOIN ?:message_statuses ms ON m.message_id = ms.message_id AND ms.user_id = mr.user_id "
        ."WHERE ms.is_read IS NULL AND mr.type = 'V' " . $condition
    );

    return $count;
}