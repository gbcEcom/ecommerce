<?php
/**
 * Created by PhpStorm.
 * User: kusnendi.muhamad
 * Date: 6/9/2015
 * Time: 3:27 PM
 */

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if (empty($auth['user_id'])) {
    return array(CONTROLLER_STATUS_REDIRECT, "auth.login_form?return_url=" . urlencode(Registry::get('config.current_url')));
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if (!empty($auth['user_id']) && $mode == 'submit'){

        //Get Request Parameter
        @$param = $_REQUEST;

        $_data = array (
            'product_id'    => $param['product_id'],
            'company_id'    => $param['company_id'],
            'amount'        => $param['amount'],
            'user_id'       => $auth['user_id'],
            'timestamp'     => time()
        );

        db_query("INSERT INTO ?:negotations ?e", $_data);

        $dispatch = "negotiations.manage";
    }

    return array(CONTROLLER_STATUS_OK, $dispatch);
}

//
// Show nego on separate page
//
if ($mode == 'manage'){

    $view   = Registry::get('view');

    $fields = array
    (
        'n.*',
        'up.b_firstname',
        'pd.product',
        'pp.price'
    );

    list($histories, $search) = fn_get_negos($_REQUEST, $fields, '', '' ,15);

    foreach ($histories as $nego){
        if ($nego['status'] == 0){
            $histories[0]['label'] = 'Menunggu';
        } elseif ($nego['status'] == 1) {
            $histories[0]['label'] = 'Diterima';
        } else {
            $histories[0]['label'] = 'Ditolak';
        }
    }
    //echo '<pre>';
    //echo print_r($histories);
    //exit;
    //Generate View
    $view->assign('histories', $histories);
//
// Create Compose Message
//
} elseif ($mode == 'tawar'){

    $param = $_REQUEST;
    $view  = Registry::get('view');

    $product = fn_get_product_data(
        $_REQUEST['product_id'],
        $auth,
        CART_LANGUAGE,
        '',
        true,
        true,
        true,
        true,
        fn_is_preview_action($auth, $_REQUEST),
        true,
        false,
        true
    );

    $view->assign('product', $product);
//
// Create Compose Message
//
} elseif ($mode == 'view'){

    $param = $_REQUEST;

    $view = Registry::get('view');

    $message = fn_get_message($param['msg_id'],$auth['user_id']);

    if (is_null($message['is_read']) || empty($message['is_read']) || $message['is_read'] == 0){
        //update message status
        $_data = array(
            'message_id'    => $param['msg_id'],
            'user_id'       => $auth['user_id'],
            'is_read'       => 1,
            'read_datetime' => time()
        );

        db_query("INSERT INTO ?:message_statuses ?e", $_data);
    }

    $view->assign('message', $message);

} elseif ($mode == 'process'){

    $param = $_REQUEST;

    db_query("UPDATE ?:negotations SET status = ?i WHERE nego_id = ?i", $param['status'], $param['nego_id']);

    $dispatch = "negotiations.manage";

    return array(CONTROLLER_STATUS_OK, $dispatch);
}

function fn_get_negos($params,$fields,$joins,$conditions,$items_per_page = 0){
    //set default parameter
    $default_params = array(
        'page'  => 1,
        'items_per_page'  => $items_per_page
    );

    $params = array_merge($default_params, $params);

    $condition = 1;

    if (!empty($conditions)) {
        $condition .= $conditions;
    }

    $limit = '';
    if (!empty($params['items_per_page'])) {
        $params['total_items'] = db_get_field("SELECT COUNT(*) FROM ?:negotations WHERE ?p", $condition);
        $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
    }

    $join = "LEFT JOIN ?:product_descriptions pd ON n.product_id = pd.product_id ";
    $join.= "LEFT JOIN ?:product_prices pp ON n.product_id = pp.product_id ";
    $join.= "LEFT JOIN ?:user_profiles up ON n.user_id = up.user_id";
    $join.= $joins;

    $condition .= db_quote(" AND pd.lang_code = ?s", 'id');

    $negotations = db_get_array("SELECT " . implode(', ',$fields) . " FROM ?:negotations n $join WHERE ?p ORDER BY n.nego_id DESC $limit", $condition);

    return array($negotations, $params);
}

function fn_get_company($id){
    $company = db_get_row("SELECT * FROM ?:companies WHERE company_id = ?i", $id);

    return $company;
}