<?php
/**
 * Created by PhpStorm.
 * User: kusnendi.muhamad
 * Date: 6/9/2015
 * Time: 3:27 PM
 */

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if (empty($auth['user_id'])) {
    return array(CONTROLLER_STATUS_REDIRECT, "auth.login_form?return_url=" . urlencode(Registry::get('config.current_url')));
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if (!empty($auth['user_id']) && $mode == 'submit'){

        //Get Request Parameter
        @$param = $_REQUEST;

        $_data = array (
            'product_id'    => $param['product_id'],
            'company_id'    => $param['company_id'],
            'amount'        => $param['amount'],
            'user_id'       => $auth['user_id'],
            'timestamp'     => time()
        );

        db_query("INSERT INTO ?:negotations ?e", $_data);

        $dispatch = "negotiations.manage";
    }

    return array(CONTROLLER_STATUS_OK, $dispatch);
}

//
// Show nego on separate page
//
if ($mode == 'manage'){
    fn_add_breadcrumb('Nego');

    $view = Registry::get('view');
    $histories = db_get_array("SELECT n.*,pd.product,pp.price FROM ?:negotations n LEFT JOIN ?:product_descriptions pd ON n.product_id=pd.product_id LEFT JOIN ?:product_prices pp ON n.product_id=pp.product_id WHERE user_id = ?i AND pd.lang_code = ?s", $auth['user_id'], 'id');

    foreach ($histories as $nego){
        if ($nego['status'] == 0){
            $histories[0]['label'] = 'Menunggu';
        } elseif ($nego['status'] == 1) {
            $histories[0]['label'] = 'Diterima';
        } else {
            $histories[0]['label'] = 'Ditolak';
        }
    }
    //echo '<pre>';
    //echo print_r($histories);
    //exit;
    //Generate View
    $view->assign('histories', $histories);
//
// Create Compose Message
//
} elseif ($mode == 'tawar'){
    fn_add_breadcrumb('Nego');
    $param = $_REQUEST;
    $view  = Registry::get('view');

    $product = fn_get_product_data(
        $_REQUEST['product_id'],
        $auth,
        CART_LANGUAGE,
        '',
        true,
        true,
        true,
        true,
        fn_is_preview_action($auth, $_REQUEST),
        true,
        false,
        true
    );

    $view->assign('product', $product);
//
// Create Compose Message
//
} elseif ($mode == 'view'){
    fn_add_breadcrumb('Message');
    fn_add_breadcrumb('View');

    $param = $_REQUEST;

    $view = Registry::get('view');

    $message = fn_get_message($param['msg_id'],$auth['user_id']);

    if (is_null($message['is_read']) || empty($message['is_read']) || $message['is_read'] == 0){
        //update message status
        $_data = array(
            'message_id'    => $param['msg_id'],
            'user_id'       => $auth['user_id'],
            'is_read'       => 1,
            'read_datetime' => time()
        );

        db_query("INSERT INTO ?:message_statuses ?e", $_data);
    }

    $view->assign('message', $message);
}

function fn_get_company($id){
    $company = db_get_row("SELECT * FROM ?:companies WHERE company_id = ?i", $id);

    return $company;
}