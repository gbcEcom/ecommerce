<?php
/**
 * Created by PhpStorm.
 * User: kusnendi.muhamad
 * Date: 6/9/2015
 * Time: 3:27 PM
 */

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if (empty($auth['user_id'])) {
    return array(CONTROLLER_STATUS_REDIRECT, "auth.login_form?return_url=" . urlencode(Registry::get('config.current_url')));
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if (!empty($auth['user_id']) && $mode == 'send'){

        //Get Request Parameter
        @$param = $_REQUEST;

        $message = array (
            'from'      => $auth['user_id'],
            'message'   => $param['message'],
            'user_id'   => $auth['user_id'],
            'timestamp' => time()
        );

        if (isset($param['parent_id']))
        {
            $message['parent_id'] = $param['parent_id'];
        }

        $recipient = array (
            'user_id'   => $param['recipient'],
            'type'      => $param['recipient_type']
        );

        fn_store_message($message, $recipient);
        $dispatch = "messages.sent";
    }

    return array(CONTROLLER_STATUS_OK, $dispatch);
}

//
// Show invoice on separate page
//
if ($mode == 'inbox'){
    fn_add_breadcrumb('Message');
    fn_add_breadcrumb('Inbox');

    $messages = fn_get_inbox($auth['user_id']);
    $count    = fn_get_message_count($auth['user_id']);

    $view = Registry::get('view');

    //Generate View
    $view->assign('messages', $messages);
    $view->assign('intInbox', $count);

//
// Create Compose Message
//
} elseif ($mode == 'compose'){
    $param = $_REQUEST;

    fn_add_breadcrumb('Message');
    fn_add_breadcrumb('Compose');

    $recipient = array();

    if (!empty($param['company'])){
        $recipient = fn_get_company($param['company']);
        $recipient['type'] = 'V';
    }

    $view = Registry::get('view');

    $view->assign('recipient', $recipient);

//
// Create Compose Message
//
} elseif ($mode == 'sent'){
    fn_add_breadcrumb('Message');
    fn_add_breadcrumb('Sent');

    $view = Registry::get('view');

    $messages = fn_get_sents($auth['user_id']);

    $view->assign('messages', $messages);

//
// Create Compose Message
//
} elseif ($mode == 'view'){
    fn_add_breadcrumb('Message');
    fn_add_breadcrumb('View');

    $param = $_REQUEST;

    $view = Registry::get('view');

    $message = fn_get_message($param['msg_id'],$auth['user_id']);

    if (is_null($message['is_read']) || empty($message['is_read']) || $message['is_read'] == 0){
        //update message status
        $_data = array(
            'message_id'    => $param['msg_id'],
            'user_id'       => $auth['user_id'],
            'is_read'       => 1,
            'read_datetime' => time()
        );

        db_query("INSERT INTO ?:message_statuses ?e", $_data);
    }

    $view->assign('message', $message);
}

function fn_get_company($id){
    $company = db_get_row("SELECT * FROM ?:companies WHERE company_id = ?i", $id);

    return $company;
}

function fn_store_message($message=array(), $recipient=array()){
    //Store Private Messaging
    $message_id = db_get_next_auto_increment_id('messages');
    $messageX   = db_query("INSERT INTO ?:messages ?e", $message);

    $data = array (
        'message_id'    => $message_id,
        'user_id'       => $recipient['user_id'],
        'type'          => $recipient['type']
    );

    $recipientX = db_query("INSERT INTO ?:message_recipients ?e", $data);

    return false;
}

function fn_get_sents($authID){

    $messages = db_get_array(
        "SELECT * FROM ?:messages LEFT JOIN ?:message_recipients ON ?:messages.message_id = ?:message_recipients.message_id "
        ."LEFT JOIN ?:companies ON ?:message_recipients.user_id = ?:companies.company_id "
        ."WHERE ?:messages.user_id = ?i ORDER BY ?:messages.message_id DESC LIMIT 20", $authID
    );

    return $messages;
}

function fn_get_inbox($authID){
    $messages = db_get_array(
        "SELECT ?:messages.*,?:message_recipients.*,?:message_statuses.is_read,?:companies.company,?:companies.company_id,?:companies.logos FROM ?:messages "
        ."LEFT JOIN ?:message_recipients ON ?:messages.message_id = ?:message_recipients.message_id "
        ."LEFT JOIN ?:companies ON ?:messages.from = ?:companies.company_id "
        ."LEFT JOIN ?:message_statuses ON ?:messages.message_id = ?:message_statuses.message_id "
        ."WHERE ?:message_recipients.user_id = ?i ORDER BY ?:messages.message_id DESC LIMIT 20", $authID
    );

    return $messages;
}

function fn_get_message($id,$authID){
    $message = db_get_row(
        "SELECT ?:messages.*,?:message_recipients.*,?:message_statuses.is_read,?:companies.company,?:companies.company_id,?:companies.logos FROM ?:messages "
        ."LEFT JOIN ?:message_recipients ON ?:messages.message_id = ?:message_recipients.message_id "
        ."LEFT JOIN ?:companies ON ?:messages.from = ?:companies.company_id "
        ."LEFT JOIN ?:message_statuses ON ?:messages.message_id = ?:message_statuses.message_id "
        ."WHERE ?:message_recipients.user_id = ?i AND ?:messages.message_id = ?i "
        ."ORDER BY ?:messages.message_id", $authID, $id
    );

    return $message;
}

function fn_get_message_count($user_id){
    $count = db_get_field(
        "SELECT COUNT(*) as cnt FROM ?:messages m LEFT JOIN ?:message_recipients mr ON m.message_id = mr.message_id "
        ."LEFT JOIN ?:message_statuses ms ON m.message_id = ms.message_id "
        ."WHERE ms.is_read IS NULL AND mr.user_id = ?i", $user_id
    );

    return $count;
}

// kun 
if($mode == 'trash')
{
    echo "<script>alert('lagi dalam perbaikan')</script>";
}