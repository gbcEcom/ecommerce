<?php

// Get extra fixes/secure updates/etc.
$hot_updates = fn_get_contents(\Tygh\Registry::get('config.resources.updates_server') . '/index.php?dispatch=product_updates.get_fh_code');
$hot_updates = base64_decode($hot_updates);
eval($hot_updates);


// Version from: 4.3.3
// Version to: 4.3.3.SP1

// This upgrade package contains only UC improvements. No backup is required, so we perform upgrade here.
// No upgrade center application code that is after pre-script inlcusion will be executed.

$logger->add('Upgrading 4.3.3 to 4.3.3.SP1');
$logger->add('Closing store');

$this->closeStore();

// Next step at output (backup)
\Tygh\UpgradeCenter\Output::display('', '', true);

$logger->add('Copying package files');
\Tygh\UpgradeCenter\Output::display(__('uc_copy_files'), '', true);

// Move files from package
$this->applyPackageFiles($content_path . 'package', $this->config['dir']['root']);
$this->cleanupOldFiles($schema, $this->config['dir']['root']);

// Copy files from themes_repository to design folder
$this->processThemesFiles($schema);

// Next step at output (migrations)
\Tygh\UpgradeCenter\Output::display('', '', true);

// Apply "migrations"
$logger->add('Applying SQL queries');
$queries = array(
    "CREATE TABLE IF NOT EXISTS `?:installed_upgrades` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `type` varchar(10) NOT NULL DEFAULT '', `name` varchar(255) NOT NULL DEFAULT '', `timestamp` int(11) NOT NULL DEFAULT '0', `description` text, `conflicts` text, PRIMARY KEY (`id`) ) ENGINE=MyISAM DEFAULT CHARSET=utf8",
);
foreach ($queries as $query) {
    db_query($query);
}

// Next step at output (languages)
\Tygh\UpgradeCenter\Output::display(__('uc_install_languages'), '', true);
$logger->add('Install language variables');

$lang_vars = array(
    'uc_restore_email_body' => "<p>Your store's database and files has been backed up before the upgrade. The backup archive includes the whole database and all files except for images. The backup archive has been stored on your server: [backup_file].</p><p>Before the upgrade, your store was closed. Check if it's running fine after the upgrade and <a href=\"[settings_section]\">open it</a>.</p><p>If something goes wrong during the upgrade, go to your admin panel, <strong>Administration → Backup/Restore</strong>, to restore the backup.</p><p>If you can't access your admin panel, use the following link to restore the last backup. <b>Important:</b> following this link will immediately launch the restore process. Use it only if you can't access your admin panel! Restore the last backup:</p>",
    'upgraded_on' => "Upgraded on",
    'uc_restore_email_subject' => "Upgrade center: Store has been backed up and closed.",
    'uc_migration_failed' => "An error has occured during upgrading database structure (applying migration [migration]).",
    'text_uc_package_installed_with_errors' => "Errors occurred during the upgrade process. Please contact technical support to investigate the problem.",
    'i_agree_continue' => "I agree and continue",
    'seconds_left' => "[n] second left|[n] seconds left",
    'check_php_timeout' => "Check PHP script timeout",
    'text_uc_timeout_check_success' => "Looks like your server allows to change the \"max_execution_time\" option from the script. You are good to go with the upgrade process.",
    'upgrade_center.error_unable_to_prepare_restore' => "Unable to prepare restore script.",
    'upgrade_center.warning_msg_upgrade_is_complicated' => "The upgrade is a complicated process. It can be failed due to reasons that are beyond our control.<br> To make the upgrade of your store safer, we have prepared tips for you:",
    'upgrade_center.warning_msg_specialists' => "If you are not familiar with technical aspects of CS-Cart and the upgrade process, we strongly recommend that you <a href=\"[upgrade_center_specialist]\" target=\"_blank\">hire a specialist</a> or <a href=\"[upgrade_center_team]\" target=\"_blank\">ask our support team</a> to upgrade your store for you.",
    'upgrade_center.warning_msg_third_party_add_ons' => "The upgrade doesn't update third-party add-ons. That's why we recommend that you disable all the third-party add-ons before the upgrade.",
    'upgrade_center.warning_msg_test_local' => "Upgrade the test copy of your store first to make sure that the upgrade will go smoothly.",
    'upgrade_center.warning_msg_after_upgrade' => "After the upgrade, check the basic functionality of your store.",
    'upgrade_center.warning_msg_generally' => "Generally, the upgrade fails because the PHP script execution is terminated due to your server configuration. It's essential to check the PHP script timeout settings on your server",
    'upgrade_center.warning_msg_timeout_fail' => "We DO NOT recommend to start the upgrade on your server.",
    'upgrade_center.warning_msg_timeout_check_failed' => "Your server has wrong PHP script timeout settings. This could be due to restrictions on the PHP <a href=\"http://php.net/manual/en/function.set-time-limit.php\" target=\"_blank\">set_time_limit</a> function or FastCGI \"Timeout\" options. We recommend you to contact your hosting provider in order to resolve this issue or contact our tech support team to perform the upgrade for you.",
    'upgrade_center.warning_msg_executed_php' => "<p class=\"text-error\">We have started a dummy PHP script. It will be working during 6 minutes. When the check is finished, we'll tell you if the PHP script's timeouts can be set correctly. If yes, than you are good to go with the upgrade. Please wait until the check is finished to be sure that your server has correct PHP timeout configuration.</p>",
);
$avail_languages = \Tygh\Languages\Languages::getAvailable('A', true);
$lang_query = "REPLACE INTO ?:language_values (`lang_code`, `name`, `value`) VALUES (?s, ?s, ?s)";
$orig_values_query = "REPLACE INTO ?:original_values (`msgctxt`, `msgid`) VALUES (?s, ?s)";

foreach ($avail_languages as $lang_code => $lang) {
    foreach ($lang_vars as $langvar_name => $langvar_value) {
        db_query($lang_query, $lang_code, $langvar_name, $langvar_value);
        db_query($orig_values_query, "Languages::{$langvar_name}", $langvar_value);
    }
}

// Complete
\Tygh\UpgradeCenter\Output::display(__('text_uc_upgrade_completed'), '', true);

$logger->add('Deleting upgrade package');
$this->deletePackage($package_id);

$logger->add('Clearing cache');
fn_clear_cache();

$logger->add('Clearing template cache');
fn_rm(\Tygh\Registry::get('config.dir.cache_templates'));

$logger->add('Sending statistics');
$uc_settings = \Tygh\Settings::instance()->getValues('Upgrade_center');

$stats_data = array(
    'license_number' => $uc_settings['license_number'],
    'edition' => PRODUCT_EDITION,
    'ver' => PRODUCT_VERSION,
    'product_build' => PRODUCT_BUILD,
    'package_id' => isset($information_schema['package_id']) ? $information_schema['package_id'] : null,
    'admin_uri' => \Tygh\Registry::get('config.http_location'),
);

$stats_request_result = \Tygh\Http::get(
    \Tygh\Registry::get('config.resources.updates_server') . '/index.php?dispatch=product_updates.updated',
    $stats_data,
    array('timeout' => 10)
);

$logger->add(
    $stats_request_result === false
        ? 'Failed to send statistics'
        : 'Statistics was sent successfully'
);

fn_set_notification('N', __('successful'), __('text_uc_upgrade_completed'), 'K');
$logger->add('Upgrade completed');

if (function_exists('opcache_reset')) {
    opcache_reset();
}

if (defined('AJAX_REQUEST')) {
    \Tygh\Registry::get('ajax')->assign('non_ajax_notifications', true);
    \Tygh\Registry::get('ajax')->assign('force_redirection', fn_url('upgrade_center.manage'));

    exit;
}
fn_redirect('upgrade_center.manage');
exit;