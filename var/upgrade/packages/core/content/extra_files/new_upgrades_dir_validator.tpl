<h4 class="upgrade-center_error">{__("permissions_issue")}</h4>
<p>{$message_text}</p>
{if $need_ftp_credentials}
    {include file="views/upgrade_center/ftp_settings.tpl"}
{/if}