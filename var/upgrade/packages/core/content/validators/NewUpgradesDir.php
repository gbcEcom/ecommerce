<?php
namespace Tygh\UpgradeCenter\Validators;

use Tygh\Registry;
use Tygh;
use Tygh\Settings;

class NewUpgradesDir implements IValidator
{
    protected $config = array();

    protected $uc_settings = array();

    public function __construct()
    {
        $this->config = Registry::get('config');
        $this->uc_settings = Settings::instance()->getValues('Upgrade_center');
    }

    public function getName()
    {
        return 'Upgrades directory';
    }

    public function check($schema, $request)
    {
        $content_dir_path = $this->config['dir']['upgrade'] . 'packages/' . $request['id'] . '/content/';


        $need_ftp_credentials = false;
        $message_text = null;

        $root_directory = rtrim($this->config['dir']['root'], '\\/') . DIRECTORY_SEPARATOR;
        $new_dir_path = $root_directory . 'upgrades';

        if (is_dir($new_dir_path)) {
            return array(true, array());
        } else {
            $mkdir_result = @mkdir($new_dir_path, 0755);
            if ($mkdir_result) {
                return array(true, array());
            } else {
                if (!is_resource(Registry::get('ftp_connection'))) {
                    fn_ftp_connect($this->uc_settings);
                }

                if (is_resource(Registry::get('ftp_connection'))) {
                    $mkdir_result = @ftp_mkdir(Registry::get('ftp_connection'), $new_dir_path);
                    if ($mkdir_result) {
                        fn_ftp_chmod_file($new_dir_path, 0755);
                        Registry::set('ftp_connection', null);

                        return array(true, array());
                    } else {
                        $message_text = 'Unable to create "upgrades" directory via FTP. Please, create it manually.';
                    }
                } else {
                    $need_ftp_credentials = true;
                }
            }
        }

        $smarty = Tygh::$app['view'];
        $smarty->assign('need_ftp_credentials', $need_ftp_credentials)
            ->assign('message_text', $message_text)
            ->assign('uc_settings', $this->uc_settings);


        return array(false, $smarty->fetch($content_dir_path . 'extra_files/new_upgrades_dir_validator.tpl'));
    }
}