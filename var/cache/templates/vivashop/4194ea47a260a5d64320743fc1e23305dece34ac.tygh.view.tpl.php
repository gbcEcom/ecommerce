<?php /* Smarty version Smarty-3.1.21, created on 2015-08-10 22:30:48
         compiled from "/home/gbadmin/public_html/production/design/themes/vivashop/templates/views/categories/view.tpl" */ ?>
<?php /*%%SmartyHeaderCode:157597790355c8c3a87073c4-02081877%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4194ea47a260a5d64320743fc1e23305dece34ac' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/themes/vivashop/templates/views/categories/view.tpl',
      1 => 1438219702,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '157597790355c8c3a87073c4-02081877',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'block' => 0,
    'category_data' => 0,
    'depth' => 0,
    'subcategories' => 0,
    'columns' => 0,
    'rows' => 0,
    'extra_ss_class' => 0,
    'splitted_subcategories' => 0,
    'ssubcateg' => 0,
    'category' => 0,
    'settings' => 0,
    'products' => 0,
    'selected_layout' => 0,
    'layouts' => 0,
    'product_columns' => 0,
    'show_no_products_block' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55c8c3a8a03c23_41157394',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55c8c3a8a03c23_41157394')) {function content_55c8c3a8a03c23_41157394($_smarty_tpl) {?><?php if (!is_callable('smarty_block_hook')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/block.hook.php';
if (!is_callable('smarty_function_live_edit')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/function.live_edit.php';
if (!is_callable('smarty_function_math')) include '/home/gbadmin/public_html/production/app/lib/vendor/smarty/smarty/libs/plugins/function.math.php';
if (!is_callable('smarty_function_split')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/function.split.php';
if (!is_callable('smarty_function_set_id')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/function.set_id.php';
?><?php
fn_preload_lang_vars(array('text_no_products','text_no_products'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
$_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"categories:view")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"categories:view"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>


<div id="category_products_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['block_id'], ENT_QUOTES, 'ISO-8859-1');?>
">
<?php $_smarty_tpl->tpl_vars['depth'] = new Smarty_variable(count(explode("/",$_smarty_tpl->tpl_vars['category_data']->value['id_path'])), null, 0);?>

<div class="vs-category vs-category-level-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['depth']->value, ENT_QUOTES, 'ISO-8859-1');?>
">

<div class="clearfix vs-category-header">
    <div class="vs-category-descr vs-categ-banner">
    <?php if (($_smarty_tpl->tpl_vars['category_data']->value['description']&&$_smarty_tpl->tpl_vars['category_data']->value['description']!='')||$_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['live_editor']) {?>
        <div class="ty-wysiwyg-content ty-mb-s vs-categ-descr-box" <?php echo smarty_function_live_edit(array('name'=>"category:description:".((string)$_smarty_tpl->tpl_vars['category_data']->value['category_id'])),$_smarty_tpl);?>
><?php echo $_smarty_tpl->tpl_vars['category_data']->value['description'];?>
</div>
    <?php }?>
    </div>
</div>
<?php echo '<script'; ?>
 type="text/javascript">
//<![CDATA[
(function(_, $) {
    $.ceEvent('on', 'ce.commoninit', function(context) {
        var windowWidth = window.innerWidth || document.documentElement.clientWidth;
        //banner
        var banner=$(".category-banner").detach();
        $(".vs-category-descr").prepend(banner);
        <?php if ($_smarty_tpl->tpl_vars['depth']->value==1) {?>
            banner.show();
        <?php }?>

        //scroller
        var scroller=$(".vs-scroller-category-product").detach();

        if (scroller.length){
            scroller.addClass("ty-float-right");
            $(".vs-category-header").append(scroller);
        }

        <?php if ($_smarty_tpl->tpl_vars['depth']->value==1) {?>
            if (scroller.length){
                $(".vs-category-descr").addClass("ty-float-left");
                if (windowWidth > 1247){
                    var header_w=$(".vs-category-header").width();
                    var scroller_w=scroller.outerWidth(true);
                    $(".vs-category-descr").css("width",header_w-scroller_w);
                }else{
                    $(".vs-category-descr").css("width","646px");
                }
                scroller.show();
            }
        <?php }?>
        
        //disabled scroller buttons
        var buttons=$(".subcateg-scroller .owl-controls");
        var buttons_status=buttons.css('display');

        if(buttons_status != "block"){
            $(".owl-buttons div",buttons).addClass("vs-disabled");
            buttons.show();
        }
    });
}(Tygh, Tygh.$));
//]]>
<?php echo '</script'; ?>
>

<?php if ($_smarty_tpl->tpl_vars['subcategories']->value) {?>
<?php echo smarty_function_math(array('equation'=>"ceil(n/c)",'assign'=>"rows",'n'=>count($_smarty_tpl->tpl_vars['subcategories']->value),'c'=>(($tmp = @$_smarty_tpl->tpl_vars['columns']->value)===null||$tmp==='' ? "2" : $tmp)),$_smarty_tpl);?>

<?php echo smarty_function_split(array('data'=>$_smarty_tpl->tpl_vars['subcategories']->value,'size'=>$_smarty_tpl->tpl_vars['rows']->value,'assign'=>"splitted_subcategories"),$_smarty_tpl);?>

<div class="clear-both">
    <?php if (sizeof($_smarty_tpl->tpl_vars['subcategories']->value)<6) {?>
        <?php $_smarty_tpl->tpl_vars['extra_ss_class'] = new Smarty_variable("disable_buttons", null, 0);?>
    <?php }?>
    <div id="scroll_list_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['block_id'], ENT_QUOTES, 'ISO-8859-1');?>
" class="owl-carousel subcateg-scroller <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['extra_ss_class']->value, ENT_QUOTES, 'ISO-8859-1');?>
">
    <?php  $_smarty_tpl->tpl_vars["ssubcateg"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["ssubcateg"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['splitted_subcategories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["ssubcateg"]->key => $_smarty_tpl->tpl_vars["ssubcateg"]->value) {
$_smarty_tpl->tpl_vars["ssubcateg"]->_loop = true;
?>
        <?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['ssubcateg']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
?>
            <?php if ($_smarty_tpl->tpl_vars['category']->value) {?>
             <div class="jscroll-item">
                <div class="ty-center scroll-image">
                    <a href="<?php echo htmlspecialchars(fn_url("categories.view?category_id=".((string)$_smarty_tpl->tpl_vars['category']->value['category_id'])), ENT_QUOTES, 'ISO-8859-1');?>
">
                        <?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('show_detailed_link'=>false,'images'=>$_smarty_tpl->tpl_vars['category']->value['main_pair'],'no_ids'=>true,'image_id'=>"category_image",'image_width'=>$_smarty_tpl->tpl_vars['settings']->value['Thumbnails']['category_lists_thumbnail_width'],'image_height'=>$_smarty_tpl->tpl_vars['settings']->value['Thumbnails']['category_lists_thumbnail_height'],'class'=>"animate scale"), 0);?>

                    </a>
                </div>
                <div class="subcateg-title ty-center">
                    <a href="<?php echo htmlspecialchars(fn_url("categories.view?category_id=".((string)$_smarty_tpl->tpl_vars['category']->value['category_id'])), ENT_QUOTES, 'ISO-8859-1');?>
"><span <?php echo smarty_function_live_edit(array('name'=>"category:category:".((string)$_smarty_tpl->tpl_vars['category']->value['category_id'])),$_smarty_tpl);?>
><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['category'], ENT_QUOTES, 'ISO-8859-1');?>
</span></a>
                </div>
            </div>
            <?php }?>
        <?php } ?>
    <?php } ?>
    </div>
    <?php echo $_smarty_tpl->getSubTemplate ("common/scroller_init.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('vs_subcateg_scroller'=>true), 0);?>

</div>
<?php }?>


<?php if ($_smarty_tpl->tpl_vars['products']->value) {?>
<?php $_smarty_tpl->tpl_vars["layouts"] = new Smarty_variable(fn_get_products_views('',false,0), null, 0);?>
<?php if ($_smarty_tpl->tpl_vars['category_data']->value['product_columns']) {?>
    <?php $_smarty_tpl->tpl_vars["product_columns"] = new Smarty_variable($_smarty_tpl->tpl_vars['category_data']->value['product_columns'], null, 0);?>
<?php } else { ?>
    <?php $_smarty_tpl->tpl_vars["product_columns"] = new Smarty_variable($_smarty_tpl->tpl_vars['settings']->value['Appearance']['columns_in_products_list'], null, 0);?>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['layouts']->value[$_smarty_tpl->tpl_vars['selected_layout']->value]['template']) {?>
    <?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['layouts']->value[$_smarty_tpl->tpl_vars['selected_layout']->value]['template']), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('columns'=>$_smarty_tpl->tpl_vars['product_columns']->value), 0);?>

<?php }?>

<?php } elseif (!$_smarty_tpl->tpl_vars['subcategories']->value||$_smarty_tpl->tpl_vars['show_no_products_block']->value) {?>
<p class="ty-no-items cm-pagination-container"><?php echo $_smarty_tpl->__("text_no_products");?>
</p>
<?php } else { ?>
<div class="cm-pagination-container clear-both"></div>
<?php }?>
</div>
<!--category_products_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['block_id'], ENT_QUOTES, 'ISO-8859-1');?>
--></div>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start(); ?><span <?php echo smarty_function_live_edit(array('name'=>"category:category:".((string)$_smarty_tpl->tpl_vars['category_data']->value['category_id'])),$_smarty_tpl);?>
><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category_data']->value['category'], ENT_QUOTES, 'ISO-8859-1');?>
</span><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"categories:view"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="views/categories/view.tpl" id="<?php echo smarty_function_set_id(array('name'=>"views/categories/view.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
$_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"categories:view")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"categories:view"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>


<div id="category_products_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['block_id'], ENT_QUOTES, 'ISO-8859-1');?>
">
<?php $_smarty_tpl->tpl_vars['depth'] = new Smarty_variable(count(explode("/",$_smarty_tpl->tpl_vars['category_data']->value['id_path'])), null, 0);?>

<div class="vs-category vs-category-level-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['depth']->value, ENT_QUOTES, 'ISO-8859-1');?>
">

<div class="clearfix vs-category-header">
    <div class="vs-category-descr vs-categ-banner">
    <?php if (($_smarty_tpl->tpl_vars['category_data']->value['description']&&$_smarty_tpl->tpl_vars['category_data']->value['description']!='')||$_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['live_editor']) {?>
        <div class="ty-wysiwyg-content ty-mb-s vs-categ-descr-box" <?php echo smarty_function_live_edit(array('name'=>"category:description:".((string)$_smarty_tpl->tpl_vars['category_data']->value['category_id'])),$_smarty_tpl);?>
><?php echo $_smarty_tpl->tpl_vars['category_data']->value['description'];?>
</div>
    <?php }?>
    </div>
</div>
<?php echo '<script'; ?>
 type="text/javascript">
//<![CDATA[
(function(_, $) {
    $.ceEvent('on', 'ce.commoninit', function(context) {
        var windowWidth = window.innerWidth || document.documentElement.clientWidth;
        //banner
        var banner=$(".category-banner").detach();
        $(".vs-category-descr").prepend(banner);
        <?php if ($_smarty_tpl->tpl_vars['depth']->value==1) {?>
            banner.show();
        <?php }?>

        //scroller
        var scroller=$(".vs-scroller-category-product").detach();

        if (scroller.length){
            scroller.addClass("ty-float-right");
            $(".vs-category-header").append(scroller);
        }

        <?php if ($_smarty_tpl->tpl_vars['depth']->value==1) {?>
            if (scroller.length){
                $(".vs-category-descr").addClass("ty-float-left");
                if (windowWidth > 1247){
                    var header_w=$(".vs-category-header").width();
                    var scroller_w=scroller.outerWidth(true);
                    $(".vs-category-descr").css("width",header_w-scroller_w);
                }else{
                    $(".vs-category-descr").css("width","646px");
                }
                scroller.show();
            }
        <?php }?>
        
        //disabled scroller buttons
        var buttons=$(".subcateg-scroller .owl-controls");
        var buttons_status=buttons.css('display');

        if(buttons_status != "block"){
            $(".owl-buttons div",buttons).addClass("vs-disabled");
            buttons.show();
        }
    });
}(Tygh, Tygh.$));
//]]>
<?php echo '</script'; ?>
>

<?php if ($_smarty_tpl->tpl_vars['subcategories']->value) {?>
<?php echo smarty_function_math(array('equation'=>"ceil(n/c)",'assign'=>"rows",'n'=>count($_smarty_tpl->tpl_vars['subcategories']->value),'c'=>(($tmp = @$_smarty_tpl->tpl_vars['columns']->value)===null||$tmp==='' ? "2" : $tmp)),$_smarty_tpl);?>

<?php echo smarty_function_split(array('data'=>$_smarty_tpl->tpl_vars['subcategories']->value,'size'=>$_smarty_tpl->tpl_vars['rows']->value,'assign'=>"splitted_subcategories"),$_smarty_tpl);?>

<div class="clear-both">
    <?php if (sizeof($_smarty_tpl->tpl_vars['subcategories']->value)<6) {?>
        <?php $_smarty_tpl->tpl_vars['extra_ss_class'] = new Smarty_variable("disable_buttons", null, 0);?>
    <?php }?>
    <div id="scroll_list_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['block_id'], ENT_QUOTES, 'ISO-8859-1');?>
" class="owl-carousel subcateg-scroller <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['extra_ss_class']->value, ENT_QUOTES, 'ISO-8859-1');?>
">
    <?php  $_smarty_tpl->tpl_vars["ssubcateg"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["ssubcateg"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['splitted_subcategories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["ssubcateg"]->key => $_smarty_tpl->tpl_vars["ssubcateg"]->value) {
$_smarty_tpl->tpl_vars["ssubcateg"]->_loop = true;
?>
        <?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['ssubcateg']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value) {
$_smarty_tpl->tpl_vars['category']->_loop = true;
?>
            <?php if ($_smarty_tpl->tpl_vars['category']->value) {?>
             <div class="jscroll-item">
                <div class="ty-center scroll-image">
                    <a href="<?php echo htmlspecialchars(fn_url("categories.view?category_id=".((string)$_smarty_tpl->tpl_vars['category']->value['category_id'])), ENT_QUOTES, 'ISO-8859-1');?>
">
                        <?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('show_detailed_link'=>false,'images'=>$_smarty_tpl->tpl_vars['category']->value['main_pair'],'no_ids'=>true,'image_id'=>"category_image",'image_width'=>$_smarty_tpl->tpl_vars['settings']->value['Thumbnails']['category_lists_thumbnail_width'],'image_height'=>$_smarty_tpl->tpl_vars['settings']->value['Thumbnails']['category_lists_thumbnail_height'],'class'=>"animate scale"), 0);?>

                    </a>
                </div>
                <div class="subcateg-title ty-center">
                    <a href="<?php echo htmlspecialchars(fn_url("categories.view?category_id=".((string)$_smarty_tpl->tpl_vars['category']->value['category_id'])), ENT_QUOTES, 'ISO-8859-1');?>
"><span <?php echo smarty_function_live_edit(array('name'=>"category:category:".((string)$_smarty_tpl->tpl_vars['category']->value['category_id'])),$_smarty_tpl);?>
><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category']->value['category'], ENT_QUOTES, 'ISO-8859-1');?>
</span></a>
                </div>
            </div>
            <?php }?>
        <?php } ?>
    <?php } ?>
    </div>
    <?php echo $_smarty_tpl->getSubTemplate ("common/scroller_init.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('vs_subcateg_scroller'=>true), 0);?>

</div>
<?php }?>


<?php if ($_smarty_tpl->tpl_vars['products']->value) {?>
<?php $_smarty_tpl->tpl_vars["layouts"] = new Smarty_variable(fn_get_products_views('',false,0), null, 0);?>
<?php if ($_smarty_tpl->tpl_vars['category_data']->value['product_columns']) {?>
    <?php $_smarty_tpl->tpl_vars["product_columns"] = new Smarty_variable($_smarty_tpl->tpl_vars['category_data']->value['product_columns'], null, 0);?>
<?php } else { ?>
    <?php $_smarty_tpl->tpl_vars["product_columns"] = new Smarty_variable($_smarty_tpl->tpl_vars['settings']->value['Appearance']['columns_in_products_list'], null, 0);?>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['layouts']->value[$_smarty_tpl->tpl_vars['selected_layout']->value]['template']) {?>
    <?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['layouts']->value[$_smarty_tpl->tpl_vars['selected_layout']->value]['template']), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('columns'=>$_smarty_tpl->tpl_vars['product_columns']->value), 0);?>

<?php }?>

<?php } elseif (!$_smarty_tpl->tpl_vars['subcategories']->value||$_smarty_tpl->tpl_vars['show_no_products_block']->value) {?>
<p class="ty-no-items cm-pagination-container"><?php echo $_smarty_tpl->__("text_no_products");?>
</p>
<?php } else { ?>
<div class="cm-pagination-container clear-both"></div>
<?php }?>
</div>
<!--category_products_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['block_id'], ENT_QUOTES, 'ISO-8859-1');?>
--></div>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start(); ?><span <?php echo smarty_function_live_edit(array('name'=>"category:category:".((string)$_smarty_tpl->tpl_vars['category_data']->value['category_id'])),$_smarty_tpl);?>
><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['category_data']->value['category'], ENT_QUOTES, 'ISO-8859-1');?>
</span><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"categories:view"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);
}?><?php }} ?>
