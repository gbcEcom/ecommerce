<?php /* Smarty version Smarty-3.1.21, created on 2015-08-10 22:31:39
         compiled from "/home/gbadmin/public_html/production/design/themes/vivashop/templates/addons/social_buttons/providers/email_content.tpl" */ ?>
<?php /*%%SmartyHeaderCode:31187845355c8c3db94b7d8-48820403%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '274ac9144eab959cb2e0fcf6798b2da7ea11437f' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/themes/vivashop/templates/addons/social_buttons/providers/email_content.tpl',
      1 => 1438219698,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '31187845355c8c3db94b7d8-48820403',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'addons' => 0,
    'provider_settings' => 0,
    'product_tab_id' => 0,
    'config' => 0,
    'send_data' => 0,
    'auth' => 0,
    'user_info' => 0,
    'product' => 0,
    'page' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55c8c3dba99a59_73725066',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55c8c3dba99a59_73725066')) {function content_55c8c3dba99a59_73725066($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/function.set_id.php';
?><?php
fn_preload_lang_vars(array('share_via_email','name_of_friend','email_of_friend','your_name','your_email','your_message','send','share_via_email','name_of_friend','email_of_friend','your_name','your_email','your_message','send'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['addons']->value['social_buttons']['email_enable']=="Y"&&$_smarty_tpl->tpl_vars['provider_settings']->value['email']['data']) {?>

<div class="hidden" id="content_elm_email_sharing" title="<?php echo $_smarty_tpl->__("share_via_email");?>
">
    <form name="email_share_form" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'ISO-8859-1');?>
" method="post">
    <input type="hidden" name="selected_section" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_tab_id']->value, ENT_QUOTES, 'ISO-8859-1');?>
" />
    <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_url'], ENT_QUOTES, 'ISO-8859-1');?>
" />

    <div class="control-group">
        <label for="send_name"><?php echo $_smarty_tpl->__("name_of_friend");?>
</label>
        <input id="send_name" class="input-text" size="50" type="text" name="send_data[to_name]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['send_data']->value['to_name'], ENT_QUOTES, 'ISO-8859-1');?>
" />
    </div>

    <div class="control-group">
        <label for="send_email" class="cm-required cm-email"><?php echo $_smarty_tpl->__("email_of_friend");?>
</label>
        <input id="send_email" class="input-text" size="50" type="text" name="send_data[to_email]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['send_data']->value['to_email'], ENT_QUOTES, 'ISO-8859-1');?>
" />
    </div>

    <div class="control-group">
        <label for="send_yourname"><?php echo $_smarty_tpl->__("your_name");?>
</label>
        <input id="send_yourname" size="50" class="input-text" type="text" name="send_data[from_name]" value="<?php if ($_smarty_tpl->tpl_vars['send_data']->value['from_name']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['send_data']->value['from_name'], ENT_QUOTES, 'ISO-8859-1');
} elseif ($_smarty_tpl->tpl_vars['auth']->value['user_id']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['user_info']->value['firstname'], ENT_QUOTES, 'ISO-8859-1');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_info']->value['lastname'], ENT_QUOTES, 'ISO-8859-1');
}?>" />
    </div>

    <div class="control-group">
        <label for="send_youremail" class="cm-email"><?php echo $_smarty_tpl->__("your_email");?>
</label>
        <input id="send_youremail" class="input-text" size="50" type="text" name="send_data[from_email]" value="<?php if ($_smarty_tpl->tpl_vars['send_data']->value['from_email']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['send_data']->value['from_email'], ENT_QUOTES, 'ISO-8859-1');
} elseif ($_smarty_tpl->tpl_vars['auth']->value['user_id']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['user_info']->value['email'], ENT_QUOTES, 'ISO-8859-1');
}?>" />
    </div>

    <div class="control-group">
        <label for="send_notes" class="cm-required"><?php echo $_smarty_tpl->__("your_message");?>
</label>
        <textarea id="send_notes"  class="input-textarea" rows="5" cols="72" name="send_data[notes]"><?php if ($_smarty_tpl->tpl_vars['send_data']->value['notes']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['send_data']->value['notes'], ENT_QUOTES, 'ISO-8859-1');
} elseif ($_smarty_tpl->tpl_vars['product']->value) {
echo $_smarty_tpl->tpl_vars['product']->value['product'];
} elseif ($_smarty_tpl->tpl_vars['page']->value) {
echo $_smarty_tpl->tpl_vars['page']->value['page'];
}?></textarea>
    </div>

    <?php echo $_smarty_tpl->getSubTemplate ("common/image_verification.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('option'=>"email_share",'align'=>"left"), 0);?>


    <div class="buttons-container">
        <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_text'=>__("send"),'but_name'=>"dispatch[share_by_email.send]",'but_role'=>"submit"), 0);?>

    </div>

    </form>
</div>

<?php }
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/social_buttons/providers/email_content.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/social_buttons/providers/email_content.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['addons']->value['social_buttons']['email_enable']=="Y"&&$_smarty_tpl->tpl_vars['provider_settings']->value['email']['data']) {?>

<div class="hidden" id="content_elm_email_sharing" title="<?php echo $_smarty_tpl->__("share_via_email");?>
">
    <form name="email_share_form" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'ISO-8859-1');?>
" method="post">
    <input type="hidden" name="selected_section" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product_tab_id']->value, ENT_QUOTES, 'ISO-8859-1');?>
" />
    <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_url'], ENT_QUOTES, 'ISO-8859-1');?>
" />

    <div class="control-group">
        <label for="send_name"><?php echo $_smarty_tpl->__("name_of_friend");?>
</label>
        <input id="send_name" class="input-text" size="50" type="text" name="send_data[to_name]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['send_data']->value['to_name'], ENT_QUOTES, 'ISO-8859-1');?>
" />
    </div>

    <div class="control-group">
        <label for="send_email" class="cm-required cm-email"><?php echo $_smarty_tpl->__("email_of_friend");?>
</label>
        <input id="send_email" class="input-text" size="50" type="text" name="send_data[to_email]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['send_data']->value['to_email'], ENT_QUOTES, 'ISO-8859-1');?>
" />
    </div>

    <div class="control-group">
        <label for="send_yourname"><?php echo $_smarty_tpl->__("your_name");?>
</label>
        <input id="send_yourname" size="50" class="input-text" type="text" name="send_data[from_name]" value="<?php if ($_smarty_tpl->tpl_vars['send_data']->value['from_name']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['send_data']->value['from_name'], ENT_QUOTES, 'ISO-8859-1');
} elseif ($_smarty_tpl->tpl_vars['auth']->value['user_id']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['user_info']->value['firstname'], ENT_QUOTES, 'ISO-8859-1');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_info']->value['lastname'], ENT_QUOTES, 'ISO-8859-1');
}?>" />
    </div>

    <div class="control-group">
        <label for="send_youremail" class="cm-email"><?php echo $_smarty_tpl->__("your_email");?>
</label>
        <input id="send_youremail" class="input-text" size="50" type="text" name="send_data[from_email]" value="<?php if ($_smarty_tpl->tpl_vars['send_data']->value['from_email']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['send_data']->value['from_email'], ENT_QUOTES, 'ISO-8859-1');
} elseif ($_smarty_tpl->tpl_vars['auth']->value['user_id']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['user_info']->value['email'], ENT_QUOTES, 'ISO-8859-1');
}?>" />
    </div>

    <div class="control-group">
        <label for="send_notes" class="cm-required"><?php echo $_smarty_tpl->__("your_message");?>
</label>
        <textarea id="send_notes"  class="input-textarea" rows="5" cols="72" name="send_data[notes]"><?php if ($_smarty_tpl->tpl_vars['send_data']->value['notes']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['send_data']->value['notes'], ENT_QUOTES, 'ISO-8859-1');
} elseif ($_smarty_tpl->tpl_vars['product']->value) {
echo $_smarty_tpl->tpl_vars['product']->value['product'];
} elseif ($_smarty_tpl->tpl_vars['page']->value) {
echo $_smarty_tpl->tpl_vars['page']->value['page'];
}?></textarea>
    </div>

    <?php echo $_smarty_tpl->getSubTemplate ("common/image_verification.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('option'=>"email_share",'align'=>"left"), 0);?>


    <div class="buttons-container">
        <?php echo $_smarty_tpl->getSubTemplate ("buttons/button.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_text'=>__("send"),'but_name'=>"dispatch[share_by_email.send]",'but_role'=>"submit"), 0);?>

    </div>

    </form>
</div>

<?php }
}?><?php }} ?>
