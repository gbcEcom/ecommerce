<?php /* Smarty version Smarty-3.1.21, created on 2015-09-22 09:50:02
         compiled from "/home/gbadmin/public_html/production/design/themes/vivashop/templates/views/checkout/components/biaya_pengiriman.tpl" */ ?>
<?php /*%%SmartyHeaderCode:22686751555c8c3a542b360-39471959%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '15e1a45082fd11dcd4749de1a9fc7a17fab529c4' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/themes/vivashop/templates/views/checkout/components/biaya_pengiriman.tpl',
      1 => 1442890198,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '22686751555c8c3a542b360-39471959',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55c8c3a54de355_03746498',
  'variables' => 
  array (
    'runtime' => 0,
    'user_data' => 0,
    'company' => 0,
    'row' => 0,
    'tipe' => 0,
    'berat' => 0,
    'user_id_kirim' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55c8c3a54de355_03746498')) {function content_55c8c3a54de355_03746498($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/function.set_id.php';
?><?php
fn_preload_lang_vars(array('delivery_times_text','delivery_times_text'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="ty-cart-content__top-buttons clearfix"  id="contentKurir" style="display:none;">
    <input type="hidden" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_data']->value['s_city'], ENT_QUOTES, 'ISO-8859-1');?>
" id="tujuan">
    <div class="ty-float-left ty-cart-content__left-buttons">
        <select class="ty-btn ty-btn__secondary " style="height: 34px" id="company">
            <option value="" selected="selected">- pilih kurir -</option>
            <?php  $_smarty_tpl->tpl_vars['row'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['row']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['company']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['row']->key => $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
?>
                <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['row']->value['id'], ENT_QUOTES, 'ISO-8859-1');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['row']->value['perusahaan'], ENT_QUOTES, 'ISO-8859-1');?>
</option>
            <?php } ?>
        </select>
    </div>
    <div class="ty-float-left ty-cart-content__left-buttons">
        <select class="ty-btn ty-btn__secondary " style="height: 34px" id="tipe" >
            <option value="" selected="selected">- tipe pengiriman -</option>
            <?php  $_smarty_tpl->tpl_vars['row'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['row']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tipe']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['row']->key => $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
?>
                <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['row']->value['id'], ENT_QUOTES, 'ISO-8859-1');?>
" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['row']->value['company_id'], ENT_QUOTES, 'ISO-8859-1');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['row']->value['tipe'], ENT_QUOTES, 'ISO-8859-1');?>
</option>
            <?php } ?>
        </select>
    </div>
    <div class="ty-float-left ty-cart-content__left-buttons">
        <div class="ty-btn ty-btn__secondary">
            <!-- kun -->
            <input type="hidden" id="beratBarang" value="<?php echo htmlspecialchars(ceil($_smarty_tpl->tpl_vars['berat']->value), ENT_QUOTES, 'ISO-8859-1');?>
"/>
            berat <b id="weight"><?php echo htmlspecialchars(ceil($_smarty_tpl->tpl_vars['berat']->value), ENT_QUOTES, 'ISO-8859-1');?>
</b> kg 
        </div>
    </div>
    <input type="hidden" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_id_kirim']->value['user_id'], ENT_QUOTES, 'ISO-8859-1');?>
" id="user_id" name="user_id">
    <div class="ty-float-right ty-cart-content__right-buttons">
        <a id="submit_kirim" onclick="biaya_kirim()" class="ty-btn ty-btn__primary">
            CEK Biaya Pengiriman
        </a>
    </div>
</div>
    
        <?php echo '<script'; ?>
>
            $(function (){
		if($('#tujuan').val() === '' || $('#company').val() === '' || $('#tipe').val() ===''){
		    $("button[id='step_three_but']").css("display","none");
		    $("div[id='step_four']").css("display","none");
		} 
                $('#tipe').chained('#company');
                $("#radioJasa").on("click", function (){
                    $("#shipping_rates_list").fadeOut();
                    $("#contentKurir").fadeIn();
$("button[id='step_three_but']").hide();
                });
                $("#radioManual").on("click", function (){
                    $("#contentKurir").fadeOut();
                    $("#shipping_rates_list").fadeIn();
                    $("button[id='step_three_but']").show();
                });
            });
            
            function biaya_kirim()
            {
                var company = $('#company').val();
                var tipe = $('#tipe').val();
                var tujuan = $('#tujuan').val();
                var berat = $('#beratBarang').val();
                var user_id = $('#user_id').val();
                        
                //alert( company + '\n' + tipe + '\n' + tujuan + '\n' + berat );
                
                if(company === '' || tipe === '' || tujuan === '' ){
                    alert("gagal");
                }else{
	     	    $("button[id='step_three_but']").show();
                    $("#container_kirim").show();
                    
                    if($('#company').val() == 4){
                        var gambar = "http://localhost/gbc_bug/images/jne.gif";
                    } else {
                        var gambar = "http://localhost/gbc_bug/images/tiki.gif";
                    }
                    
                    $.ajax({
                       type: 'GET',
                       url: fn_url('checkout.select_biaya'),
                       dataType: 'json',
                       data: {
                            'company' : company ,
                            'tipe' : tipe,
                            'tujuan' : tujuan,
                            'berat' : berat,
                            'user_id' : user_id
                       },
                       success: function (data) {
		           //alert(JSON.stringify(data));
                           console.log(data.result);
                           $(".text_kurir").text($('#company option:selected').text() +" "+ $('#tipe option:selected').text()).formatCurrency({decimalSymbol:',',roundToDecimalPlace:0,symbol:'Rp '});
                           $("#text_tujuan").text(data.result.nama_kota);
                           $("#text_waktu").text(data.result.estimasi);
                           $("#text_barang").text(berat);
                           $("#text_kirim").text(data.result.biaya_pengiriman).formatCurrency({decimalSymbol:',',roundToDecimalPlace:0,symbol:'Rp '});
                           $('#text_gambar').attr('src', gambar);
                           $('#text_total_kirim').text(berat * data.result.biaya_pengiriman).formatCurrency({decimalSymbol:',',roundToDecimalPlace:0,symbol:'Rp '});
                        }
                    });
                }
                return false;
            }
            
            biaya_manual = function ()
            {
                var user_id = $('#user_id').val();
                $("#container_kirim").hide();
                //alert(manual + user_id);
                $.ajax({
                    type: 'GET',
                    url: fn_url('checkout.biaya_manual'),
                    data:{
                        'user_id' : user_id
                    },
                    success: function (data) {
                        //alert(data);
                    }
                });
            }
        <?php echo '</script'; ?>
>
    
           
    <div id="container_kirim" style="display: none; margin-top:25px;">
        <table width="35%">
            <tr>
                <th colspan="2"><h3>Anda Memakai Jasa Kurir : <span class="text_kurir"></span></h3></th>
                <th></th>
            </tr>
            <tr>
                <th colspan="2"><img id="text_gambar" style="width: 100px;" src=""></th>
                <th></th>
            </tr>
            <tr>
                <td>Tujuan :</td>
                <td><span id="text_tujuan"></span></td>
            </tr>
            <tr>
                <td>Estimasi Waktu :</td>
                <td><span id="text_waktu"></span> hari</td>
            </tr>
            <tr>
                <td>Berat Barang :</td>
                <td><span id="text_barang"></span>kg</td>
            </tr>
            <tr>
                <td>Biaya Kirim/kg :</td>
                <td><span id="text_kirim"></span> <a target="_blank" style="text-decoration: underline;color: darkorange" href="http://www.jne.co.id/"><i class="text_kurir"></i></a></td>
            </tr>
            <tr>
                <td>Total Biaya Pengiriman :</td>
                <td><span id="text_total_kirim"></span></td>
            </tr>
        </table>
    </div>
        <div class="biaya_kirim" style="padding:20px;padding-bottom: 0px;display: none;"></div>
        <div class="text" style="padding:20px;display: none; padding-top: 0px;"><b>Biaya Pengiriman: </b><b class="number"></b></div>
        <p class="rule_pengiriman" style="
            /* padding: 0 20px; */
            background-color: red;
            color: white;
            padding: 5px 10px;
            text-align: center;
            text-transform: uppercase;
            display: none;">

            <?php echo $_smarty_tpl->__("delivery_times_text");?>

        </p>
        
        
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="views/checkout/components/biaya_pengiriman.tpl" id="<?php echo smarty_function_set_id(array('name'=>"views/checkout/components/biaya_pengiriman.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="ty-cart-content__top-buttons clearfix"  id="contentKurir" style="display:none;">
    <input type="hidden" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_data']->value['s_city'], ENT_QUOTES, 'ISO-8859-1');?>
" id="tujuan">
    <div class="ty-float-left ty-cart-content__left-buttons">
        <select class="ty-btn ty-btn__secondary " style="height: 34px" id="company">
            <option value="" selected="selected">- pilih kurir -</option>
            <?php  $_smarty_tpl->tpl_vars['row'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['row']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['company']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['row']->key => $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
?>
                <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['row']->value['id'], ENT_QUOTES, 'ISO-8859-1');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['row']->value['perusahaan'], ENT_QUOTES, 'ISO-8859-1');?>
</option>
            <?php } ?>
        </select>
    </div>
    <div class="ty-float-left ty-cart-content__left-buttons">
        <select class="ty-btn ty-btn__secondary " style="height: 34px" id="tipe" >
            <option value="" selected="selected">- tipe pengiriman -</option>
            <?php  $_smarty_tpl->tpl_vars['row'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['row']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tipe']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['row']->key => $_smarty_tpl->tpl_vars['row']->value) {
$_smarty_tpl->tpl_vars['row']->_loop = true;
?>
                <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['row']->value['id'], ENT_QUOTES, 'ISO-8859-1');?>
" class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['row']->value['company_id'], ENT_QUOTES, 'ISO-8859-1');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['row']->value['tipe'], ENT_QUOTES, 'ISO-8859-1');?>
</option>
            <?php } ?>
        </select>
    </div>
    <div class="ty-float-left ty-cart-content__left-buttons">
        <div class="ty-btn ty-btn__secondary">
            <!-- kun -->
            <input type="hidden" id="beratBarang" value="<?php echo htmlspecialchars(ceil($_smarty_tpl->tpl_vars['berat']->value), ENT_QUOTES, 'ISO-8859-1');?>
"/>
            berat <b id="weight"><?php echo htmlspecialchars(ceil($_smarty_tpl->tpl_vars['berat']->value), ENT_QUOTES, 'ISO-8859-1');?>
</b> kg 
        </div>
    </div>
    <input type="hidden" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_id_kirim']->value['user_id'], ENT_QUOTES, 'ISO-8859-1');?>
" id="user_id" name="user_id">
    <div class="ty-float-right ty-cart-content__right-buttons">
        <a id="submit_kirim" onclick="biaya_kirim()" class="ty-btn ty-btn__primary">
            CEK Biaya Pengiriman
        </a>
    </div>
</div>
    
        <?php echo '<script'; ?>
>
            $(function (){
		if($('#tujuan').val() === '' || $('#company').val() === '' || $('#tipe').val() ===''){
		    $("button[id='step_three_but']").css("display","none");
		    $("div[id='step_four']").css("display","none");
		} 
                $('#tipe').chained('#company');
                $("#radioJasa").on("click", function (){
                    $("#shipping_rates_list").fadeOut();
                    $("#contentKurir").fadeIn();
$("button[id='step_three_but']").hide();
                });
                $("#radioManual").on("click", function (){
                    $("#contentKurir").fadeOut();
                    $("#shipping_rates_list").fadeIn();
                    $("button[id='step_three_but']").show();
                });
            });
            
            function biaya_kirim()
            {
                var company = $('#company').val();
                var tipe = $('#tipe').val();
                var tujuan = $('#tujuan').val();
                var berat = $('#beratBarang').val();
                var user_id = $('#user_id').val();
                        
                //alert( company + '\n' + tipe + '\n' + tujuan + '\n' + berat );
                
                if(company === '' || tipe === '' || tujuan === '' ){
                    alert("gagal");
                }else{
	     	    $("button[id='step_three_but']").show();
                    $("#container_kirim").show();
                    
                    if($('#company').val() == 4){
                        var gambar = "http://localhost/gbc_bug/images/jne.gif";
                    } else {
                        var gambar = "http://localhost/gbc_bug/images/tiki.gif";
                    }
                    
                    $.ajax({
                       type: 'GET',
                       url: fn_url('checkout.select_biaya'),
                       dataType: 'json',
                       data: {
                            'company' : company ,
                            'tipe' : tipe,
                            'tujuan' : tujuan,
                            'berat' : berat,
                            'user_id' : user_id
                       },
                       success: function (data) {
		           //alert(JSON.stringify(data));
                           console.log(data.result);
                           $(".text_kurir").text($('#company option:selected').text() +" "+ $('#tipe option:selected').text()).formatCurrency({decimalSymbol:',',roundToDecimalPlace:0,symbol:'Rp '});
                           $("#text_tujuan").text(data.result.nama_kota);
                           $("#text_waktu").text(data.result.estimasi);
                           $("#text_barang").text(berat);
                           $("#text_kirim").text(data.result.biaya_pengiriman).formatCurrency({decimalSymbol:',',roundToDecimalPlace:0,symbol:'Rp '});
                           $('#text_gambar').attr('src', gambar);
                           $('#text_total_kirim').text(berat * data.result.biaya_pengiriman).formatCurrency({decimalSymbol:',',roundToDecimalPlace:0,symbol:'Rp '});
                        }
                    });
                }
                return false;
            }
            
            biaya_manual = function ()
            {
                var user_id = $('#user_id').val();
                $("#container_kirim").hide();
                //alert(manual + user_id);
                $.ajax({
                    type: 'GET',
                    url: fn_url('checkout.biaya_manual'),
                    data:{
                        'user_id' : user_id
                    },
                    success: function (data) {
                        //alert(data);
                    }
                });
            }
        <?php echo '</script'; ?>
>
    
           
    <div id="container_kirim" style="display: none; margin-top:25px;">
        <table width="35%">
            <tr>
                <th colspan="2"><h3>Anda Memakai Jasa Kurir : <span class="text_kurir"></span></h3></th>
                <th></th>
            </tr>
            <tr>
                <th colspan="2"><img id="text_gambar" style="width: 100px;" src=""></th>
                <th></th>
            </tr>
            <tr>
                <td>Tujuan :</td>
                <td><span id="text_tujuan"></span></td>
            </tr>
            <tr>
                <td>Estimasi Waktu :</td>
                <td><span id="text_waktu"></span> hari</td>
            </tr>
            <tr>
                <td>Berat Barang :</td>
                <td><span id="text_barang"></span>kg</td>
            </tr>
            <tr>
                <td>Biaya Kirim/kg :</td>
                <td><span id="text_kirim"></span> <a target="_blank" style="text-decoration: underline;color: darkorange" href="http://www.jne.co.id/"><i class="text_kurir"></i></a></td>
            </tr>
            <tr>
                <td>Total Biaya Pengiriman :</td>
                <td><span id="text_total_kirim"></span></td>
            </tr>
        </table>
    </div>
        <div class="biaya_kirim" style="padding:20px;padding-bottom: 0px;display: none;"></div>
        <div class="text" style="padding:20px;display: none; padding-top: 0px;"><b>Biaya Pengiriman: </b><b class="number"></b></div>
        <p class="rule_pengiriman" style="
            /* padding: 0 20px; */
            background-color: red;
            color: white;
            padding: 5px 10px;
            text-align: center;
            text-transform: uppercase;
            display: none;">

            <?php echo $_smarty_tpl->__("delivery_times_text");?>

        </p>
        
        
<?php }?><?php }} ?>
