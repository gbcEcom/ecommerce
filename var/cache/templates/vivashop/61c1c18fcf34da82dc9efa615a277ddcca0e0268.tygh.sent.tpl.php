<?php /* Smarty version Smarty-3.1.21, created on 2015-09-23 07:36:21
         compiled from "/home/gbadmin/public_html/production/design/themes/vivashop/templates/views/messages/sent.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16371905165601f405c37641-64003883%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '61c1c18fcf34da82dc9efa615a277ddcca0e0268' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/themes/vivashop/templates/views/messages/sent.tpl',
      1 => 1438219702,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '16371905165601f405c37641-64003883',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'messages' => 0,
    'message' => 0,
    '_title' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5601f405d22059_23488405',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5601f405d22059_23488405')) {function content_5601f405d22059_23488405($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
$_smarty_tpl->tpl_vars["_title"] = new Smarty_variable("Inbox", null, 0);?>
<div class="message-box" style="margin-bottom: 15px">
    <table class="ty-table" style="margin-top: 0px !important;">
        <thead>
        <tr>
            <th width="25%">Penerima</th>
            <th width="55%">Pesan</th>
            <th width="20%">Tanggal</th>
        </tr>
        </thead>

        <tbody>
        <?php if (count($_smarty_tpl->tpl_vars['messages']->value)) {?>
            <?php  $_smarty_tpl->tpl_vars["message"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["message"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['messages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["message"]->key => $_smarty_tpl->tpl_vars["message"]->value) {
$_smarty_tpl->tpl_vars["message"]->_loop = true;
?>
                <tr style="cursor: pointer">
                    <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['company'], ENT_QUOTES, 'ISO-8859-1');?>
</td>
                    <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['message'], ENT_QUOTES, 'ISO-8859-1');?>
</td>
                    <td><?php echo htmlspecialchars(date('F d, Y H:i',$_smarty_tpl->tpl_vars['message']->value['timestamp']), ENT_QUOTES, 'ISO-8859-1');?>
</td>
                </tr>
            <?php } ?>
        <?php } else { ?>
            <tr>
                <td colspan="3">Data pesan belum ada !</td>
            </tr>
        <?php }?>
        </tbody>
    </table>
</div>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo htmlspecialchars($_smarty_tpl->tpl_vars['_title']->value, ENT_QUOTES, 'ISO-8859-1');
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="views/messages/sent.tpl" id="<?php echo smarty_function_set_id(array('name'=>"views/messages/sent.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
$_smarty_tpl->tpl_vars["_title"] = new Smarty_variable("Inbox", null, 0);?>
<div class="message-box" style="margin-bottom: 15px">
    <table class="ty-table" style="margin-top: 0px !important;">
        <thead>
        <tr>
            <th width="25%">Penerima</th>
            <th width="55%">Pesan</th>
            <th width="20%">Tanggal</th>
        </tr>
        </thead>

        <tbody>
        <?php if (count($_smarty_tpl->tpl_vars['messages']->value)) {?>
            <?php  $_smarty_tpl->tpl_vars["message"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["message"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['messages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["message"]->key => $_smarty_tpl->tpl_vars["message"]->value) {
$_smarty_tpl->tpl_vars["message"]->_loop = true;
?>
                <tr style="cursor: pointer">
                    <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['company'], ENT_QUOTES, 'ISO-8859-1');?>
</td>
                    <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['message'], ENT_QUOTES, 'ISO-8859-1');?>
</td>
                    <td><?php echo htmlspecialchars(date('F d, Y H:i',$_smarty_tpl->tpl_vars['message']->value['timestamp']), ENT_QUOTES, 'ISO-8859-1');?>
</td>
                </tr>
            <?php } ?>
        <?php } else { ?>
            <tr>
                <td colspan="3">Data pesan belum ada !</td>
            </tr>
        <?php }?>
        </tbody>
    </table>
</div>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo htmlspecialchars($_smarty_tpl->tpl_vars['_title']->value, ENT_QUOTES, 'ISO-8859-1');
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
}?><?php }} ?>
