<?php /* Smarty version Smarty-3.1.21, created on 2015-08-10 22:52:22
         compiled from "/home/gbadmin/public_html/production/design/themes/vivashop/templates/addons/blog/hooks/pages/page_extra.pre.tpl" */ ?>
<?php /*%%SmartyHeaderCode:98341071655c8c8b68ab5f6-96357471%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a5aeec616868d3ca082e641004ad3488502b87b8' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/themes/vivashop/templates/addons/blog/hooks/pages/page_extra.pre.tpl',
      1 => 1438219694,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '98341071655c8c8b68ab5f6-96357471',
  'function' => 
  array (
    'list_blog' => 
    array (
      'parameter' => 
      array (
      ),
      'compiled' => '',
    ),
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'page' => 0,
    'subpages' => 0,
    'subpage' => 0,
    'settings' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => 0,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55c8c8b6a28819_14870668',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55c8c8b6a28819_14870668')) {function content_55c8c8b6a28819_14870668($_smarty_tpl) {?><?php if (!is_callable('smarty_function_live_edit')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/function.live_edit.php';
if (!is_callable('smarty_function_set_id')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/function.set_id.php';
?><?php
fn_preload_lang_vars(array('blog','blog.read_more','blog','blog.read_more'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['page']->value['page_type']==@constant('PAGE_TYPE_BLOG')) {?>

<?php if ($_smarty_tpl->tpl_vars['subpages']->value) {?>
    <?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo $_smarty_tpl->__("blog");
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
    
    <div class="vs-blog-list-main-image">
        <?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('image_width'=>"894",'obj_id'=>$_smarty_tpl->tpl_vars['page']->value['page_id'],'images'=>$_smarty_tpl->tpl_vars['page']->value['main_pair']), 0);?>

    </div>

    <div class="ty-blog vs-blog">
        <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

        
        <?php if (!is_callable('smarty_modifier_date_format')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/modifier.date_format.php';
if (!is_callable('smarty_block_hook')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/block.hook.php';
if (!is_callable('smarty_modifier_truncate')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/modifier.truncate.php';
?><?php if (!function_exists('smarty_template_function_list_blog')) {
    function smarty_template_function_list_blog($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->smarty->template_functions['list_blog']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?>
        <?php  $_smarty_tpl->tpl_vars["subpage"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["subpage"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['subpages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["subpage"]->key => $_smarty_tpl->tpl_vars["subpage"]->value) {
$_smarty_tpl->tpl_vars["subpage"]->_loop = true;
?>
            <div class="ty-blog__item vs-column3">
                <?php if ($_smarty_tpl->tpl_vars['subpage']->value['main_pair']) {?>
                <a href="<?php echo htmlspecialchars(fn_url("pages.view?page_id=".((string)$_smarty_tpl->tpl_vars['subpage']->value['page_id'])), ENT_QUOTES, 'ISO-8859-1');?>
" class="ty-center">
                    <span class="ty-blog__img-block">
                        <?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('image_width'=>"330",'image_height'=>"112",'obj_id'=>$_smarty_tpl->tpl_vars['subpage']->value['page_id'],'images'=>$_smarty_tpl->tpl_vars['subpage']->value['main_pair']), 0);?>

                    </span>
                </a>
                <?php }?>
                <div class="vs-blog-title">
                    <a href="<?php echo htmlspecialchars(fn_url("pages.view?page_id=".((string)$_smarty_tpl->tpl_vars['subpage']->value['page_id'])), ENT_QUOTES, 'ISO-8859-1');?>
">
                        <h2 class="ty-blog__post-title">
                            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subpage']->value['page'], ENT_QUOTES, 'ISO-8859-1');?>

                        </h2>
                    </a>
                </div>

                <div class="vs-blog-details">
                    <div class="ty-blog__author"><i class="vs-icon-user"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subpage']->value['author'], ENT_QUOTES, 'ISO-8859-1');?>
</div><div class="ty-blog__date"><i class="ty-icon-calendar"></i> <?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['subpage']->value['timestamp'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format'])), ENT_QUOTES, 'ISO-8859-1');?>
</div><?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"blog:list")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"blog:list"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();
$_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"blog:list"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

                </div>

                <div class="ty-blog__description">
                    <div class="ty-wysiwyg-content">
                        <div><?php echo smarty_modifier_truncate(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['subpage']->value['spoiler']),200,"...");?>
</div>
                    </div>
                    <div class="ty-blog__read-more ty-mt-l">
                        <a class="ty-btn ty-btn__secondary" href="<?php echo htmlspecialchars(fn_url("pages.view?page_id=".((string)$_smarty_tpl->tpl_vars['subpage']->value['page_id'])), ENT_QUOTES, 'ISO-8859-1');?>
"><?php echo $_smarty_tpl->__("blog.read_more");?>
</a>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;
foreach (Smarty::$global_tpl_vars as $key => $value) if(!isset($_smarty_tpl->tpl_vars[$key])) $_smarty_tpl->tpl_vars[$key] = $value;}}?>

        <?php smarty_template_function_list_blog($_smarty_tpl,array('subpages'=>$_smarty_tpl->tpl_vars['subpages']->value));?>


        <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    </div>

<?php }?>

<?php if ($_smarty_tpl->tpl_vars['page']->value['description']) {?>
    <?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start(); ?><span class="ty-blog__post-title" <?php echo smarty_function_live_edit(array('name'=>"page:page:".((string)$_smarty_tpl->tpl_vars['page']->value['page_id'])),$_smarty_tpl);?>
><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['page'], ENT_QUOTES, 'ISO-8859-1');?>
</span><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php }?>

<?php }
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/blog/hooks/pages/page_extra.pre.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/blog/hooks/pages/page_extra.pre.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['page']->value['page_type']==@constant('PAGE_TYPE_BLOG')) {?>

<?php if ($_smarty_tpl->tpl_vars['subpages']->value) {?>
    <?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo $_smarty_tpl->__("blog");
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
    
    <div class="vs-blog-list-main-image">
        <?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('image_width'=>"894",'obj_id'=>$_smarty_tpl->tpl_vars['page']->value['page_id'],'images'=>$_smarty_tpl->tpl_vars['page']->value['main_pair']), 0);?>

    </div>

    <div class="ty-blog vs-blog">
        <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

        
        <?php if (!is_callable('smarty_modifier_date_format')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/modifier.date_format.php';
if (!is_callable('smarty_block_hook')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/block.hook.php';
if (!is_callable('smarty_modifier_truncate')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/modifier.truncate.php';
?><?php if (!function_exists('smarty_template_function_list_blog')) {
    function smarty_template_function_list_blog($_smarty_tpl,$params) {
    $saved_tpl_vars = $_smarty_tpl->tpl_vars;
    foreach ($_smarty_tpl->smarty->template_functions['list_blog']['parameter'] as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);};
    foreach ($params as $key => $value) {$_smarty_tpl->tpl_vars[$key] = new Smarty_variable($value);}?>
        <?php  $_smarty_tpl->tpl_vars["subpage"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["subpage"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['subpages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["subpage"]->key => $_smarty_tpl->tpl_vars["subpage"]->value) {
$_smarty_tpl->tpl_vars["subpage"]->_loop = true;
?>
            <div class="ty-blog__item vs-column3">
                <?php if ($_smarty_tpl->tpl_vars['subpage']->value['main_pair']) {?>
                <a href="<?php echo htmlspecialchars(fn_url("pages.view?page_id=".((string)$_smarty_tpl->tpl_vars['subpage']->value['page_id'])), ENT_QUOTES, 'ISO-8859-1');?>
" class="ty-center">
                    <span class="ty-blog__img-block">
                        <?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('image_width'=>"330",'image_height'=>"112",'obj_id'=>$_smarty_tpl->tpl_vars['subpage']->value['page_id'],'images'=>$_smarty_tpl->tpl_vars['subpage']->value['main_pair']), 0);?>

                    </span>
                </a>
                <?php }?>
                <div class="vs-blog-title">
                    <a href="<?php echo htmlspecialchars(fn_url("pages.view?page_id=".((string)$_smarty_tpl->tpl_vars['subpage']->value['page_id'])), ENT_QUOTES, 'ISO-8859-1');?>
">
                        <h2 class="ty-blog__post-title">
                            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subpage']->value['page'], ENT_QUOTES, 'ISO-8859-1');?>

                        </h2>
                    </a>
                </div>

                <div class="vs-blog-details">
                    <div class="ty-blog__author"><i class="vs-icon-user"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subpage']->value['author'], ENT_QUOTES, 'ISO-8859-1');?>
</div><div class="ty-blog__date"><i class="ty-icon-calendar"></i> <?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['subpage']->value['timestamp'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format'])), ENT_QUOTES, 'ISO-8859-1');?>
</div><?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"blog:list")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"blog:list"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();
$_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"blog:list"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

                </div>

                <div class="ty-blog__description">
                    <div class="ty-wysiwyg-content">
                        <div><?php echo smarty_modifier_truncate(preg_replace('!<[^>]*?>!', ' ', $_smarty_tpl->tpl_vars['subpage']->value['spoiler']),200,"...");?>
</div>
                    </div>
                    <div class="ty-blog__read-more ty-mt-l">
                        <a class="ty-btn ty-btn__secondary" href="<?php echo htmlspecialchars(fn_url("pages.view?page_id=".((string)$_smarty_tpl->tpl_vars['subpage']->value['page_id'])), ENT_QUOTES, 'ISO-8859-1');?>
"><?php echo $_smarty_tpl->__("blog.read_more");?>
</a>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php $_smarty_tpl->tpl_vars = $saved_tpl_vars;
foreach (Smarty::$global_tpl_vars as $key => $value) if(!isset($_smarty_tpl->tpl_vars[$key])) $_smarty_tpl->tpl_vars[$key] = $value;}}?>

        <?php smarty_template_function_list_blog($_smarty_tpl,array('subpages'=>$_smarty_tpl->tpl_vars['subpages']->value));?>


        <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    </div>

<?php }?>

<?php if ($_smarty_tpl->tpl_vars['page']->value['description']) {?>
    <?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start(); ?><span class="ty-blog__post-title" <?php echo smarty_function_live_edit(array('name'=>"page:page:".((string)$_smarty_tpl->tpl_vars['page']->value['page_id'])),$_smarty_tpl);?>
><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['page'], ENT_QUOTES, 'ISO-8859-1');?>
</span><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php }?>

<?php }
}?><?php }} ?>
