<?php /* Smarty version Smarty-3.1.21, created on 2015-09-11 13:26:46
         compiled from "/home/gbadmin/public_html/production/design/themes/vivashop/templates/addons/hybrid_auth/views/auth/connect_social.tpl" */ ?>
<?php /*%%SmartyHeaderCode:94790818655f27426cbef45-35121932%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1ff32ab263384a6c209eabd688f43df598db71ee' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/themes/vivashop/templates/addons/hybrid_auth/views/auth/connect_social.tpl',
      1 => 1440053942,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '94790818655f27426cbef45-35121932',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'id' => 0,
    'config' => 0,
    'user_login' => 0,
    'image_verification' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55f27426de60b7_15156088',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55f27426de60b7_15156088')) {function content_55f27426de60b7_15156088($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/function.set_id.php';
?><?php
fn_preload_lang_vars(array('email','password','hybrid_auth.connect_social','email','password','hybrid_auth.connect_social'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
$_smarty_tpl->tpl_vars["id"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['id']->value)===null||$tmp==='' ? "main_login" : $tmp), null, 0);?>

<div class="ty-connect-social">
    <form name="connect-social" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'ISO-8859-1');?>
" method="post">
        <input type="hidden" name="return_url" value="<?php echo htmlspecialchars((($tmp = @$_REQUEST['return_url'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['config']->value['current_url'] : $tmp), ENT_QUOTES, 'ISO-8859-1');?>
" />
        <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_url'], ENT_QUOTES, 'ISO-8859-1');?>
" />
        <input type="hidden" name="user_login" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_login']->value, ENT_QUOTES, 'ISO-8859-1');?>
" />

        <div class="ty-control-group">
            <label for="login_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'ISO-8859-1');?>
" class="ty-login__filed-label ty-control-group__label cm-required cm-trim cm-email"><?php echo $_smarty_tpl->__("email");?>
</label>
            <input type="text" id="login_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'ISO-8859-1');?>
" name="user_login" size="30" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_login']->value, ENT_QUOTES, 'ISO-8859-1');?>
" class="ty-login__input"/>
        </div>

        <div class="ty-control-group password-forgot">
            <label for="psw_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'ISO-8859-1');?>
" class="ty-login__filed-label ty-control-group__label ty-password-forgot__label cm-required "><?php echo $_smarty_tpl->__("password");?>
</label>
            <input type="password" id="psw_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'ISO-8859-1');?>
" name="password" size="30" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['demo_password'], ENT_QUOTES, 'ISO-8859-1');?>
" class="ty-login__input" maxlength="32" />
        </div>

        <?php $_smarty_tpl->tpl_vars["image_verification"] = new Smarty_variable($_smarty_tpl->getSubTemplate ("common/image_verification.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('option'=>"register",'align'=>"left"), 0));?>

        <?php if ($_smarty_tpl->tpl_vars['image_verification']->value) {?>
            <div class="ty-control-group">
                <?php echo $_smarty_tpl->tpl_vars['image_verification']->value;?>

            </div>
        <?php }?>

        <div class="buttons-container clearfix">
            <div class="ty-float-right">
                <?php echo $_smarty_tpl->getSubTemplate ("buttons/login.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[auth.login]",'but_role'=>"submit"), 0);?>

            </div>
        </div>
    </form>
</div>
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo $_smarty_tpl->__("hybrid_auth.connect_social");
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/hybrid_auth/views/auth/connect_social.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/hybrid_auth/views/auth/connect_social.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
$_smarty_tpl->tpl_vars["id"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['id']->value)===null||$tmp==='' ? "main_login" : $tmp), null, 0);?>

<div class="ty-connect-social">
    <form name="connect-social" action="<?php echo htmlspecialchars(fn_url(''), ENT_QUOTES, 'ISO-8859-1');?>
" method="post">
        <input type="hidden" name="return_url" value="<?php echo htmlspecialchars((($tmp = @$_REQUEST['return_url'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['config']->value['current_url'] : $tmp), ENT_QUOTES, 'ISO-8859-1');?>
" />
        <input type="hidden" name="redirect_url" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['current_url'], ENT_QUOTES, 'ISO-8859-1');?>
" />
        <input type="hidden" name="user_login" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_login']->value, ENT_QUOTES, 'ISO-8859-1');?>
" />

        <div class="ty-control-group">
            <label for="login_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'ISO-8859-1');?>
" class="ty-login__filed-label ty-control-group__label cm-required cm-trim cm-email"><?php echo $_smarty_tpl->__("email");?>
</label>
            <input type="text" id="login_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'ISO-8859-1');?>
" name="user_login" size="30" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_login']->value, ENT_QUOTES, 'ISO-8859-1');?>
" class="ty-login__input"/>
        </div>

        <div class="ty-control-group password-forgot">
            <label for="psw_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'ISO-8859-1');?>
" class="ty-login__filed-label ty-control-group__label ty-password-forgot__label cm-required "><?php echo $_smarty_tpl->__("password");?>
</label>
            <input type="password" id="psw_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'ISO-8859-1');?>
" name="password" size="30" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['config']->value['demo_password'], ENT_QUOTES, 'ISO-8859-1');?>
" class="ty-login__input" maxlength="32" />
        </div>

        <?php $_smarty_tpl->tpl_vars["image_verification"] = new Smarty_variable($_smarty_tpl->getSubTemplate ("common/image_verification.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('option'=>"register",'align'=>"left"), 0));?>

        <?php if ($_smarty_tpl->tpl_vars['image_verification']->value) {?>
            <div class="ty-control-group">
                <?php echo $_smarty_tpl->tpl_vars['image_verification']->value;?>

            </div>
        <?php }?>

        <div class="buttons-container clearfix">
            <div class="ty-float-right">
                <?php echo $_smarty_tpl->getSubTemplate ("buttons/login.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('but_name'=>"dispatch[auth.login]",'but_role'=>"submit"), 0);?>

            </div>
        </div>
    </form>
</div>
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo $_smarty_tpl->__("hybrid_auth.connect_social");
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
}?><?php }} ?>
