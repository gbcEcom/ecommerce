<?php /* Smarty version Smarty-3.1.21, created on 2015-08-10 22:32:04
         compiled from "/home/gbadmin/public_html/production/design/themes/vivashop/templates/blocks/vendors/vendor_informations.tpl" */ ?>
<?php /*%%SmartyHeaderCode:138344447255c8c3f41079c1-90485001%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2d41057a42673237b56576b1e8a0369a6b188627' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/themes/vivashop/templates/blocks/vendors/vendor_informations.tpl',
      1 => 1438219693,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '138344447255c8c3f41079c1-90485001',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'company_data' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55c8c3f41d5ff2_46240871',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55c8c3f41d5ff2_46240871')) {function content_55c8c3f41d5ff2_46240871($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?>

<div class="">
    <div class="row-fluid">
        <div class="span8">
            <h1>
                <span style="color: #008000; font-size: 24px"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company_data']->value['company'], ENT_QUOTES, 'ISO-8859-1');?>
</span>
            </h1>
        </div>
    </div>
    <div class="row-fluid shop-header-middle" style="padding: 10px 0px; border-top: 1px dotted #dedede; border-bottom: 1px dotted #dedede">
        <div class="span10">
            <div style="padding-top: 5px">
                <span style="margin-right: 20px"><i class="fa fa-clock-o"></i> Hari ini</span>
                <span style="margin-right: 20px"><i class="fa fa-map-marker"></i> Jakarta</span>
                <span style="margin-right: 20px"><i class="fa fa-home"></i> Hanya online</span>
                <span style="margin-right: 20px"><i class="fa fa-bank"></i> <?php echo htmlspecialchars(date('F Y',$_smarty_tpl->tpl_vars['company_data']->value['timestamp']), ENT_QUOTES, 'ISO-8859-1');?>
</span>
            </div>
        </div>
        <div class="span6 hidden-phone">
            <div style="float: right !important;">
                <a href="index.php?dispatch=messages.compose&company=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company_data']->value['company_id'], ENT_QUOTES, 'ISO-8859-1');?>
" class="ty-btn ty-btn__primary" style="padding: 4px; font-size: 10px; font-weight: bold; cursor: pointer"><i class="fa fa-envelope" style="top:0"></i> Kirim Pesan</a>
                <?php if (is_null($_smarty_tpl->tpl_vars['company_data']->value['is_favorite'])||empty($_smarty_tpl->tpl_vars['company_data']->value['is_favorite'])||$_smarty_tpl->tpl_vars['company_data']->value['is_favorite']==0) {?>
                    <?php $_smarty_tpl->tpl_vars["company_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['company_data']->value['company_id'], null, 0);?>
                    <a href="<?php echo htmlspecialchars(fn_url("companies.favorite?company_id=".((string)$_smarty_tpl->tpl_vars['company_id']->value)), ENT_QUOTES, 'ISO-8859-1');?>
" class="ty-btn ty-btn__primary" style="padding: 4px; font-size: 10px; font-weight: bold"><i class="fa fa-heart" style="top:0"></i> Jadikan Favorit</a>
                <?php } else { ?>
                    <a href="#" class="ty-btn ty-btn__primary" style="padding: 4px; font-size: 10px; font-weight: bold"><i class="fa fa-heart" style="top:0"></i> Jadikan Favorit</a>
                <?php }?>

            </div>
        </div>
    </div>
    <div class="row-fluid shop-header-bottom hidden-phone" style="padding-top: 15px">
        <div class="span6">
            <div style="font-size: 12px;"><?php echo $_smarty_tpl->tpl_vars['company_data']->value['company_description'];?>
</div>
        </div>
        <div class="span10" style="background-color: #f9f9f9; padding: 15px; border: 1px solid #dedede">
            <div class="row-fluid">
                <div class="span4" style="text-align: center">
                    <div style="font-size: 18px; color: #008000;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company_data']->value['success_orders'], ENT_QUOTES, 'ISO-8859-1');?>
</div>
                    <small>Transaksi Berhasil</small>
                </div>
                <div class="span4" style="text-align: center">
                    <div style="font-size: 18px; color: #008000;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company_data']->value['product_sold'], ENT_QUOTES, 'ISO-8859-1');?>
</div>
                    <small>Produk Terjual</small>
                </div>
                <div class="span4" style="text-align: center">
                    <div style="font-size: 18px; color: #008000;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company_data']->value['total_products'], ENT_QUOTES, 'ISO-8859-1');?>
</div>
                    <small>Total Produk</small>
                </div>
                <div class="span4" style="text-align: center">
                    <div style="font-size: 18px; color: #008000;"><?php echo htmlspecialchars(empty($_smarty_tpl->tpl_vars['company_data']->value['follower']) ? '-' : $_smarty_tpl->tpl_vars['company_data']->value['follower'], ENT_QUOTES, 'ISO-8859-1');?>
</div>
                    <div><i class="fa fa-heart"></i></div>
                </div>
            </div>
        </div>
    </div>
</div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="blocks/vendors/vendor_informations.tpl" id="<?php echo smarty_function_set_id(array('name'=>"blocks/vendors/vendor_informations.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?>

<div class="">
    <div class="row-fluid">
        <div class="span8">
            <h1>
                <span style="color: #008000; font-size: 24px"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company_data']->value['company'], ENT_QUOTES, 'ISO-8859-1');?>
</span>
            </h1>
        </div>
    </div>
    <div class="row-fluid shop-header-middle" style="padding: 10px 0px; border-top: 1px dotted #dedede; border-bottom: 1px dotted #dedede">
        <div class="span10">
            <div style="padding-top: 5px">
                <span style="margin-right: 20px"><i class="fa fa-clock-o"></i> Hari ini</span>
                <span style="margin-right: 20px"><i class="fa fa-map-marker"></i> Jakarta</span>
                <span style="margin-right: 20px"><i class="fa fa-home"></i> Hanya online</span>
                <span style="margin-right: 20px"><i class="fa fa-bank"></i> <?php echo htmlspecialchars(date('F Y',$_smarty_tpl->tpl_vars['company_data']->value['timestamp']), ENT_QUOTES, 'ISO-8859-1');?>
</span>
            </div>
        </div>
        <div class="span6 hidden-phone">
            <div style="float: right !important;">
                <a href="index.php?dispatch=messages.compose&company=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company_data']->value['company_id'], ENT_QUOTES, 'ISO-8859-1');?>
" class="ty-btn ty-btn__primary" style="padding: 4px; font-size: 10px; font-weight: bold; cursor: pointer"><i class="fa fa-envelope" style="top:0"></i> Kirim Pesan</a>
                <?php if (is_null($_smarty_tpl->tpl_vars['company_data']->value['is_favorite'])||empty($_smarty_tpl->tpl_vars['company_data']->value['is_favorite'])||$_smarty_tpl->tpl_vars['company_data']->value['is_favorite']==0) {?>
                    <?php $_smarty_tpl->tpl_vars["company_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['company_data']->value['company_id'], null, 0);?>
                    <a href="<?php echo htmlspecialchars(fn_url("companies.favorite?company_id=".((string)$_smarty_tpl->tpl_vars['company_id']->value)), ENT_QUOTES, 'ISO-8859-1');?>
" class="ty-btn ty-btn__primary" style="padding: 4px; font-size: 10px; font-weight: bold"><i class="fa fa-heart" style="top:0"></i> Jadikan Favorit</a>
                <?php } else { ?>
                    <a href="#" class="ty-btn ty-btn__primary" style="padding: 4px; font-size: 10px; font-weight: bold"><i class="fa fa-heart" style="top:0"></i> Jadikan Favorit</a>
                <?php }?>

            </div>
        </div>
    </div>
    <div class="row-fluid shop-header-bottom hidden-phone" style="padding-top: 15px">
        <div class="span6">
            <div style="font-size: 12px;"><?php echo $_smarty_tpl->tpl_vars['company_data']->value['company_description'];?>
</div>
        </div>
        <div class="span10" style="background-color: #f9f9f9; padding: 15px; border: 1px solid #dedede">
            <div class="row-fluid">
                <div class="span4" style="text-align: center">
                    <div style="font-size: 18px; color: #008000;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company_data']->value['success_orders'], ENT_QUOTES, 'ISO-8859-1');?>
</div>
                    <small>Transaksi Berhasil</small>
                </div>
                <div class="span4" style="text-align: center">
                    <div style="font-size: 18px; color: #008000;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company_data']->value['product_sold'], ENT_QUOTES, 'ISO-8859-1');?>
</div>
                    <small>Produk Terjual</small>
                </div>
                <div class="span4" style="text-align: center">
                    <div style="font-size: 18px; color: #008000;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['company_data']->value['total_products'], ENT_QUOTES, 'ISO-8859-1');?>
</div>
                    <small>Total Produk</small>
                </div>
                <div class="span4" style="text-align: center">
                    <div style="font-size: 18px; color: #008000;"><?php echo htmlspecialchars(empty($_smarty_tpl->tpl_vars['company_data']->value['follower']) ? '-' : $_smarty_tpl->tpl_vars['company_data']->value['follower'], ENT_QUOTES, 'ISO-8859-1');?>
</div>
                    <div><i class="fa fa-heart"></i></div>
                </div>
            </div>
        </div>
    </div>
</div><?php }?><?php }} ?>
