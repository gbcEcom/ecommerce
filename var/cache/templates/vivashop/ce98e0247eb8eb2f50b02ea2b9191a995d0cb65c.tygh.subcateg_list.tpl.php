<?php /* Smarty version Smarty-3.1.21, created on 2015-08-10 22:30:45
         compiled from "/home/gbadmin/public_html/production/design/themes/vivashop/templates/common/subcateg_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:103975128055c8c3a5487b46-73591559%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ce98e0247eb8eb2f50b02ea2b9191a995d0cb65c' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/themes/vivashop/templates/common/subcateg_list.tpl',
      1 => 1438219691,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '103975128055c8c3a5487b46-73591559',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'vs_select_menu' => 0,
    'categ_id' => 0,
    'item' => 0,
    'spacer' => 0,
    'category_data' => 0,
    'level' => 0,
    'categ_path' => 0,
    'item_path' => 0,
    'vendor_categs' => 0,
    'current' => 0,
    'limit' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55c8c3a5614f96_12962341',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55c8c3a5614f96_12962341')) {function content_55c8c3a5614f96_12962341($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['vs_select_menu']->value) {?>
    <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = fn_get_categories_tree($_smarty_tpl->tpl_vars['categ_id']->value); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
        <option value="<?php echo htmlspecialchars(fn_url("categories.view?category_id=".((string)$_smarty_tpl->tpl_vars['item']->value['category_id'])), ENT_QUOTES, 'ISO-8859-1');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['category'], ENT_QUOTES, 'ISO-8859-1');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['spacer']->value, ENT_QUOTES, 'ISO-8859-1');
echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['category'], ENT_QUOTES, 'ISO-8859-1');?>

        </option>
        
        <?php if ($_smarty_tpl->tpl_vars['item']->value['subcategories']) {?>
            <?php echo $_smarty_tpl->getSubTemplate ("common/subcateg_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('categ_id'=>$_smarty_tpl->tpl_vars['item']->value['category_id'],'vs_select_menu'=>true,'spacer'=>((string)$_smarty_tpl->tpl_vars['spacer']->value)."¦    "), 0);?>

        <?php }?>
    <?php } ?>
<?php } else { ?>
    <ul>
    <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = fn_get_categories_tree($_smarty_tpl->tpl_vars['categ_id']->value); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
        <?php $_smarty_tpl->tpl_vars["categ_path"] = new Smarty_variable(explode("/",$_smarty_tpl->tpl_vars['category_data']->value['id_path']), null, 0);?>
        <?php $_smarty_tpl->tpl_vars["item_path"] = new Smarty_variable(explode("/",$_smarty_tpl->tpl_vars['item']->value['id_path']), null, 0);?>
        <?php $_smarty_tpl->tpl_vars["level"] = new Smarty_variable($_smarty_tpl->tpl_vars['item']->value['level'], null, 0);?>

        <?php if ($_smarty_tpl->tpl_vars['categ_path']->value[$_smarty_tpl->tpl_vars['level']->value]==$_smarty_tpl->tpl_vars['item_path']->value[$_smarty_tpl->tpl_vars['level']->value]) {?>
            <?php $_smarty_tpl->tpl_vars["current"] = new Smarty_variable(true, null, 0);?>
        <?php } else { ?>
            <?php $_smarty_tpl->tpl_vars["current"] = new Smarty_variable(false, null, 0);?>
        <?php }?>
        <li>

            <a href="<?php if ($_smarty_tpl->tpl_vars['vendor_categs']->value) {
echo htmlspecialchars(fn_url("companies.products?category_id=".((string)$_smarty_tpl->tpl_vars['item']->value['category_id'])."&company_id=".((string)$_smarty_tpl->tpl_vars['company_id']->value)), ENT_QUOTES, 'ISO-8859-1');
} else {
echo htmlspecialchars(fn_url("categories.view?category_id=".((string)$_smarty_tpl->tpl_vars['item']->value['category_id'])), ENT_QUOTES, 'ISO-8859-1');
}?>" class="<?php if ($_smarty_tpl->tpl_vars['current']->value) {?>active<?php }?>">
                <i class="ty-icon-right-dir"></i><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['category'], ENT_QUOTES, 'ISO-8859-1');?>

            </a>
            <?php if ($_smarty_tpl->tpl_vars['item']->value['subcategories']&&$_smarty_tpl->tpl_vars['current']->value&&$_smarty_tpl->tpl_vars['item']->value['level']<=$_smarty_tpl->tpl_vars['limit']->value) {?>
                <?php echo $_smarty_tpl->getSubTemplate ("common/subcateg_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('categ_id'=>$_smarty_tpl->tpl_vars['item']->value['category_id'],'limit'=>$_smarty_tpl->tpl_vars['limit']->value,'vendor_categs'=>$_smarty_tpl->tpl_vars['vendor_categs']->value), 0);?>

            <?php }?>
        </li>
    <?php } ?>
    </ul>
<?php }
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="common/subcateg_list.tpl" id="<?php echo smarty_function_set_id(array('name'=>"common/subcateg_list.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['vs_select_menu']->value) {?>
    <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = fn_get_categories_tree($_smarty_tpl->tpl_vars['categ_id']->value); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
        <option value="<?php echo htmlspecialchars(fn_url("categories.view?category_id=".((string)$_smarty_tpl->tpl_vars['item']->value['category_id'])), ENT_QUOTES, 'ISO-8859-1');?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['category'], ENT_QUOTES, 'ISO-8859-1');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['spacer']->value, ENT_QUOTES, 'ISO-8859-1');
echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['category'], ENT_QUOTES, 'ISO-8859-1');?>

        </option>
        
        <?php if ($_smarty_tpl->tpl_vars['item']->value['subcategories']) {?>
            <?php echo $_smarty_tpl->getSubTemplate ("common/subcateg_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('categ_id'=>$_smarty_tpl->tpl_vars['item']->value['category_id'],'vs_select_menu'=>true,'spacer'=>((string)$_smarty_tpl->tpl_vars['spacer']->value)."¦    "), 0);?>

        <?php }?>
    <?php } ?>
<?php } else { ?>
    <ul>
    <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = fn_get_categories_tree($_smarty_tpl->tpl_vars['categ_id']->value); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value) {
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
        <?php $_smarty_tpl->tpl_vars["categ_path"] = new Smarty_variable(explode("/",$_smarty_tpl->tpl_vars['category_data']->value['id_path']), null, 0);?>
        <?php $_smarty_tpl->tpl_vars["item_path"] = new Smarty_variable(explode("/",$_smarty_tpl->tpl_vars['item']->value['id_path']), null, 0);?>
        <?php $_smarty_tpl->tpl_vars["level"] = new Smarty_variable($_smarty_tpl->tpl_vars['item']->value['level'], null, 0);?>

        <?php if ($_smarty_tpl->tpl_vars['categ_path']->value[$_smarty_tpl->tpl_vars['level']->value]==$_smarty_tpl->tpl_vars['item_path']->value[$_smarty_tpl->tpl_vars['level']->value]) {?>
            <?php $_smarty_tpl->tpl_vars["current"] = new Smarty_variable(true, null, 0);?>
        <?php } else { ?>
            <?php $_smarty_tpl->tpl_vars["current"] = new Smarty_variable(false, null, 0);?>
        <?php }?>
        <li>

            <a href="<?php if ($_smarty_tpl->tpl_vars['vendor_categs']->value) {
echo htmlspecialchars(fn_url("companies.products?category_id=".((string)$_smarty_tpl->tpl_vars['item']->value['category_id'])."&company_id=".((string)$_smarty_tpl->tpl_vars['company_id']->value)), ENT_QUOTES, 'ISO-8859-1');
} else {
echo htmlspecialchars(fn_url("categories.view?category_id=".((string)$_smarty_tpl->tpl_vars['item']->value['category_id'])), ENT_QUOTES, 'ISO-8859-1');
}?>" class="<?php if ($_smarty_tpl->tpl_vars['current']->value) {?>active<?php }?>">
                <i class="ty-icon-right-dir"></i><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item']->value['category'], ENT_QUOTES, 'ISO-8859-1');?>

            </a>
            <?php if ($_smarty_tpl->tpl_vars['item']->value['subcategories']&&$_smarty_tpl->tpl_vars['current']->value&&$_smarty_tpl->tpl_vars['item']->value['level']<=$_smarty_tpl->tpl_vars['limit']->value) {?>
                <?php echo $_smarty_tpl->getSubTemplate ("common/subcateg_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('categ_id'=>$_smarty_tpl->tpl_vars['item']->value['category_id'],'limit'=>$_smarty_tpl->tpl_vars['limit']->value,'vendor_categs'=>$_smarty_tpl->tpl_vars['vendor_categs']->value), 0);?>

            <?php }?>
        </li>
    <?php } ?>
    </ul>
<?php }
}?><?php }} ?>
