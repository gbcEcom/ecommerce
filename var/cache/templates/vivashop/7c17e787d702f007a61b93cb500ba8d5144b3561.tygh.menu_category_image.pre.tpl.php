<?php /* Smarty version Smarty-3.1.21, created on 2015-08-10 22:30:43
         compiled from "/home/gbadmin/public_html/production/design/themes/vivashop/templates/addons/dropdown_image/hooks/blocks/menu_category_image.pre.tpl" */ ?>
<?php /*%%SmartyHeaderCode:176044298355c8c3a350f064-37645075%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7c17e787d702f007a61b93cb500ba8d5144b3561' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/themes/vivashop/templates/addons/dropdown_image/hooks/blocks/menu_category_image.pre.tpl',
      1 => 1438219698,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '176044298355c8c3a350f064-37645075',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'item1' => 0,
    'item2' => 0,
    'category_id' => 0,
    'company_id' => 0,
    'cat_image' => 0,
    'dropdown_image' => 0,
    'offset' => 0,
    'image_width' => 0,
    'image_height' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55c8c3a36e0195_59998901',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55c8c3a36e0195_59998901')) {function content_55c8c3a36e0195_59998901($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_regex_replace')) include '/home/gbadmin/public_html/production/app/lib/vendor/smarty/smarty/libs/plugins/modifier.regex_replace.php';
if (!is_callable('smarty_function_set_id')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['item1']->value['category_id']) {?>
	<?php $_smarty_tpl->tpl_vars["category_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['item1']->value['category_id'], null, 0);?>
<?php } elseif ($_smarty_tpl->tpl_vars['item2']->value['category_id']) {?>
	<?php $_smarty_tpl->tpl_vars["category_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['item2']->value['category_id'], null, 0);?>
<?php } else { ?>
	<?php $_smarty_tpl->tpl_vars["category_id"] = new Smarty_variable(smarty_modifier_regex_replace($_smarty_tpl->tpl_vars['item2']->value['href'],"/(.*)category_id=/",''), null, 0);?>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['item1']->value['company_id']) {?>
	<?php $_smarty_tpl->tpl_vars["company_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['item1']->value['company_id'], null, 0);?>
<?php } elseif ($_smarty_tpl->tpl_vars['item2']->value['company_id']) {?>
	<?php $_smarty_tpl->tpl_vars["company_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['item2']->value['company_id'], null, 0);?>
<?php } else { ?>
	<?php $_smarty_tpl->tpl_vars["company_id"] = new Smarty_variable("1", null, 0);?>
<?php }?>

<?php $_smarty_tpl->tpl_vars["cat_image"] = new Smarty_variable(fn_get_image_pairs($_smarty_tpl->tpl_vars['category_id']->value,'category','M',true,true), null, 0);?>

<?php $_smarty_tpl->tpl_vars["dropdown_image"] = new Smarty_variable(fn_get_dropdown_image_by_categ($_smarty_tpl->tpl_vars['category_id']->value,$_smarty_tpl->tpl_vars['company_id']->value), null, 0);?>

<?php if ($_smarty_tpl->tpl_vars['cat_image']->value['pair_id']) {?>
	<?php if ($_smarty_tpl->tpl_vars['dropdown_image']->value['custom_menu_settings']=="Y") {?>
		<?php $_smarty_tpl->tpl_vars["offset"] = new Smarty_variable("right:".((string)$_smarty_tpl->tpl_vars['dropdown_image']->value['offset_right'])."px; bottom:".((string)$_smarty_tpl->tpl_vars['dropdown_image']->value['offset_bottom'])."px;", null, 0);?>
	<?php } else { ?>
		<?php $_smarty_tpl->tpl_vars["offset"] = new Smarty_variable("right:0px; bottom:0px;", null, 0);?>
	<?php }?>

	<?php if ($_smarty_tpl->tpl_vars['dropdown_image']->value['dd_status']=="Y") {?>
		<div class="dropdown-image">
			<div style="position: absolute;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['offset']->value, ENT_QUOTES, 'ISO-8859-1');?>
">
			<?php if (isset($_smarty_tpl->tpl_vars['dropdown_image']->value['url'])) {?>
				<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dropdown_image']->value['url'], ENT_QUOTES, 'ISO-8859-1');?>
">
			<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['dropdown_image']->value['image_width']==0) {?>
					<?php $_smarty_tpl->tpl_vars["image_width"] = new Smarty_variable('', null, 0);?>
				<?php } else { ?>
					<?php $_smarty_tpl->tpl_vars["image_width"] = new Smarty_variable($_smarty_tpl->tpl_vars['dropdown_image']->value['image_width'], null, 0);?>
				<?php }?>

				<?php if ($_smarty_tpl->tpl_vars['dropdown_image']->value['image_height']==0) {?>
					<?php $_smarty_tpl->tpl_vars["image_height"] = new Smarty_variable('', null, 0);?>
				<?php } else { ?>
					<?php $_smarty_tpl->tpl_vars["image_height"] = new Smarty_variable($_smarty_tpl->tpl_vars['dropdown_image']->value['image_height'], null, 0);?>
				<?php }?>

			    <?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('images'=>$_smarty_tpl->tpl_vars['cat_image']->value['detailed'],'image_width'=>$_smarty_tpl->tpl_vars['image_width']->value,'image_height'=>$_smarty_tpl->tpl_vars['image_height']->value,'vs_lazy_additional'=>true), 0);?>


			<?php if (isset($_smarty_tpl->tpl_vars['dropdown_image']->value['url'])) {?>
		    	</a>
		    <?php }?>
		    </div>
		</div>
	<?php }?>

<?php }
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/dropdown_image/hooks/blocks/menu_category_image.pre.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/dropdown_image/hooks/blocks/menu_category_image.pre.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['item1']->value['category_id']) {?>
	<?php $_smarty_tpl->tpl_vars["category_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['item1']->value['category_id'], null, 0);?>
<?php } elseif ($_smarty_tpl->tpl_vars['item2']->value['category_id']) {?>
	<?php $_smarty_tpl->tpl_vars["category_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['item2']->value['category_id'], null, 0);?>
<?php } else { ?>
	<?php $_smarty_tpl->tpl_vars["category_id"] = new Smarty_variable(smarty_modifier_regex_replace($_smarty_tpl->tpl_vars['item2']->value['href'],"/(.*)category_id=/",''), null, 0);?>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['item1']->value['company_id']) {?>
	<?php $_smarty_tpl->tpl_vars["company_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['item1']->value['company_id'], null, 0);?>
<?php } elseif ($_smarty_tpl->tpl_vars['item2']->value['company_id']) {?>
	<?php $_smarty_tpl->tpl_vars["company_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['item2']->value['company_id'], null, 0);?>
<?php } else { ?>
	<?php $_smarty_tpl->tpl_vars["company_id"] = new Smarty_variable("1", null, 0);?>
<?php }?>

<?php $_smarty_tpl->tpl_vars["cat_image"] = new Smarty_variable(fn_get_image_pairs($_smarty_tpl->tpl_vars['category_id']->value,'category','M',true,true), null, 0);?>

<?php $_smarty_tpl->tpl_vars["dropdown_image"] = new Smarty_variable(fn_get_dropdown_image_by_categ($_smarty_tpl->tpl_vars['category_id']->value,$_smarty_tpl->tpl_vars['company_id']->value), null, 0);?>

<?php if ($_smarty_tpl->tpl_vars['cat_image']->value['pair_id']) {?>
	<?php if ($_smarty_tpl->tpl_vars['dropdown_image']->value['custom_menu_settings']=="Y") {?>
		<?php $_smarty_tpl->tpl_vars["offset"] = new Smarty_variable("right:".((string)$_smarty_tpl->tpl_vars['dropdown_image']->value['offset_right'])."px; bottom:".((string)$_smarty_tpl->tpl_vars['dropdown_image']->value['offset_bottom'])."px;", null, 0);?>
	<?php } else { ?>
		<?php $_smarty_tpl->tpl_vars["offset"] = new Smarty_variable("right:0px; bottom:0px;", null, 0);?>
	<?php }?>

	<?php if ($_smarty_tpl->tpl_vars['dropdown_image']->value['dd_status']=="Y") {?>
		<div class="dropdown-image">
			<div style="position: absolute;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['offset']->value, ENT_QUOTES, 'ISO-8859-1');?>
">
			<?php if (isset($_smarty_tpl->tpl_vars['dropdown_image']->value['url'])) {?>
				<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dropdown_image']->value['url'], ENT_QUOTES, 'ISO-8859-1');?>
">
			<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['dropdown_image']->value['image_width']==0) {?>
					<?php $_smarty_tpl->tpl_vars["image_width"] = new Smarty_variable('', null, 0);?>
				<?php } else { ?>
					<?php $_smarty_tpl->tpl_vars["image_width"] = new Smarty_variable($_smarty_tpl->tpl_vars['dropdown_image']->value['image_width'], null, 0);?>
				<?php }?>

				<?php if ($_smarty_tpl->tpl_vars['dropdown_image']->value['image_height']==0) {?>
					<?php $_smarty_tpl->tpl_vars["image_height"] = new Smarty_variable('', null, 0);?>
				<?php } else { ?>
					<?php $_smarty_tpl->tpl_vars["image_height"] = new Smarty_variable($_smarty_tpl->tpl_vars['dropdown_image']->value['image_height'], null, 0);?>
				<?php }?>

			    <?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('images'=>$_smarty_tpl->tpl_vars['cat_image']->value['detailed'],'image_width'=>$_smarty_tpl->tpl_vars['image_width']->value,'image_height'=>$_smarty_tpl->tpl_vars['image_height']->value,'vs_lazy_additional'=>true), 0);?>


			<?php if (isset($_smarty_tpl->tpl_vars['dropdown_image']->value['url'])) {?>
		    	</a>
		    <?php }?>
		    </div>
		</div>
	<?php }?>

<?php }
}?><?php }} ?>
