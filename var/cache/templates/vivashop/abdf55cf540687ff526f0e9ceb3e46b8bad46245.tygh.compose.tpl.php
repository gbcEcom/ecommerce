<?php /* Smarty version Smarty-3.1.21, created on 2015-10-06 12:23:14
         compiled from "/home/gbadmin/public_html/production/design/themes/vivashop/templates/views/messages/compose.tpl" */ ?>
<?php /*%%SmartyHeaderCode:60979707256135ac2520c66-32451313%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'abdf55cf540687ff526f0e9ceb3e46b8bad46245' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/themes/vivashop/templates/views/messages/compose.tpl',
      1 => 1438219702,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '60979707256135ac2520c66-32451313',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'recipient' => 0,
    '_title' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_56135ac2676a33_68253069',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56135ac2676a33_68253069')) {function content_56135ac2676a33_68253069($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
$_smarty_tpl->tpl_vars["_title"] = new Smarty_variable("Inbox", null, 0);?>
<div class="box" style="margin-bottom: 15px">
    <div class="box-header">
        <h4>Percakapan dengan <span style="color: #0092ef"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['recipient']->value['company'], ENT_QUOTES, 'ISO-8859-1');?>
</span></h4>
    </div>
    <div class="box-body">
        <form method="post" action="<?php echo htmlspecialchars(fn_url("messages.send"), ENT_QUOTES, 'ISO-8859-1');?>
" class="form">
            <input type="hidden" name="recipient" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['recipient']->value['company_id'], ENT_QUOTES, 'ISO-8859-1');?>
" />
            <input type="hidden" name="recipient_type" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['recipient']->value['type'], ENT_QUOTES, 'ISO-8859-1');?>
" />
            <div class="ty-control-group">
                <textarea class="ty-input-text-full ty-input-textarea" name="message" rows="5" placeholder="Ketik pesan..." required="required" autofocus="autofocus"></textarea>
            </div>
            <div class="controls">
                <button class="ty-btn ty-btn__secondary" type="button">Kembali</button>
                <button class="ty-btn ty-btn__primary ty-float-right" type="submit">Kirim</button>
            </div>
        </form>
    </div>
</div>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo htmlspecialchars($_smarty_tpl->tpl_vars['_title']->value, ENT_QUOTES, 'ISO-8859-1');
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="views/messages/compose.tpl" id="<?php echo smarty_function_set_id(array('name'=>"views/messages/compose.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
$_smarty_tpl->tpl_vars["_title"] = new Smarty_variable("Inbox", null, 0);?>
<div class="box" style="margin-bottom: 15px">
    <div class="box-header">
        <h4>Percakapan dengan <span style="color: #0092ef"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['recipient']->value['company'], ENT_QUOTES, 'ISO-8859-1');?>
</span></h4>
    </div>
    <div class="box-body">
        <form method="post" action="<?php echo htmlspecialchars(fn_url("messages.send"), ENT_QUOTES, 'ISO-8859-1');?>
" class="form">
            <input type="hidden" name="recipient" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['recipient']->value['company_id'], ENT_QUOTES, 'ISO-8859-1');?>
" />
            <input type="hidden" name="recipient_type" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['recipient']->value['type'], ENT_QUOTES, 'ISO-8859-1');?>
" />
            <div class="ty-control-group">
                <textarea class="ty-input-text-full ty-input-textarea" name="message" rows="5" placeholder="Ketik pesan..." required="required" autofocus="autofocus"></textarea>
            </div>
            <div class="controls">
                <button class="ty-btn ty-btn__secondary" type="button">Kembali</button>
                <button class="ty-btn ty-btn__primary ty-float-right" type="submit">Kirim</button>
            </div>
        </form>
    </div>
</div>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo htmlspecialchars($_smarty_tpl->tpl_vars['_title']->value, ENT_QUOTES, 'ISO-8859-1');
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
}?><?php }} ?>
