<?php /* Smarty version Smarty-3.1.21, created on 2015-08-10 22:30:45
         compiled from "/home/gbadmin/public_html/production/design/themes/vivashop/templates/blocks/static_templates/subcategories.tpl" */ ?>
<?php /*%%SmartyHeaderCode:37728602555c8c3a531cfd6-38714750%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f9d27c712a3ed7223090a100658646e78df17286' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/themes/vivashop/templates/blocks/static_templates/subcategories.tpl',
      1 => 1438219692,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '37728602555c8c3a531cfd6-38714750',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'subcategories' => 0,
    'columns' => 0,
    'rows' => 0,
    'category_data' => 0,
    'block' => 0,
    'item1' => 0,
    'level' => 0,
    'categ_path' => 0,
    'item_path' => 0,
    'current' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55c8c3a54725d2_83965769',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55c8c3a54725d2_83965769')) {function content_55c8c3a54725d2_83965769($_smarty_tpl) {?><?php if (!is_callable('smarty_function_math')) include '/home/gbadmin/public_html/production/app/lib/vendor/smarty/smarty/libs/plugins/function.math.php';
if (!is_callable('smarty_function_split')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/function.split.php';
if (!is_callable('smarty_modifier_regex_replace')) include '/home/gbadmin/public_html/production/app/lib/vendor/smarty/smarty/libs/plugins/modifier.regex_replace.php';
if (!is_callable('smarty_function_set_id')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/function.set_id.php';
?><?php
fn_preload_lang_vars(array('browse_by_category','browse_by_category'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
echo smarty_function_math(array('equation'=>"ceil(n/c)",'assign'=>"rows",'n'=>count($_smarty_tpl->tpl_vars['subcategories']->value),'c'=>(($tmp = @$_smarty_tpl->tpl_vars['columns']->value)===null||$tmp==='' ? "2" : $tmp)),$_smarty_tpl);?>

<?php echo smarty_function_split(array('data'=>$_smarty_tpl->tpl_vars['subcategories']->value,'size'=>$_smarty_tpl->tpl_vars['rows']->value,'assign'=>"splitted_subcategories"),$_smarty_tpl);?>


<?php $_smarty_tpl->tpl_vars["root_categ"] = new Smarty_variable(smarty_modifier_regex_replace($_smarty_tpl->tpl_vars['category_data']->value['id_path'],"/\/(.*)/",''), null, 0);?>

<div class="vs-sidebox clearfix">
    <h2 class="vs-sidebox-title">
            <span><?php echo $_smarty_tpl->__("browse_by_category");?>
</span>
            <span class="vs-title-toggle cm-combination visible-phone visible-tablet" id="sw_sidebox_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['block_id'], ENT_QUOTES, 'ISO-8859-1');?>
">
                <i class="ty-sidebox__icon-open ty-icon-down-open"></i>
                <i class="ty-sidebox__icon-hide ty-icon-up-open"></i>
            </span>
    </h2>
    <div class="vs-toggle-body vs-sidebox-body vs-side-subcateg clearfix" id="sidebox_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['block_id'], ENT_QUOTES, 'ISO-8859-1');?>
">

        <?php  $_smarty_tpl->tpl_vars['item1'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item1']->_loop = false;
 $_from = fn_get_categories_tree(0); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item1']->key => $_smarty_tpl->tpl_vars['item1']->value) {
$_smarty_tpl->tpl_vars['item1']->_loop = true;
?>
            <?php $_smarty_tpl->tpl_vars["categ_path"] = new Smarty_variable(explode("/",$_smarty_tpl->tpl_vars['category_data']->value['id_path']), null, 0);?>
            <?php $_smarty_tpl->tpl_vars["item_path"] = new Smarty_variable(explode("/",$_smarty_tpl->tpl_vars['item1']->value['id_path']), null, 0);?>
            <?php $_smarty_tpl->tpl_vars["level"] = new Smarty_variable($_smarty_tpl->tpl_vars['item1']->value['level'], null, 0);?>

            <?php if ($_smarty_tpl->tpl_vars['categ_path']->value[$_smarty_tpl->tpl_vars['level']->value]==$_smarty_tpl->tpl_vars['item_path']->value[$_smarty_tpl->tpl_vars['level']->value]) {?>
                <?php $_smarty_tpl->tpl_vars["current"] = new Smarty_variable(true, null, 0);?>
            <?php } else { ?>
                <?php $_smarty_tpl->tpl_vars["current"] = new Smarty_variable(false, null, 0);?>
            <?php }?>

            <div class="item1-wrapper">
                <a href="<?php echo htmlspecialchars(fn_url("categories.view?category_id=".((string)$_smarty_tpl->tpl_vars['item1']->value['category_id'])), ENT_QUOTES, 'ISO-8859-1');?>
" class="item1 <?php if ($_smarty_tpl->tpl_vars['current']->value) {?>active<?php }?>">
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1']->value['category'], ENT_QUOTES, 'ISO-8859-1');?>

                </a>
                <?php if ($_smarty_tpl->tpl_vars['item1']->value['subcategories']&&$_smarty_tpl->tpl_vars['current']->value) {?>
                    <?php echo $_smarty_tpl->getSubTemplate ("common/subcateg_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('categ_id'=>$_smarty_tpl->tpl_vars['item1']->value['category_id'],'limit'=>1), 0);?>

                <?php }?>
            </div>
        <?php } ?>

    </div>
</div>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="blocks/static_templates/subcategories.tpl" id="<?php echo smarty_function_set_id(array('name'=>"blocks/static_templates/subcategories.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
echo smarty_function_math(array('equation'=>"ceil(n/c)",'assign'=>"rows",'n'=>count($_smarty_tpl->tpl_vars['subcategories']->value),'c'=>(($tmp = @$_smarty_tpl->tpl_vars['columns']->value)===null||$tmp==='' ? "2" : $tmp)),$_smarty_tpl);?>

<?php echo smarty_function_split(array('data'=>$_smarty_tpl->tpl_vars['subcategories']->value,'size'=>$_smarty_tpl->tpl_vars['rows']->value,'assign'=>"splitted_subcategories"),$_smarty_tpl);?>


<?php $_smarty_tpl->tpl_vars["root_categ"] = new Smarty_variable(smarty_modifier_regex_replace($_smarty_tpl->tpl_vars['category_data']->value['id_path'],"/\/(.*)/",''), null, 0);?>

<div class="vs-sidebox clearfix">
    <h2 class="vs-sidebox-title">
            <span><?php echo $_smarty_tpl->__("browse_by_category");?>
</span>
            <span class="vs-title-toggle cm-combination visible-phone visible-tablet" id="sw_sidebox_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['block_id'], ENT_QUOTES, 'ISO-8859-1');?>
">
                <i class="ty-sidebox__icon-open ty-icon-down-open"></i>
                <i class="ty-sidebox__icon-hide ty-icon-up-open"></i>
            </span>
    </h2>
    <div class="vs-toggle-body vs-sidebox-body vs-side-subcateg clearfix" id="sidebox_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['block_id'], ENT_QUOTES, 'ISO-8859-1');?>
">

        <?php  $_smarty_tpl->tpl_vars['item1'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item1']->_loop = false;
 $_from = fn_get_categories_tree(0); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item1']->key => $_smarty_tpl->tpl_vars['item1']->value) {
$_smarty_tpl->tpl_vars['item1']->_loop = true;
?>
            <?php $_smarty_tpl->tpl_vars["categ_path"] = new Smarty_variable(explode("/",$_smarty_tpl->tpl_vars['category_data']->value['id_path']), null, 0);?>
            <?php $_smarty_tpl->tpl_vars["item_path"] = new Smarty_variable(explode("/",$_smarty_tpl->tpl_vars['item1']->value['id_path']), null, 0);?>
            <?php $_smarty_tpl->tpl_vars["level"] = new Smarty_variable($_smarty_tpl->tpl_vars['item1']->value['level'], null, 0);?>

            <?php if ($_smarty_tpl->tpl_vars['categ_path']->value[$_smarty_tpl->tpl_vars['level']->value]==$_smarty_tpl->tpl_vars['item_path']->value[$_smarty_tpl->tpl_vars['level']->value]) {?>
                <?php $_smarty_tpl->tpl_vars["current"] = new Smarty_variable(true, null, 0);?>
            <?php } else { ?>
                <?php $_smarty_tpl->tpl_vars["current"] = new Smarty_variable(false, null, 0);?>
            <?php }?>

            <div class="item1-wrapper">
                <a href="<?php echo htmlspecialchars(fn_url("categories.view?category_id=".((string)$_smarty_tpl->tpl_vars['item1']->value['category_id'])), ENT_QUOTES, 'ISO-8859-1');?>
" class="item1 <?php if ($_smarty_tpl->tpl_vars['current']->value) {?>active<?php }?>">
                    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1']->value['category'], ENT_QUOTES, 'ISO-8859-1');?>

                </a>
                <?php if ($_smarty_tpl->tpl_vars['item1']->value['subcategories']&&$_smarty_tpl->tpl_vars['current']->value) {?>
                    <?php echo $_smarty_tpl->getSubTemplate ("common/subcateg_list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('categ_id'=>$_smarty_tpl->tpl_vars['item1']->value['category_id'],'limit'=>1), 0);?>

                <?php }?>
            </div>
        <?php } ?>

    </div>
</div>
<?php }?><?php }} ?>
