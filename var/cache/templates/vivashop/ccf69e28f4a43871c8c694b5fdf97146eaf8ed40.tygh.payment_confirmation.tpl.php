<?php /* Smarty version Smarty-3.1.21, created on 2015-08-10 23:57:03
         compiled from "/home/gbadmin/public_html/production/design/themes/vivashop/templates/views/orders/payment_confirmation.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1321497555c8d7df8f6422-06544870%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ccf69e28f4a43871c8c694b5fdf97146eaf8ed40' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/themes/vivashop/templates/views/orders/payment_confirmation.tpl',
      1 => 1438219700,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '1321497555c8d7df8f6422-06544870',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'tgl' => 0,
    'arrBln' => 0,
    'x' => 0,
    'bln' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55c8d7df9baf05_59338745',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55c8d7df9baf05_59338745')) {function content_55c8d7df9baf05_59338745($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="vs-mainbox-general clearfix" style="background-color: #e5e5e5; margin-bottom: 15px;">
    <h1 class="vs-mainbox-title" style="padding-left: 15px !important;">Payment Confirmation</h1>
    <div class="vs-mainbox-body" style="padding: 0px 15px 15px 15px">
        <div class="account">
            <form id="PaymentConfirmation2" method="post" action="<?php echo htmlspecialchars(fn_url("orders.do_confirmation"), ENT_QUOTES, 'ISO-8859-1');?>
" enctype="multipart/form-data">
                <div class="ty-control-group">
                    <label class="ty-control-group__label cm-required">No Order</label>
                    <input id="OrderId" class="ty-input-text-full" type="text" name="order_id" placeholder="Tulis no order transaksi/faktur belanja anda" required="required" autofocus="autofocus" />
                </div>
                <div class="control-group">
                    <label class="control-label cm-required">Nama Pemilik Rekening</label>
                    <input class="ty-input-text-full" type="text" name="account_name" placeholder="Tulis nama pemilik rekening pembayaran" required="required" />
                </div>
                <div class="row-fluid control-group">
                    <div class="span8">
                        <label class="control-label cm-required">Pembayaran dari bank</label>
                        <select name="from_bank" class="ty-input-text-full">
                            <option value="bca">Bank BCA</option>
                            <option value="mandiri">Bank Mandiri</option>
                        </select>
                    </div>
                    <div class="span8">
                        <label class="control-label cm-required">Ke Bank</label>
                        <select name="to_bank" class="ty-input-text-full">
                            <option value="bca">Bank BCA</option>
                            <option value="mandiri">Bank Mandiri</option>
                        </select>
                    </div>
                </div>
                <div class="row-fluid control-group">
                    <div class="span8">
                        <label class="control-label cm-required">Tanggal Transfer</label>
                        <div class="controls">
                            <select name="tgl" class="input-text-short">
                                <?php $_smarty_tpl->tpl_vars['tgl'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['tgl']->step = 1;$_smarty_tpl->tpl_vars['tgl']->total = (int) ceil(($_smarty_tpl->tpl_vars['tgl']->step > 0 ? 31+1 - (1) : 1-(31)+1)/abs($_smarty_tpl->tpl_vars['tgl']->step));
if ($_smarty_tpl->tpl_vars['tgl']->total > 0) {
for ($_smarty_tpl->tpl_vars['tgl']->value = 1, $_smarty_tpl->tpl_vars['tgl']->iteration = 1;$_smarty_tpl->tpl_vars['tgl']->iteration <= $_smarty_tpl->tpl_vars['tgl']->total;$_smarty_tpl->tpl_vars['tgl']->value += $_smarty_tpl->tpl_vars['tgl']->step, $_smarty_tpl->tpl_vars['tgl']->iteration++) {
$_smarty_tpl->tpl_vars['tgl']->first = $_smarty_tpl->tpl_vars['tgl']->iteration == 1;$_smarty_tpl->tpl_vars['tgl']->last = $_smarty_tpl->tpl_vars['tgl']->iteration == $_smarty_tpl->tpl_vars['tgl']->total;?>
                                    <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tgl']->value, ENT_QUOTES, 'ISO-8859-1');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tgl']->value, ENT_QUOTES, 'ISO-8859-1');?>
</option>
                                <?php }} ?>
                            </select>
                            <select name="bln" class="input-text-medium">
                                <?php  $_smarty_tpl->tpl_vars['bln'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['bln']->_loop = false;
 $_smarty_tpl->tpl_vars['x'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['arrBln']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['bln']->key => $_smarty_tpl->tpl_vars['bln']->value) {
$_smarty_tpl->tpl_vars['bln']->_loop = true;
 $_smarty_tpl->tpl_vars['x']->value = $_smarty_tpl->tpl_vars['bln']->key;
?>
                                    <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['x']->value, ENT_QUOTES, 'ISO-8859-1');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['bln']->value, ENT_QUOTES, 'ISO-8859-1');?>
</option>
                                <?php } ?>
                            </select>
                            <select name="thn" class="input-text-medium">
                                <option value="<?php echo htmlspecialchars(date('Y'), ENT_QUOTES, 'ISO-8859-1');?>
"><?php echo htmlspecialchars(date('Y'), ENT_QUOTES, 'ISO-8859-1');?>
</option>
                            </select>
                        </div>
                    </div>
                    <div class="span8">
                        <label class="control-label cm-required">Jumlah Transfer</label>
                        <input class="ty-input-text-full" type="text" name="amount" placeholder="misal: 500000" required="required" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label cm-required">Bukti Transfer</label>
                    <input class="input-text" type="file" name="payment_receipt" />
                </div>
                <div class="buttons-container left">
                    <span class="button-submit button-wrap-left"><span class="button-submit button-wrap-right"><input style="padding: 3px 10px; background-color: #0098d1; border: 1px solid #006699; font-size: 12px; color: #ffffff" type="submit" value="Konfirmasi"></span></span>
                </div>
            </form>
        </div>
    </div>
</div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="views/orders/payment_confirmation.tpl" id="<?php echo smarty_function_set_id(array('name'=>"views/orders/payment_confirmation.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="vs-mainbox-general clearfix" style="background-color: #e5e5e5; margin-bottom: 15px;">
    <h1 class="vs-mainbox-title" style="padding-left: 15px !important;">Payment Confirmation</h1>
    <div class="vs-mainbox-body" style="padding: 0px 15px 15px 15px">
        <div class="account">
            <form id="PaymentConfirmation2" method="post" action="<?php echo htmlspecialchars(fn_url("orders.do_confirmation"), ENT_QUOTES, 'ISO-8859-1');?>
" enctype="multipart/form-data">
                <div class="ty-control-group">
                    <label class="ty-control-group__label cm-required">No Order</label>
                    <input id="OrderId" class="ty-input-text-full" type="text" name="order_id" placeholder="Tulis no order transaksi/faktur belanja anda" required="required" autofocus="autofocus" />
                </div>
                <div class="control-group">
                    <label class="control-label cm-required">Nama Pemilik Rekening</label>
                    <input class="ty-input-text-full" type="text" name="account_name" placeholder="Tulis nama pemilik rekening pembayaran" required="required" />
                </div>
                <div class="row-fluid control-group">
                    <div class="span8">
                        <label class="control-label cm-required">Pembayaran dari bank</label>
                        <select name="from_bank" class="ty-input-text-full">
                            <option value="bca">Bank BCA</option>
                            <option value="mandiri">Bank Mandiri</option>
                        </select>
                    </div>
                    <div class="span8">
                        <label class="control-label cm-required">Ke Bank</label>
                        <select name="to_bank" class="ty-input-text-full">
                            <option value="bca">Bank BCA</option>
                            <option value="mandiri">Bank Mandiri</option>
                        </select>
                    </div>
                </div>
                <div class="row-fluid control-group">
                    <div class="span8">
                        <label class="control-label cm-required">Tanggal Transfer</label>
                        <div class="controls">
                            <select name="tgl" class="input-text-short">
                                <?php $_smarty_tpl->tpl_vars['tgl'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['tgl']->step = 1;$_smarty_tpl->tpl_vars['tgl']->total = (int) ceil(($_smarty_tpl->tpl_vars['tgl']->step > 0 ? 31+1 - (1) : 1-(31)+1)/abs($_smarty_tpl->tpl_vars['tgl']->step));
if ($_smarty_tpl->tpl_vars['tgl']->total > 0) {
for ($_smarty_tpl->tpl_vars['tgl']->value = 1, $_smarty_tpl->tpl_vars['tgl']->iteration = 1;$_smarty_tpl->tpl_vars['tgl']->iteration <= $_smarty_tpl->tpl_vars['tgl']->total;$_smarty_tpl->tpl_vars['tgl']->value += $_smarty_tpl->tpl_vars['tgl']->step, $_smarty_tpl->tpl_vars['tgl']->iteration++) {
$_smarty_tpl->tpl_vars['tgl']->first = $_smarty_tpl->tpl_vars['tgl']->iteration == 1;$_smarty_tpl->tpl_vars['tgl']->last = $_smarty_tpl->tpl_vars['tgl']->iteration == $_smarty_tpl->tpl_vars['tgl']->total;?>
                                    <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tgl']->value, ENT_QUOTES, 'ISO-8859-1');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['tgl']->value, ENT_QUOTES, 'ISO-8859-1');?>
</option>
                                <?php }} ?>
                            </select>
                            <select name="bln" class="input-text-medium">
                                <?php  $_smarty_tpl->tpl_vars['bln'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['bln']->_loop = false;
 $_smarty_tpl->tpl_vars['x'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['arrBln']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['bln']->key => $_smarty_tpl->tpl_vars['bln']->value) {
$_smarty_tpl->tpl_vars['bln']->_loop = true;
 $_smarty_tpl->tpl_vars['x']->value = $_smarty_tpl->tpl_vars['bln']->key;
?>
                                    <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['x']->value, ENT_QUOTES, 'ISO-8859-1');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['bln']->value, ENT_QUOTES, 'ISO-8859-1');?>
</option>
                                <?php } ?>
                            </select>
                            <select name="thn" class="input-text-medium">
                                <option value="<?php echo htmlspecialchars(date('Y'), ENT_QUOTES, 'ISO-8859-1');?>
"><?php echo htmlspecialchars(date('Y'), ENT_QUOTES, 'ISO-8859-1');?>
</option>
                            </select>
                        </div>
                    </div>
                    <div class="span8">
                        <label class="control-label cm-required">Jumlah Transfer</label>
                        <input class="ty-input-text-full" type="text" name="amount" placeholder="misal: 500000" required="required" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label cm-required">Bukti Transfer</label>
                    <input class="input-text" type="file" name="payment_receipt" />
                </div>
                <div class="buttons-container left">
                    <span class="button-submit button-wrap-left"><span class="button-submit button-wrap-right"><input style="padding: 3px 10px; background-color: #0098d1; border: 1px solid #006699; font-size: 12px; color: #ffffff" type="submit" value="Konfirmasi"></span></span>
                </div>
            </form>
        </div>
    </div>
</div><?php }?><?php }} ?>
