<?php /* Smarty version Smarty-3.1.21, created on 2015-08-10 22:30:43
         compiled from "/home/gbadmin/public_html/production/design/themes/vivashop/templates/blocks/topmenu_dropdown.tpl" */ ?>
<?php /*%%SmartyHeaderCode:81226705555c8c3a3228f62-58364427%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a5d59209c2630817941bf0d72a38f66199bd81cb' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/themes/vivashop/templates/blocks/topmenu_dropdown.tpl',
      1 => 1438219693,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '81226705555c8c3a3228f62-58364427',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'items' => 0,
    'block' => 0,
    'item1' => 0,
    'childs' => 0,
    'item_1_class' => 0,
    'item1_url' => 0,
    'name' => 0,
    'dd_img_class' => 0,
    'dd_menu' => 0,
    'cols' => 0,
    'item_2_class' => 0,
    'item2' => 0,
    'item2_url' => 0,
    'cat_image' => 0,
    'limit' => 0,
    'item3' => 0,
    'item3_url' => 0,
    'count_child' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55c8c3a3731ea9_20102257',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55c8c3a3731ea9_20102257')) {function content_55c8c3a3731ea9_20102257($_smarty_tpl) {?><?php if (!is_callable('smarty_block_hook')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/block.hook.php';
if (!is_callable('smarty_function_set_id')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/function.set_id.php';
?><?php
fn_preload_lang_vars(array('all_categories','view_more','all_categories','view_more'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
$_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"blocks:topmenu_dropdown")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"blocks:topmenu_dropdown"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>

<?php if ($_smarty_tpl->tpl_vars['items']->value) {?>

    <div class="wrap-dropdown-hybrid">
        <!-- level 1 wrapper - horizontal -->
        
        <ul class="dropdown-hybrid clearfix">
            <!-- level 1 items - horizontal -->
            <?php $_smarty_tpl->tpl_vars["page_name"] = new Smarty_variable($_REQUEST['dispatch'], null, 0);?>


            <?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"blocks:topmenu_dropdown_top_menu")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"blocks:topmenu_dropdown_top_menu"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>


            <li class="">
                <a class="drop item-1" >
                    <i class="vs-icon-menu"></i>
                    <span><?php echo $_smarty_tpl->__("all_categories");?>
</span>
                </a>
                <div class="dropdown-hybrid-column">
                    <!-- level 2 wrapper - vertical -->
                    <ul id="vmenu_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['block_id'], ENT_QUOTES, 'ISO-8859-1');?>
" class="adropdown adropdown-vertical<?php if ($_smarty_tpl->tpl_vars['block']->value['properties']['right_to_left_orientation']=="Y") {?> rtl<?php }?>">
                            <?php  $_smarty_tpl->tpl_vars["item1"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["item1"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars["item1"]->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars["item1"]->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars["item1"]->key => $_smarty_tpl->tpl_vars["item1"]->value) {
$_smarty_tpl->tpl_vars["item1"]->_loop = true;
 $_smarty_tpl->tpl_vars["item1"]->iteration++;
 $_smarty_tpl->tpl_vars["item1"]->last = $_smarty_tpl->tpl_vars["item1"]->iteration === $_smarty_tpl->tpl_vars["item1"]->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["item1"]['last'] = $_smarty_tpl->tpl_vars["item1"]->last;
$_smarty_tpl->tpl_vars["cols"] = new Smarty_variable(0, null, 0);
if ($_smarty_tpl->tpl_vars['item1']->value['active']||fn_check_is_active_menu_item($_smarty_tpl->tpl_vars['item1']->value,$_smarty_tpl->tpl_vars['block']->value['type'])) {
$_smarty_tpl->tpl_vars["item_1_class"] = new Smarty_variable("active", null, 0);
} else {
$_smarty_tpl->tpl_vars["item_1_class"] = new Smarty_variable('', null, 0);
}
if (!$_smarty_tpl->getVariable('smarty')->value['foreach']['item1']['last']) {
$_smarty_tpl->tpl_vars["item_1_class"] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['item_1_class']->value)." b-border ", null, 0);
}
if ($_smarty_tpl->tpl_vars['item1']->value[$_smarty_tpl->tpl_vars['childs']->value]) {
$_smarty_tpl->tpl_vars["item_1_class"] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['item_1_class']->value)." dir", null, 0);
}?><!-- level 2 items - vertical --><li class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item_1_class']->value, ENT_QUOTES, 'ISO-8859-1');?>
 adropdown-vertical_li"><?php $_smarty_tpl->tpl_vars["item1_url"] = new Smarty_variable(fn_form_dropdown_object_link($_smarty_tpl->tpl_vars['item1']->value,$_smarty_tpl->tpl_vars['block']->value['type']), null, 0);
if ($_smarty_tpl->tpl_vars['item1']->value[$_smarty_tpl->tpl_vars['childs']->value]) {?><a<?php if ($_smarty_tpl->tpl_vars['item1_url']->value) {?> href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1_url']->value, ENT_QUOTES, 'ISO-8859-1');?>
"<?php }?> <?php if ($_smarty_tpl->tpl_vars['item1']->value['new_window']) {?>target="_blank"<?php }?> class="item-1"><span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1']->value[$_smarty_tpl->tpl_vars['name']->value], ENT_QUOTES, 'ISO-8859-1');?>
</span><i class="vs-icon-arrow-right"></i></a><div class="vs-title-toggle cm-combination visible-phone visible-tablet" id="sw_vs_box_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1']->value['category_id'], ENT_QUOTES, 'ISO-8859-1');?>
000"><i class="ty-sidebox__icon-open ty-icon-down-open"></i><i class="ty-sidebox__icon-hide ty-icon-up-open"></i></div><?php $_smarty_tpl->_capture_stack[0][] = array("dropdown_image", null, null); ob_start();
$_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"blocks:menu_category_image")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"blocks:menu_category_image"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();
$_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"blocks:menu_category_image"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
$_smarty_tpl->_capture_stack[0][] = array("dropdown_image_custom_menu", null, null); ob_start();
$_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"blocks:menu_category_custom_menu")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"blocks:menu_category_custom_menu"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();
$_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"blocks:menu_category_custom_menu"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
$_smarty_tpl->tpl_vars['et_dd_cols_viva'] = new Smarty_variable(true, null, 0);
$_smarty_tpl->_capture_stack[0][] = array("dropdown_image_custom_menu_cols", null, null); ob_start();
$_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"blocks:menu_category_custom_menu_cols")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"blocks:menu_category_custom_menu_cols"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();
$_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"blocks:menu_category_custom_menu_cols"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['dropdown_image_custom_menu'])) {
$_smarty_tpl->tpl_vars["dd_menu"] = new Smarty_variable(trim(Smarty::$_smarty_vars['capture']['dropdown_image_custom_menu']), null, 0);
} else {
$_smarty_tpl->tpl_vars["dd_menu"] = new Smarty_variable('', null, 0);
}
if (trim(Smarty::$_smarty_vars['capture']['dropdown_image'])) {
$_smarty_tpl->tpl_vars['cols'] = new Smarty_variable(5, null, 0);
if (trim(Smarty::$_smarty_vars['capture']['dropdown_image_custom_menu_cols'])) {
$_smarty_tpl->tpl_vars['cols'] = new Smarty_variable(trim(Smarty::$_smarty_vars['capture']['dropdown_image_custom_menu_cols']), null, 0);
}
$_smarty_tpl->tpl_vars["dd_img_class"] = new Smarty_variable("dd-img", null, 0);
} else {
$_smarty_tpl->tpl_vars['cols'] = new Smarty_variable(5, null, 0);
$_smarty_tpl->tpl_vars["dd_img_class"] = new Smarty_variable("no-dd-img", null, 0);
}?><!-- level 3 wrapper - horizontal --><div class="adropdown-fullwidth <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dd_img_class']->value, ENT_QUOTES, 'ISO-8859-1');?>
" style="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dd_menu']->value, ENT_QUOTES, 'ISO-8859-1');?>
" id="vs_box_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1']->value['category_id'], ENT_QUOTES, 'ISO-8859-1');?>
000"><div class="main-categ-title"><a<?php if ($_smarty_tpl->tpl_vars['item1_url']->value) {?> href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1_url']->value, ENT_QUOTES, 'ISO-8859-1');?>
"<?php }?> <?php if ($_smarty_tpl->tpl_vars['item1']->value['new_window']) {?>target="_blank"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1']->value[$_smarty_tpl->tpl_vars['name']->value], ENT_QUOTES, 'ISO-8859-1');?>
</a></div><?php if (trim(Smarty::$_smarty_vars['capture']['dropdown_image'])) {
echo Smarty::$_smarty_vars['capture']['dropdown_image'];
}
$_smarty_tpl->tpl_vars["item2"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["item2"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['item1']->value[$_smarty_tpl->tpl_vars['childs']->value]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars["item2"]->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars["item2"]->iteration=0;
 $_smarty_tpl->tpl_vars["item2"]->index=-1;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["item2"]['index']=-1;
foreach ($_from as $_smarty_tpl->tpl_vars["item2"]->key => $_smarty_tpl->tpl_vars["item2"]->value) {
$_smarty_tpl->tpl_vars["item2"]->_loop = true;
 $_smarty_tpl->tpl_vars["item2"]->iteration++;
 $_smarty_tpl->tpl_vars["item2"]->index++;
 $_smarty_tpl->tpl_vars["item2"]->first = $_smarty_tpl->tpl_vars["item2"]->index === 0;
 $_smarty_tpl->tpl_vars["item2"]->last = $_smarty_tpl->tpl_vars["item2"]->iteration === $_smarty_tpl->tpl_vars["item2"]->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["item2"]['first'] = $_smarty_tpl->tpl_vars["item2"]->first;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["item2"]['index']++;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["item2"]['last'] = $_smarty_tpl->tpl_vars["item2"]->last;
if ($_smarty_tpl->getVariable('smarty')->value['foreach']['item2']['index']%$_smarty_tpl->tpl_vars['cols']->value==0||$_smarty_tpl->getVariable('smarty')->value['foreach']['item2']['first']) {
$_smarty_tpl->tpl_vars["item_2_class"] = new Smarty_variable("firstcolumn", null, 0);
} elseif ($_smarty_tpl->getVariable('smarty')->value['foreach']['item2']['index']%$_smarty_tpl->tpl_vars['cols']->value==($_smarty_tpl->tpl_vars['cols']->value-1)||$_smarty_tpl->getVariable('smarty')->value['foreach']['item2']['last']) {
$_smarty_tpl->tpl_vars["item_2_class"] = new Smarty_variable("lastcolumn", null, 0);
} else {
$_smarty_tpl->tpl_vars["item_2_class"] = new Smarty_variable('', null, 0);
}?><!-- level 2 item - horizontal --><div class="col-1 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item_2_class']->value, ENT_QUOTES, 'ISO-8859-1');?>
"><?php $_smarty_tpl->tpl_vars["item2_url"] = new Smarty_variable(fn_form_dropdown_object_link($_smarty_tpl->tpl_vars['item2']->value,$_smarty_tpl->tpl_vars['block']->value['type']), null, 0);?><h3<?php if ($_smarty_tpl->tpl_vars['item2']->value['active']||fn_check_is_active_menu_item($_smarty_tpl->tpl_vars['item1']->value,$_smarty_tpl->tpl_vars['block']->value['type'])) {?> class="active"<?php }?>><a<?php if ($_smarty_tpl->tpl_vars['item2_url']->value) {?> href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item2_url']->value, ENT_QUOTES, 'ISO-8859-1');?>
"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item2']->value[$_smarty_tpl->tpl_vars['name']->value], ENT_QUOTES, 'ISO-8859-1');?>
<span class="vs-title-toggle cm-combination visible-phone visible-tablet" id="sw_vs_box_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item2']->value['category_id'], ENT_QUOTES, 'ISO-8859-1');?>
000"><i class="ty-sidebox__icon-open ty-icon-down-open"></i><i class="ty-sidebox__icon-hide ty-icon-up-open"></i></span></a></h3><?php $_smarty_tpl->tpl_vars["cat_image"] = new Smarty_variable(fn_get_image_pairs($_smarty_tpl->tpl_vars['item2']->value['category_id'],'category','M',true,true), null, 0);?><div style="" class="vs-sub-categ-img"><a<?php if ($_smarty_tpl->tpl_vars['item2_url']->value) {?> href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item2_url']->value, ENT_QUOTES, 'ISO-8859-1');?>
"<?php }?> class="vs-sub-categ-img-link"><?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('images'=>$_smarty_tpl->tpl_vars['cat_image']->value['detailed'],'image_width'=>139,'image_height'=>139,'vs_lazy_additional'=>true), 0);?>
</a></div><?php if ($_smarty_tpl->tpl_vars['item2']->value[$_smarty_tpl->tpl_vars['childs']->value]) {?><!-- level 3 wrapper - vertical --><ul id="vs_box_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item2']->value['category_id'], ENT_QUOTES, 'ISO-8859-1');?>
000"><?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"blocks:topmenu_dropdown_2levels_col_elements")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"blocks:topmenu_dropdown_2levels_col_elements"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();
$_smarty_tpl->tpl_vars['limit'] = new Smarty_variable($_smarty_tpl->tpl_vars['block']->value['properties']['dropdown_third_level_elements'], null, 0);
$_smarty_tpl->tpl_vars['count_child'] = new Smarty_variable(count(fn_get_categories_tree($_smarty_tpl->tpl_vars['item2']->value['category_id'])), null, 0);
$_smarty_tpl->tpl_vars["item3"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["item3"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['item2']->value[$_smarty_tpl->tpl_vars['childs']->value]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["item3"]['index']=-1;
foreach ($_from as $_smarty_tpl->tpl_vars["item3"]->key => $_smarty_tpl->tpl_vars["item3"]->value) {
$_smarty_tpl->tpl_vars["item3"]->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["item3"]['index']++;
if ($_smarty_tpl->getVariable('smarty')->value['foreach']['item3']['index']<$_smarty_tpl->tpl_vars['limit']->value) {
$_smarty_tpl->tpl_vars["item3_url"] = new Smarty_variable(fn_form_dropdown_object_link($_smarty_tpl->tpl_vars['item3']->value,$_smarty_tpl->tpl_vars['block']->value['type']), null, 0);?><!-- level 3 item - vertical --><li<?php if ($_smarty_tpl->tpl_vars['item3']->value['active']||fn_check_is_active_menu_item($_smarty_tpl->tpl_vars['item3']->value,$_smarty_tpl->tpl_vars['block']->value['type'])) {?> class="active"<?php }?>><a<?php if ($_smarty_tpl->tpl_vars['item3_url']->value) {?> href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item3_url']->value, ENT_QUOTES, 'ISO-8859-1');?>
"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item3']->value[$_smarty_tpl->tpl_vars['name']->value], ENT_QUOTES, 'ISO-8859-1');?>
</a></li><?php }
}
if ($_smarty_tpl->tpl_vars['limit']->value<$_smarty_tpl->tpl_vars['count_child']->value) {?><li class="view-more"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item2_url']->value, ENT_QUOTES, 'ISO-8859-1');?>
"><?php echo $_smarty_tpl->__("view_more");?>
</a></li><?php }
$_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"blocks:topmenu_dropdown_2levels_col_elements"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</ul><?php }?></div><?php } ?></div><?php } else { ?><a<?php if ($_smarty_tpl->tpl_vars['item1_url']->value) {?> href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1_url']->value, ENT_QUOTES, 'ISO-8859-1');?>
"<?php }?> <?php if ($_smarty_tpl->tpl_vars['item1']->value['new_window']) {?>target="_blank"<?php }?>><span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1']->value[$_smarty_tpl->tpl_vars['name']->value], ENT_QUOTES, 'ISO-8859-1');?>
</span></a><?php }?><!-- end level 2 items - vertical --></li><?php } ?></ul></div><!-- end level 1 items - horizontal --></li><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"blocks:topmenu_dropdown_top_menu"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</ul><div class="clear"></div></div><?php }
$_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"blocks:topmenu_dropdown"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="blocks/topmenu_dropdown.tpl" id="<?php echo smarty_function_set_id(array('name'=>"blocks/topmenu_dropdown.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
$_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"blocks:topmenu_dropdown")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"blocks:topmenu_dropdown"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();
if ($_smarty_tpl->tpl_vars['items']->value) {?><div class="wrap-dropdown-hybrid"><!-- level 1 wrapper - horizontal --><ul class="dropdown-hybrid clearfix"><!-- level 1 items - horizontal --><?php $_smarty_tpl->tpl_vars["page_name"] = new Smarty_variable($_REQUEST['dispatch'], null, 0);
$_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"blocks:topmenu_dropdown_top_menu")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"blocks:topmenu_dropdown_top_menu"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();?>
<li class=""><a class="drop item-1" ><i class="vs-icon-menu"></i><span><?php echo $_smarty_tpl->__("all_categories");?>
</span></a><div class="dropdown-hybrid-column"><!-- level 2 wrapper - vertical --><ul id="vmenu_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['block']->value['block_id'], ENT_QUOTES, 'ISO-8859-1');?>
" class="adropdown adropdown-vertical<?php if ($_smarty_tpl->tpl_vars['block']->value['properties']['right_to_left_orientation']=="Y") {?> rtl<?php }?>"><?php  $_smarty_tpl->tpl_vars["item1"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["item1"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars["item1"]->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars["item1"]->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars["item1"]->key => $_smarty_tpl->tpl_vars["item1"]->value) {
$_smarty_tpl->tpl_vars["item1"]->_loop = true;
 $_smarty_tpl->tpl_vars["item1"]->iteration++;
 $_smarty_tpl->tpl_vars["item1"]->last = $_smarty_tpl->tpl_vars["item1"]->iteration === $_smarty_tpl->tpl_vars["item1"]->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["item1"]['last'] = $_smarty_tpl->tpl_vars["item1"]->last;
$_smarty_tpl->tpl_vars["cols"] = new Smarty_variable(0, null, 0);
if ($_smarty_tpl->tpl_vars['item1']->value['active']||fn_check_is_active_menu_item($_smarty_tpl->tpl_vars['item1']->value,$_smarty_tpl->tpl_vars['block']->value['type'])) {
$_smarty_tpl->tpl_vars["item_1_class"] = new Smarty_variable("active", null, 0);
} else {
$_smarty_tpl->tpl_vars["item_1_class"] = new Smarty_variable('', null, 0);
}
if (!$_smarty_tpl->getVariable('smarty')->value['foreach']['item1']['last']) {
$_smarty_tpl->tpl_vars["item_1_class"] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['item_1_class']->value)." b-border ", null, 0);
}
if ($_smarty_tpl->tpl_vars['item1']->value[$_smarty_tpl->tpl_vars['childs']->value]) {
$_smarty_tpl->tpl_vars["item_1_class"] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['item_1_class']->value)." dir", null, 0);
}?><!-- level 2 items - vertical --><li class="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item_1_class']->value, ENT_QUOTES, 'ISO-8859-1');?>
 adropdown-vertical_li"><?php $_smarty_tpl->tpl_vars["item1_url"] = new Smarty_variable(fn_form_dropdown_object_link($_smarty_tpl->tpl_vars['item1']->value,$_smarty_tpl->tpl_vars['block']->value['type']), null, 0);
if ($_smarty_tpl->tpl_vars['item1']->value[$_smarty_tpl->tpl_vars['childs']->value]) {?><a<?php if ($_smarty_tpl->tpl_vars['item1_url']->value) {?> href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1_url']->value, ENT_QUOTES, 'ISO-8859-1');?>
"<?php }?> <?php if ($_smarty_tpl->tpl_vars['item1']->value['new_window']) {?>target="_blank"<?php }?> class="item-1"><span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1']->value[$_smarty_tpl->tpl_vars['name']->value], ENT_QUOTES, 'ISO-8859-1');?>
</span><i class="vs-icon-arrow-right"></i></a><div class="vs-title-toggle cm-combination visible-phone visible-tablet" id="sw_vs_box_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1']->value['category_id'], ENT_QUOTES, 'ISO-8859-1');?>
000"><i class="ty-sidebox__icon-open ty-icon-down-open"></i><i class="ty-sidebox__icon-hide ty-icon-up-open"></i></div><?php $_smarty_tpl->_capture_stack[0][] = array("dropdown_image", null, null); ob_start();
$_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"blocks:menu_category_image")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"blocks:menu_category_image"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();
$_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"blocks:menu_category_image"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
$_smarty_tpl->_capture_stack[0][] = array("dropdown_image_custom_menu", null, null); ob_start();
$_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"blocks:menu_category_custom_menu")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"blocks:menu_category_custom_menu"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();
$_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"blocks:menu_category_custom_menu"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
$_smarty_tpl->tpl_vars['et_dd_cols_viva'] = new Smarty_variable(true, null, 0);
$_smarty_tpl->_capture_stack[0][] = array("dropdown_image_custom_menu_cols", null, null); ob_start();
$_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"blocks:menu_category_custom_menu_cols")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"blocks:menu_category_custom_menu_cols"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();
$_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"blocks:menu_category_custom_menu_cols"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['dropdown_image_custom_menu'])) {
$_smarty_tpl->tpl_vars["dd_menu"] = new Smarty_variable(trim(Smarty::$_smarty_vars['capture']['dropdown_image_custom_menu']), null, 0);
} else {
$_smarty_tpl->tpl_vars["dd_menu"] = new Smarty_variable('', null, 0);
}
if (trim(Smarty::$_smarty_vars['capture']['dropdown_image'])) {
$_smarty_tpl->tpl_vars['cols'] = new Smarty_variable(5, null, 0);
if (trim(Smarty::$_smarty_vars['capture']['dropdown_image_custom_menu_cols'])) {
$_smarty_tpl->tpl_vars['cols'] = new Smarty_variable(trim(Smarty::$_smarty_vars['capture']['dropdown_image_custom_menu_cols']), null, 0);
}
$_smarty_tpl->tpl_vars["dd_img_class"] = new Smarty_variable("dd-img", null, 0);
} else {
$_smarty_tpl->tpl_vars['cols'] = new Smarty_variable(5, null, 0);
$_smarty_tpl->tpl_vars["dd_img_class"] = new Smarty_variable("no-dd-img", null, 0);
}?><!-- level 3 wrapper - horizontal --><div class="adropdown-fullwidth <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dd_img_class']->value, ENT_QUOTES, 'ISO-8859-1');?>
" style="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['dd_menu']->value, ENT_QUOTES, 'ISO-8859-1');?>
" id="vs_box_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1']->value['category_id'], ENT_QUOTES, 'ISO-8859-1');?>
000"><div class="main-categ-title"><a<?php if ($_smarty_tpl->tpl_vars['item1_url']->value) {?> href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1_url']->value, ENT_QUOTES, 'ISO-8859-1');?>
"<?php }?> <?php if ($_smarty_tpl->tpl_vars['item1']->value['new_window']) {?>target="_blank"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1']->value[$_smarty_tpl->tpl_vars['name']->value], ENT_QUOTES, 'ISO-8859-1');?>
</a></div><?php if (trim(Smarty::$_smarty_vars['capture']['dropdown_image'])) {
echo Smarty::$_smarty_vars['capture']['dropdown_image'];
}
$_smarty_tpl->tpl_vars["item2"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["item2"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['item1']->value[$_smarty_tpl->tpl_vars['childs']->value]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars["item2"]->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars["item2"]->iteration=0;
 $_smarty_tpl->tpl_vars["item2"]->index=-1;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["item2"]['index']=-1;
foreach ($_from as $_smarty_tpl->tpl_vars["item2"]->key => $_smarty_tpl->tpl_vars["item2"]->value) {
$_smarty_tpl->tpl_vars["item2"]->_loop = true;
 $_smarty_tpl->tpl_vars["item2"]->iteration++;
 $_smarty_tpl->tpl_vars["item2"]->index++;
 $_smarty_tpl->tpl_vars["item2"]->first = $_smarty_tpl->tpl_vars["item2"]->index === 0;
 $_smarty_tpl->tpl_vars["item2"]->last = $_smarty_tpl->tpl_vars["item2"]->iteration === $_smarty_tpl->tpl_vars["item2"]->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["item2"]['first'] = $_smarty_tpl->tpl_vars["item2"]->first;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["item2"]['index']++;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["item2"]['last'] = $_smarty_tpl->tpl_vars["item2"]->last;
if ($_smarty_tpl->getVariable('smarty')->value['foreach']['item2']['index']%$_smarty_tpl->tpl_vars['cols']->value==0||$_smarty_tpl->getVariable('smarty')->value['foreach']['item2']['first']) {
$_smarty_tpl->tpl_vars["item_2_class"] = new Smarty_variable("firstcolumn", null, 0);
} elseif ($_smarty_tpl->getVariable('smarty')->value['foreach']['item2']['index']%$_smarty_tpl->tpl_vars['cols']->value==($_smarty_tpl->tpl_vars['cols']->value-1)||$_smarty_tpl->getVariable('smarty')->value['foreach']['item2']['last']) {
$_smarty_tpl->tpl_vars["item_2_class"] = new Smarty_variable("lastcolumn", null, 0);
} else {
$_smarty_tpl->tpl_vars["item_2_class"] = new Smarty_variable('', null, 0);
}?><!-- level 2 item - horizontal --><div class="col-1 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item_2_class']->value, ENT_QUOTES, 'ISO-8859-1');?>
"><?php $_smarty_tpl->tpl_vars["item2_url"] = new Smarty_variable(fn_form_dropdown_object_link($_smarty_tpl->tpl_vars['item2']->value,$_smarty_tpl->tpl_vars['block']->value['type']), null, 0);?><h3<?php if ($_smarty_tpl->tpl_vars['item2']->value['active']||fn_check_is_active_menu_item($_smarty_tpl->tpl_vars['item1']->value,$_smarty_tpl->tpl_vars['block']->value['type'])) {?> class="active"<?php }?>><a<?php if ($_smarty_tpl->tpl_vars['item2_url']->value) {?> href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item2_url']->value, ENT_QUOTES, 'ISO-8859-1');?>
"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item2']->value[$_smarty_tpl->tpl_vars['name']->value], ENT_QUOTES, 'ISO-8859-1');?>
<span class="vs-title-toggle cm-combination visible-phone visible-tablet" id="sw_vs_box_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item2']->value['category_id'], ENT_QUOTES, 'ISO-8859-1');?>
000"><i class="ty-sidebox__icon-open ty-icon-down-open"></i><i class="ty-sidebox__icon-hide ty-icon-up-open"></i></span></a></h3><?php $_smarty_tpl->tpl_vars["cat_image"] = new Smarty_variable(fn_get_image_pairs($_smarty_tpl->tpl_vars['item2']->value['category_id'],'category','M',true,true), null, 0);?><div style="" class="vs-sub-categ-img"><a<?php if ($_smarty_tpl->tpl_vars['item2_url']->value) {?> href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item2_url']->value, ENT_QUOTES, 'ISO-8859-1');?>
"<?php }?> class="vs-sub-categ-img-link"><?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('images'=>$_smarty_tpl->tpl_vars['cat_image']->value['detailed'],'image_width'=>139,'image_height'=>139,'vs_lazy_additional'=>true), 0);?>
</a></div><?php if ($_smarty_tpl->tpl_vars['item2']->value[$_smarty_tpl->tpl_vars['childs']->value]) {?><!-- level 3 wrapper - vertical --><ul id="vs_box_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item2']->value['category_id'], ENT_QUOTES, 'ISO-8859-1');?>
000"><?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"blocks:topmenu_dropdown_2levels_col_elements")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"blocks:topmenu_dropdown_2levels_col_elements"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();
$_smarty_tpl->tpl_vars['limit'] = new Smarty_variable($_smarty_tpl->tpl_vars['block']->value['properties']['dropdown_third_level_elements'], null, 0);
$_smarty_tpl->tpl_vars['count_child'] = new Smarty_variable(count(fn_get_categories_tree($_smarty_tpl->tpl_vars['item2']->value['category_id'])), null, 0);
$_smarty_tpl->tpl_vars["item3"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["item3"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['item2']->value[$_smarty_tpl->tpl_vars['childs']->value]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["item3"]['index']=-1;
foreach ($_from as $_smarty_tpl->tpl_vars["item3"]->key => $_smarty_tpl->tpl_vars["item3"]->value) {
$_smarty_tpl->tpl_vars["item3"]->_loop = true;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']["item3"]['index']++;
if ($_smarty_tpl->getVariable('smarty')->value['foreach']['item3']['index']<$_smarty_tpl->tpl_vars['limit']->value) {
$_smarty_tpl->tpl_vars["item3_url"] = new Smarty_variable(fn_form_dropdown_object_link($_smarty_tpl->tpl_vars['item3']->value,$_smarty_tpl->tpl_vars['block']->value['type']), null, 0);?><!-- level 3 item - vertical --><li<?php if ($_smarty_tpl->tpl_vars['item3']->value['active']||fn_check_is_active_menu_item($_smarty_tpl->tpl_vars['item3']->value,$_smarty_tpl->tpl_vars['block']->value['type'])) {?> class="active"<?php }?>><a<?php if ($_smarty_tpl->tpl_vars['item3_url']->value) {?> href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item3_url']->value, ENT_QUOTES, 'ISO-8859-1');?>
"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item3']->value[$_smarty_tpl->tpl_vars['name']->value], ENT_QUOTES, 'ISO-8859-1');?>
</a></li><?php }
}
if ($_smarty_tpl->tpl_vars['limit']->value<$_smarty_tpl->tpl_vars['count_child']->value) {?><li class="view-more"><a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item2_url']->value, ENT_QUOTES, 'ISO-8859-1');?>
"><?php echo $_smarty_tpl->__("view_more");?>
</a></li><?php }
$_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"blocks:topmenu_dropdown_2levels_col_elements"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</ul><?php }?></div><?php } ?></div><?php } else { ?><a<?php if ($_smarty_tpl->tpl_vars['item1_url']->value) {?> href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1_url']->value, ENT_QUOTES, 'ISO-8859-1');?>
"<?php }?> <?php if ($_smarty_tpl->tpl_vars['item1']->value['new_window']) {?>target="_blank"<?php }?>><span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['item1']->value[$_smarty_tpl->tpl_vars['name']->value], ENT_QUOTES, 'ISO-8859-1');?>
</span></a><?php }?><!-- end level 2 items - vertical --></li><?php } ?></ul></div><!-- end level 1 items - horizontal --></li><?php $_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"blocks:topmenu_dropdown_top_menu"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</ul><div class="clear"></div></div><?php }
$_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"blocks:topmenu_dropdown"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);
}?><?php }} ?>
