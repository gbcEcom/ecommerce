<?php /* Smarty version Smarty-3.1.21, created on 2015-09-01 16:06:32
         compiled from "/home/gbadmin/public_html/production/design/themes/vivashop/templates/views/negotiations/tawar.tpl" */ ?>
<?php /*%%SmartyHeaderCode:125134304155e56a98dfd395-96528663%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '47d07aeefbd11c833c111b909f76ae378ff26aa8' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/themes/vivashop/templates/views/negotiations/tawar.tpl',
      1 => 1441098328,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '125134304155e56a98dfd395-96528663',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'product' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55e56a98ec7e89_49058659',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55e56a98ec7e89_49058659')) {function content_55e56a98ec7e89_49058659($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><div class="brand-wrapper clearfix" style="padding: 10px; background-color: #f9f9f9; margin-bottom: 15px">
    <div class="advance-options-wrapper ty-float-left clearfix">
        <span class="meta-company" style="padding-right: 15px"><b>Toko:</b> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['company_name'], ENT_QUOTES, 'ISO-8859-1');?>
</span>
        <span class="meta-stock" style="padding-right: 15px"><b>Stock:</b> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['amount'], ENT_QUOTES, 'ISO-8859-1');?>
 pcs</span>
        <span class="meta-stock"><b>Kode:</b> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_code'], ENT_QUOTES, 'ISO-8859-1');?>
 pcs</span>
    </div>
</div>

<div class="row-fluid" style="margin-bottom: 15px">
    <div class="span6">
        <div class="image-wrap">
            <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['main_pair']['detailed']['http_image_path'], ENT_QUOTES, 'ISO-8859-1');?>
" style="width: 282px; height: 171px" />
        </div>
    </div>
    <div class="span8">
        <div class="product-info" style="margin-bottom: 30px; max-width: 320px !important;">
            <div class="prices-container price-wrap clearfix product-detail-price">
                <div class="product-price" style="padding: 15px; text-align: center; font-size: 24px; background-color: #F5F5F5">
                    <span style="color: #2dcc70; font-weight: 600">Rp. <?php echo htmlspecialchars(number_format($_smarty_tpl->tpl_vars['product']->value['price'],0,',','.'), ENT_QUOTES, 'ISO-8859-1');?>
</span>
                </div>
            </div>
        </div>
        <section>
            <form method="post" action="<?php echo htmlspecialchars(fn_url("negotiations.submit"), ENT_QUOTES, 'ISO-8859-1');?>
">
                <input type="hidden" name="company_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['company_id'], ENT_QUOTES, 'ISO-8859-1');?>
" />
                <input type="hidden" name="product_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_id'], ENT_QUOTES, 'ISO-8859-1');?>
" />
                <div class="ty-control-group">
                    <input style="padding: 8px" type="number" class="ty-input-text-medium" name="amount" placeholder="Tawar harga anda" />
                </div>
                <div class="controls">
                    <button type="submit" class="ty-btn ty-btn__primary">Tawar</button>
                </div>
            </form>
        </section>
    </div>
</div>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start(); ?><span>Tawar <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product'], ENT_QUOTES, 'ISO-8859-1');?>
</span><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="views/negotiations/tawar.tpl" id="<?php echo smarty_function_set_id(array('name'=>"views/negotiations/tawar.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><div class="brand-wrapper clearfix" style="padding: 10px; background-color: #f9f9f9; margin-bottom: 15px">
    <div class="advance-options-wrapper ty-float-left clearfix">
        <span class="meta-company" style="padding-right: 15px"><b>Toko:</b> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['company_name'], ENT_QUOTES, 'ISO-8859-1');?>
</span>
        <span class="meta-stock" style="padding-right: 15px"><b>Stock:</b> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['amount'], ENT_QUOTES, 'ISO-8859-1');?>
 pcs</span>
        <span class="meta-stock"><b>Kode:</b> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_code'], ENT_QUOTES, 'ISO-8859-1');?>
 pcs</span>
    </div>
</div>

<div class="row-fluid" style="margin-bottom: 15px">
    <div class="span6">
        <div class="image-wrap">
            <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['main_pair']['detailed']['http_image_path'], ENT_QUOTES, 'ISO-8859-1');?>
" style="width: 282px; height: 171px" />
        </div>
    </div>
    <div class="span8">
        <div class="product-info" style="margin-bottom: 30px; max-width: 320px !important;">
            <div class="prices-container price-wrap clearfix product-detail-price">
                <div class="product-price" style="padding: 15px; text-align: center; font-size: 24px; background-color: #F5F5F5">
                    <span style="color: #2dcc70; font-weight: 600">Rp. <?php echo htmlspecialchars(number_format($_smarty_tpl->tpl_vars['product']->value['price'],0,',','.'), ENT_QUOTES, 'ISO-8859-1');?>
</span>
                </div>
            </div>
        </div>
        <section>
            <form method="post" action="<?php echo htmlspecialchars(fn_url("negotiations.submit"), ENT_QUOTES, 'ISO-8859-1');?>
">
                <input type="hidden" name="company_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['company_id'], ENT_QUOTES, 'ISO-8859-1');?>
" />
                <input type="hidden" name="product_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product_id'], ENT_QUOTES, 'ISO-8859-1');?>
" />
                <div class="ty-control-group">
                    <input style="padding: 8px" type="number" class="ty-input-text-medium" name="amount" placeholder="Tawar harga anda" />
                </div>
                <div class="controls">
                    <button type="submit" class="ty-btn ty-btn__primary">Tawar</button>
                </div>
            </form>
        </section>
    </div>
</div>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start(); ?><span>Tawar <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['product'], ENT_QUOTES, 'ISO-8859-1');?>
</span><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
}?><?php }} ?>
