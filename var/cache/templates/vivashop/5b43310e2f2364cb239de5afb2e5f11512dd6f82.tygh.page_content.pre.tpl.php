<?php /* Smarty version Smarty-3.1.21, created on 2015-08-10 22:52:22
         compiled from "/home/gbadmin/public_html/production/design/themes/vivashop/templates/addons/blog/hooks/pages/page_content.pre.tpl" */ ?>
<?php /*%%SmartyHeaderCode:181795685855c8c8b65bc327-29488577%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5b43310e2f2364cb239de5afb2e5f11512dd6f82' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/themes/vivashop/templates/addons/blog/hooks/pages/page_content.pre.tpl',
      1 => 1438219694,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '181795685855c8c8b65bc327-29488577',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'page' => 0,
    'settings' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55c8c8b66658e9_04165649',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55c8c8b66658e9_04165649')) {function content_55c8c8b66658e9_04165649($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/modifier.date_format.php';
if (!is_callable('smarty_block_hook')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/block.hook.php';
if (!is_callable('smarty_function_set_id')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/function.set_id.php';
?><?php
fn_preload_lang_vars(array('posted_by','posted_by'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
if ($_smarty_tpl->tpl_vars['page']->value['description']&&$_smarty_tpl->tpl_vars['page']->value['page_type']==@constant('PAGE_TYPE_BLOG')) {?>
    <div class="vs-blog-post-wrapper">
    	<div class="vs-blog-details">
    		<div class="ty-blog__author"><?php echo $_smarty_tpl->__("posted_by");?>
&nbsp;&nbsp;<i class="vs-icon-user"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['author'], ENT_QUOTES, 'ISO-8859-1');?>
</div><div class="ty-blog__date"><i class="ty-icon-calendar"></i> <?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['page']->value['timestamp'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format'])), ENT_QUOTES, 'ISO-8859-1');?>
</div><?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"blog:post")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"blog:post"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();
$_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"blog:post"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

	    </div>
    	<?php if ($_smarty_tpl->tpl_vars['page']->value['main_pair']) {?>
    	    <div class="ty-blog__img-block">
    	        <?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('image_width'=>"894",'obj_id'=>$_smarty_tpl->tpl_vars['page']->value['page_id'],'images'=>$_smarty_tpl->tpl_vars['page']->value['main_pair']), 0);?>

    	    </div>
    	<?php }?>
    </div>
<?php }?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/blog/hooks/pages/page_content.pre.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/blog/hooks/pages/page_content.pre.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
if ($_smarty_tpl->tpl_vars['page']->value['description']&&$_smarty_tpl->tpl_vars['page']->value['page_type']==@constant('PAGE_TYPE_BLOG')) {?>
    <div class="vs-blog-post-wrapper">
    	<div class="vs-blog-details">
    		<div class="ty-blog__author"><?php echo $_smarty_tpl->__("posted_by");?>
&nbsp;&nbsp;<i class="vs-icon-user"></i> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['author'], ENT_QUOTES, 'ISO-8859-1');?>
</div><div class="ty-blog__date"><i class="ty-icon-calendar"></i> <?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['page']->value['timestamp'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format'])), ENT_QUOTES, 'ISO-8859-1');?>
</div><?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"blog:post")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"blog:post"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();
$_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"blog:post"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>

	    </div>
    	<?php if ($_smarty_tpl->tpl_vars['page']->value['main_pair']) {?>
    	    <div class="ty-blog__img-block">
    	        <?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('image_width'=>"894",'obj_id'=>$_smarty_tpl->tpl_vars['page']->value['page_id'],'images'=>$_smarty_tpl->tpl_vars['page']->value['main_pair']), 0);?>

    	    </div>
    	<?php }?>
    </div>
<?php }?>
<?php }?><?php }} ?>
