<?php /* Smarty version Smarty-3.1.21, created on 2015-08-10 23:42:19
         compiled from "/home/gbadmin/public_html/production/design/themes/vivashop/templates/addons/blog/blocks/recent_posts.tpl" */ ?>
<?php /*%%SmartyHeaderCode:138735415655c8d46b6e98f5-02488040%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '73eb4269434c78c78cbdf4ce815f16633298c969' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/themes/vivashop/templates/addons/blog/blocks/recent_posts.tpl',
      1 => 1438219694,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '138735415655c8d46b6e98f5-02488040',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'items' => 0,
    'page' => 0,
    'data' => 0,
    'settings' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55c8d46b7b6486_41632253',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55c8d46b7b6486_41632253')) {function content_55c8d46b7b6486_41632253($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/modifier.date_format.php';
if (!is_callable('smarty_block_hook')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/block.hook.php';
if (!is_callable('smarty_function_set_id')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?>

<?php if ($_smarty_tpl->tpl_vars['items']->value) {?>

<div class="ty-blog-sidebox vs-blog-sidebar">
    <ul class="ty-blog-sidebox__list">
<?php  $_smarty_tpl->tpl_vars["page"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["page"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["page"]->key => $_smarty_tpl->tpl_vars["page"]->value) {
$_smarty_tpl->tpl_vars["page"]->_loop = true;
?>
        <li class="ty-blog-sidebox__item clearfix">
        	<?php $_smarty_tpl->tpl_vars['data'] = new Smarty_variable(fn_get_page_data($_smarty_tpl->tpl_vars['page']->value['page_id']), null, 0);?>
        	<?php if ($_smarty_tpl->tpl_vars['data']->value['main_pair']) {?>
        	    <div class="ty-blog__img-block ty-float-left">
        	    	<a href="<?php echo htmlspecialchars(fn_url("pages.view?page_id=".((string)$_smarty_tpl->tpl_vars['page']->value['page_id'])), ENT_QUOTES, 'ISO-8859-1');?>
">
        	        <?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('image_width'=>"52",'image_height'=>"38",'obj_id'=>$_smarty_tpl->tpl_vars['data']->value['page_id'],'images'=>$_smarty_tpl->tpl_vars['data']->value['main_pair']), 0);?>

        	        </a>
        	    </div>
        	<?php }?>
        	<div class="ty-float-left vs-blog-text-wrapper">
	            <a class="vs-blog-title" href="<?php echo htmlspecialchars(fn_url("pages.view?page_id=".((string)$_smarty_tpl->tpl_vars['page']->value['page_id'])), ENT_QUOTES, 'ISO-8859-1');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['page'], ENT_QUOTES, 'ISO-8859-1');?>
</a>
	            <div class="blog_sidebar_recent"><div class="ty-blog__date"><i class="ty-icon-calendar"></i> <?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['page']->value['timestamp'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format'])), ENT_QUOTES, 'ISO-8859-1');?>
</div><?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"blog:sidebar")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"blog:sidebar"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();
$_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"blog:sidebar"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</div>
	    	</div>
        </li>
<?php } ?>
    </ul>
</div>

<?php }
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/blog/blocks/recent_posts.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/blog/blocks/recent_posts.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?>

<?php if ($_smarty_tpl->tpl_vars['items']->value) {?>

<div class="ty-blog-sidebox vs-blog-sidebar">
    <ul class="ty-blog-sidebox__list">
<?php  $_smarty_tpl->tpl_vars["page"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["page"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["page"]->key => $_smarty_tpl->tpl_vars["page"]->value) {
$_smarty_tpl->tpl_vars["page"]->_loop = true;
?>
        <li class="ty-blog-sidebox__item clearfix">
        	<?php $_smarty_tpl->tpl_vars['data'] = new Smarty_variable(fn_get_page_data($_smarty_tpl->tpl_vars['page']->value['page_id']), null, 0);?>
        	<?php if ($_smarty_tpl->tpl_vars['data']->value['main_pair']) {?>
        	    <div class="ty-blog__img-block ty-float-left">
        	    	<a href="<?php echo htmlspecialchars(fn_url("pages.view?page_id=".((string)$_smarty_tpl->tpl_vars['page']->value['page_id'])), ENT_QUOTES, 'ISO-8859-1');?>
">
        	        <?php echo $_smarty_tpl->getSubTemplate ("common/image.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('image_width'=>"52",'image_height'=>"38",'obj_id'=>$_smarty_tpl->tpl_vars['data']->value['page_id'],'images'=>$_smarty_tpl->tpl_vars['data']->value['main_pair']), 0);?>

        	        </a>
        	    </div>
        	<?php }?>
        	<div class="ty-float-left vs-blog-text-wrapper">
	            <a class="vs-blog-title" href="<?php echo htmlspecialchars(fn_url("pages.view?page_id=".((string)$_smarty_tpl->tpl_vars['page']->value['page_id'])), ENT_QUOTES, 'ISO-8859-1');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['page'], ENT_QUOTES, 'ISO-8859-1');?>
</a>
	            <div class="blog_sidebar_recent"><div class="ty-blog__date"><i class="ty-icon-calendar"></i> <?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['page']->value['timestamp'],((string)$_smarty_tpl->tpl_vars['settings']->value['Appearance']['date_format'])), ENT_QUOTES, 'ISO-8859-1');?>
</div><?php $_smarty_tpl->smarty->_tag_stack[] = array('hook', array('name'=>"blog:sidebar")); $_block_repeat=true; echo smarty_block_hook(array('name'=>"blog:sidebar"), null, $_smarty_tpl, $_block_repeat);while ($_block_repeat) { ob_start();
$_block_content = ob_get_clean(); $_block_repeat=false; echo smarty_block_hook(array('name'=>"blog:sidebar"), $_block_content, $_smarty_tpl, $_block_repeat);  } array_pop($_smarty_tpl->smarty->_tag_stack);?>
</div>
	    	</div>
        </li>
<?php } ?>
    </ul>
</div>

<?php }
}?><?php }} ?>
