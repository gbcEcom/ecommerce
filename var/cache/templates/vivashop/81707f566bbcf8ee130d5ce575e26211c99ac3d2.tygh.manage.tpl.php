<?php /* Smarty version Smarty-3.1.21, created on 2015-09-01 16:08:07
         compiled from "/home/gbadmin/public_html/production/design/themes/vivashop/templates/views/negotiations/manage.tpl" */ ?>
<?php /*%%SmartyHeaderCode:196759786655e56af78e12f8-08852675%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '81707f566bbcf8ee130d5ce575e26211c99ac3d2' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/themes/vivashop/templates/views/negotiations/manage.tpl',
      1 => 1441098320,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '196759786655e56af78e12f8-08852675',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'histories' => 0,
    'nego' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55e56af79fa2d4_94361264',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55e56af79fa2d4_94361264')) {function content_55e56af79fa2d4_94361264($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?><table class="ty-table">
    <thead>
        <tr>
            <th width="15%">Tanggal</th>
            <th width="25%">Produk</th>
            <th width="15%">Harga</th>
            <th width="15%">Nego</th>
            <th width="10%">Status</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php if (count($_smarty_tpl->tpl_vars['histories']->value)) {?>
            <?php  $_smarty_tpl->tpl_vars["nego"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["nego"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['histories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["nego"]->key => $_smarty_tpl->tpl_vars["nego"]->value) {
$_smarty_tpl->tpl_vars["nego"]->_loop = true;
?>
                <?php $_smarty_tpl->tpl_vars["product_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['nego']->value['product_id'], null, 0);?>
                <?php $_smarty_tpl->tpl_vars["nego_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['nego']->value['nego_id'], null, 0);?>
                <tr>
                    <td><?php echo htmlspecialchars(date('d M Y',$_smarty_tpl->tpl_vars['nego']->value['timestamp']), ENT_QUOTES, 'ISO-8859-1');?>
</td>
                    <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nego']->value['product'], ENT_QUOTES, 'ISO-8859-1');?>
</td>
                    <td>Rp. <?php echo htmlspecialchars(number_format($_smarty_tpl->tpl_vars['nego']->value['price'],0,',','.'), ENT_QUOTES, 'ISO-8859-1');?>
</td>
                    <td>Rp. <?php echo htmlspecialchars(number_format($_smarty_tpl->tpl_vars['nego']->value['amount'],0,',','.'), ENT_QUOTES, 'ISO-8859-1');?>
</td>
                    <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nego']->value['label'], ENT_QUOTES, 'ISO-8859-1');?>
</td>
                    <td style="text-align: center">
                        <?php if ($_smarty_tpl->tpl_vars['nego']->value['status']==1) {?>
                            <a href="<?php echo htmlspecialchars(fn_url("products.view&product_id=".((string)$_smarty_tpl->tpl_vars['product_id']->value)."&nego_to_buy=".((string)$_smarty_tpl->tpl_vars['nego_id']->value)), ENT_QUOTES, 'ISO-8859-1');?>
" class="ty-btn ty-btn__secondary">BELI</a>
                        <?php } else { ?>
                            <a href="<?php echo htmlspecialchars(fn_url("negotiations.tawar&product_id=".((string)$_smarty_tpl->tpl_vars['product_id']->value)), ENT_QUOTES, 'ISO-8859-1');?>
" class="ty-btn ty-btn__secondary">NEGO ULANG</a>
                        <?php }?>
                    </td>
                </tr>
            <?php } ?>
        <?php } else { ?>
            <tr>
                <td colspan="6">Tidak ada sejarah tawar produk</td>
            </tr>
        <?php }?>
    </tbody>
</table>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start(); ?><span>History Nego Produk</span><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="views/negotiations/manage.tpl" id="<?php echo smarty_function_set_id(array('name'=>"views/negotiations/manage.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?><table class="ty-table">
    <thead>
        <tr>
            <th width="15%">Tanggal</th>
            <th width="25%">Produk</th>
            <th width="15%">Harga</th>
            <th width="15%">Nego</th>
            <th width="10%">Status</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php if (count($_smarty_tpl->tpl_vars['histories']->value)) {?>
            <?php  $_smarty_tpl->tpl_vars["nego"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["nego"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['histories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["nego"]->key => $_smarty_tpl->tpl_vars["nego"]->value) {
$_smarty_tpl->tpl_vars["nego"]->_loop = true;
?>
                <?php $_smarty_tpl->tpl_vars["product_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['nego']->value['product_id'], null, 0);?>
                <?php $_smarty_tpl->tpl_vars["nego_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['nego']->value['nego_id'], null, 0);?>
                <tr>
                    <td><?php echo htmlspecialchars(date('d M Y',$_smarty_tpl->tpl_vars['nego']->value['timestamp']), ENT_QUOTES, 'ISO-8859-1');?>
</td>
                    <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nego']->value['product'], ENT_QUOTES, 'ISO-8859-1');?>
</td>
                    <td>Rp. <?php echo htmlspecialchars(number_format($_smarty_tpl->tpl_vars['nego']->value['price'],0,',','.'), ENT_QUOTES, 'ISO-8859-1');?>
</td>
                    <td>Rp. <?php echo htmlspecialchars(number_format($_smarty_tpl->tpl_vars['nego']->value['amount'],0,',','.'), ENT_QUOTES, 'ISO-8859-1');?>
</td>
                    <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nego']->value['label'], ENT_QUOTES, 'ISO-8859-1');?>
</td>
                    <td style="text-align: center">
                        <?php if ($_smarty_tpl->tpl_vars['nego']->value['status']==1) {?>
                            <a href="<?php echo htmlspecialchars(fn_url("products.view&product_id=".((string)$_smarty_tpl->tpl_vars['product_id']->value)."&nego_to_buy=".((string)$_smarty_tpl->tpl_vars['nego_id']->value)), ENT_QUOTES, 'ISO-8859-1');?>
" class="ty-btn ty-btn__secondary">BELI</a>
                        <?php } else { ?>
                            <a href="<?php echo htmlspecialchars(fn_url("negotiations.tawar&product_id=".((string)$_smarty_tpl->tpl_vars['product_id']->value)), ENT_QUOTES, 'ISO-8859-1');?>
" class="ty-btn ty-btn__secondary">NEGO ULANG</a>
                        <?php }?>
                    </td>
                </tr>
            <?php } ?>
        <?php } else { ?>
            <tr>
                <td colspan="6">Tidak ada sejarah tawar produk</td>
            </tr>
        <?php }?>
    </tbody>
</table>

<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start(); ?><span>History Nego Produk</span><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
}?><?php }} ?>
