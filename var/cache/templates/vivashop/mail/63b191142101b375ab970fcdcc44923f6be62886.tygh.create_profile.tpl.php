<?php /* Smarty version Smarty-3.1.21, created on 2015-08-10 23:49:02
         compiled from "/home/gbadmin/public_html/production/design/themes/vivashop/mail/templates/addons/hybrid_auth/create_profile.tpl" */ ?>
<?php /*%%SmartyHeaderCode:72279792455c8d5fe5aaf93-04538107%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '63b191142101b375ab970fcdcc44923f6be62886' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/themes/vivashop/mail/templates/addons/hybrid_auth/create_profile.tpl',
      1 => 1438219703,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '72279792455c8d5fe5aaf93-04538107',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'user_data' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55c8d5fe63e209_59675158',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55c8d5fe63e209_59675158')) {function content_55c8d5fe63e209_59675158($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/function.set_id.php';
?><?php
fn_preload_lang_vars(array('dear','hybrid_auth.password_generated','hybrid_auth.change_password','dear','hybrid_auth.password_generated','hybrid_auth.change_password'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
echo $_smarty_tpl->getSubTemplate ("common/letter_header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->__("dear");?>
 <?php if ($_smarty_tpl->tpl_vars['user_data']->value['user_name']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['user_data']->value['user_name'], ENT_QUOTES, 'ISO-8859-1');
} else {
echo htmlspecialchars(strtolower(fn_get_user_type_description($_smarty_tpl->tpl_vars['user_data']->value['user_type'])), ENT_QUOTES, 'ISO-8859-1');
}?>,<br>
<br>
<?php echo $_smarty_tpl->__("hybrid_auth.password_generated");?>
: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_data']->value['password'], ENT_QUOTES, 'ISO-8859-1');?>
<br>
<br />
<?php echo $_smarty_tpl->__("hybrid_auth.change_password");?>
: <url><br> <?php echo htmlspecialchars(fn_url("profiles.update"), ENT_QUOTES, 'ISO-8859-1');?>

<br />
<?php echo $_smarty_tpl->getSubTemplate ("common/letter_footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/hybrid_auth/create_profile.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/hybrid_auth/create_profile.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
echo $_smarty_tpl->getSubTemplate ("common/letter_header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php echo $_smarty_tpl->__("dear");?>
 <?php if ($_smarty_tpl->tpl_vars['user_data']->value['user_name']) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['user_data']->value['user_name'], ENT_QUOTES, 'ISO-8859-1');
} else {
echo htmlspecialchars(strtolower(fn_get_user_type_description($_smarty_tpl->tpl_vars['user_data']->value['user_type'])), ENT_QUOTES, 'ISO-8859-1');
}?>,<br>
<br>
<?php echo $_smarty_tpl->__("hybrid_auth.password_generated");?>
: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_data']->value['password'], ENT_QUOTES, 'ISO-8859-1');?>
<br>
<br />
<?php echo $_smarty_tpl->__("hybrid_auth.change_password");?>
: <url><br> <?php echo htmlspecialchars(fn_url("profiles.update"), ENT_QUOTES, 'ISO-8859-1');?>

<br />
<?php echo $_smarty_tpl->getSubTemplate ("common/letter_footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

<?php }?><?php }} ?>
