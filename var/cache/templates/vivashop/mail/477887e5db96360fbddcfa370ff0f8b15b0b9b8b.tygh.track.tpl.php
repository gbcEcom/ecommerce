<?php /* Smarty version Smarty-3.1.21, created on 2015-08-11 11:11:49
         compiled from "/home/gbadmin/public_html/production/design/themes/vivashop/mail/templates/orders/track.tpl" */ ?>
<?php /*%%SmartyHeaderCode:170950228355c97605180383-51946366%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '477887e5db96360fbddcfa370ff0f8b15b0b9b8b' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/themes/vivashop/mail/templates/orders/track.tpl',
      1 => 1438219703,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '170950228355c97605180383-51946366',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'o_id' => 0,
    'access_key' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55c97605243447_96554675',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55c97605243447_96554675')) {function content_55c97605243447_96554675($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/function.set_id.php';
?><?php
fn_preload_lang_vars(array('hello','text_track_request','text_track_view_order','text_track_view_all_orders','hello','text_track_request','text_track_view_order','text_track_view_all_orders'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
echo $_smarty_tpl->getSubTemplate ("common/letter_header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<?php echo $_smarty_tpl->__("hello");?>
,<br /><br />

<?php echo $_smarty_tpl->__("text_track_request");?>
<br /><br />

<?php if ($_smarty_tpl->tpl_vars['o_id']->value) {?>
<?php echo $_smarty_tpl->__("text_track_view_order",array("[order]"=>$_smarty_tpl->tpl_vars['o_id']->value));?>
<br />
<a href="<?php echo htmlspecialchars(fn_url("orders.track?ekey=".((string)$_smarty_tpl->tpl_vars['access_key']->value)."&o_id=".((string)$_smarty_tpl->tpl_vars['o_id']->value),'C','http'), ENT_QUOTES, 'ISO-8859-1');?>
"><?php echo htmlspecialchars(fn_url("orders.track?ekey=".((string)$_smarty_tpl->tpl_vars['access_key']->value)."&o_id=".((string)$_smarty_tpl->tpl_vars['o_id']->value),'C','http'), ENT_QUOTES, 'ISO-8859-1');?>
</a><br />
<br />
<?php }?>

<?php echo $_smarty_tpl->__("text_track_view_all_orders");?>
<br />
<a href="<?php echo htmlspecialchars(fn_url("orders.track?ekey=".((string)$_smarty_tpl->tpl_vars['access_key']->value),'C','http'), ENT_QUOTES, 'ISO-8859-1');?>
"><?php echo htmlspecialchars(fn_url("orders.track?ekey=".((string)$_smarty_tpl->tpl_vars['access_key']->value),'C','http'), ENT_QUOTES, 'ISO-8859-1');?>
</a><br />

<?php echo $_smarty_tpl->getSubTemplate ("common/letter_footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="orders/track.tpl" id="<?php echo smarty_function_set_id(array('name'=>"orders/track.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
echo $_smarty_tpl->getSubTemplate ("common/letter_header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<?php echo $_smarty_tpl->__("hello");?>
,<br /><br />

<?php echo $_smarty_tpl->__("text_track_request");?>
<br /><br />

<?php if ($_smarty_tpl->tpl_vars['o_id']->value) {?>
<?php echo $_smarty_tpl->__("text_track_view_order",array("[order]"=>$_smarty_tpl->tpl_vars['o_id']->value));?>
<br />
<a href="<?php echo htmlspecialchars(fn_url("orders.track?ekey=".((string)$_smarty_tpl->tpl_vars['access_key']->value)."&o_id=".((string)$_smarty_tpl->tpl_vars['o_id']->value),'C','http'), ENT_QUOTES, 'ISO-8859-1');?>
"><?php echo htmlspecialchars(fn_url("orders.track?ekey=".((string)$_smarty_tpl->tpl_vars['access_key']->value)."&o_id=".((string)$_smarty_tpl->tpl_vars['o_id']->value),'C','http'), ENT_QUOTES, 'ISO-8859-1');?>
</a><br />
<br />
<?php }?>

<?php echo $_smarty_tpl->__("text_track_view_all_orders");?>
<br />
<a href="<?php echo htmlspecialchars(fn_url("orders.track?ekey=".((string)$_smarty_tpl->tpl_vars['access_key']->value),'C','http'), ENT_QUOTES, 'ISO-8859-1');?>
"><?php echo htmlspecialchars(fn_url("orders.track?ekey=".((string)$_smarty_tpl->tpl_vars['access_key']->value),'C','http'), ENT_QUOTES, 'ISO-8859-1');?>
</a><br />

<?php echo $_smarty_tpl->getSubTemplate ("common/letter_footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);
}?><?php }} ?>
