<?php /* Smarty version Smarty-3.1.21, created on 2015-10-21 07:54:31
         compiled from "/home/gbadmin/public_html/production/design/themes/vivashop/templates/views/messages/view.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17174124765626e247698940-46621964%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '22c7c5457d6ed8d96a7bd773bd9012d3667388b7' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/themes/vivashop/templates/views/messages/view.tpl',
      1 => 1438219702,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '17174124765626e247698940-46621964',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'message' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5626e2477d28a0_53090301',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5626e2477d28a0_53090301')) {function content_5626e2477d28a0_53090301($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start(); ?>
<div class="box" style="margin-top: 0px !important; margin-bottom: 15px">
    <div class="box-header">
        <div class="row-fluid">
            <div class="span8">
                <h4>Percakapan dengan <span style="color: #0092ef"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['company'], ENT_QUOTES, 'ISO-8859-1');?>
</span></h4>
            </div>
            <div class="span8">
                <small class="ty-float-right"><?php echo htmlspecialchars(date('d/m/Y H:i',$_smarty_tpl->tpl_vars['message']->value['timestamp']), ENT_QUOTES, 'ISO-8859-1');?>
</small>
            </div>
        </div>
    </div>
    <div class="box-body">
        <!-- Sender Information -->
        <div class="" style="border-bottom: 1px solid #dedede; margin-bottom: 20px; display: none">
            <div>From: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['company'], ENT_QUOTES, 'ISO-8859-1');?>
</div>
            <div style="display: none">Belanja Lebih Mudah dan Murah, Cuma di RB Fashion !</div>
        </div>

        <small style="font-style: italic; color: #888"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['company'], ENT_QUOTES, 'ISO-8859-1');?>
 Say:</small>
        <div class="message" style="border: 1px solid #dedede; padding: 15px; border-radius: 5px; background-color: #f9f9f9; margin-bottom: 20px;">
            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['message'], ENT_QUOTES, 'ISO-8859-1');?>

        </div>

        <form method="post" action="<?php echo htmlspecialchars(fn_url("messages.send"), ENT_QUOTES, 'ISO-8859-1');?>
" class="form">
            <input type="hidden" name="recipient" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['company_id'], ENT_QUOTES, 'ISO-8859-1');?>
" />
            <input type="hidden" name="recipient_type" value="V" />
            <input type="hidden" name="parent_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['message_id'], ENT_QUOTES, 'ISO-8859-1');?>
" />
            <div class="ty-control-group">
                <textarea class="ty-input-text-full ty-input-textarea" name="message" rows="5" placeholder="Ketik pesan..." required="required" autofocus="autofocus"></textarea>
            </div>
            <div class="controls">
                <button class="ty-btn ty-btn__secondary" type="button" onclick="self.history.back(-1)">Kembali</button>
                <button class="ty-btn ty-btn__primary ty-float-right" type="submit">Kirim</button>
            </div>
        </form>
    </div>
</div><?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="views/messages/view.tpl" id="<?php echo smarty_function_set_id(array('name'=>"views/messages/view.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else { ?>
<div class="box" style="margin-top: 0px !important; margin-bottom: 15px">
    <div class="box-header">
        <div class="row-fluid">
            <div class="span8">
                <h4>Percakapan dengan <span style="color: #0092ef"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['company'], ENT_QUOTES, 'ISO-8859-1');?>
</span></h4>
            </div>
            <div class="span8">
                <small class="ty-float-right"><?php echo htmlspecialchars(date('d/m/Y H:i',$_smarty_tpl->tpl_vars['message']->value['timestamp']), ENT_QUOTES, 'ISO-8859-1');?>
</small>
            </div>
        </div>
    </div>
    <div class="box-body">
        <!-- Sender Information -->
        <div class="" style="border-bottom: 1px solid #dedede; margin-bottom: 20px; display: none">
            <div>From: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['company'], ENT_QUOTES, 'ISO-8859-1');?>
</div>
            <div style="display: none">Belanja Lebih Mudah dan Murah, Cuma di RB Fashion !</div>
        </div>

        <small style="font-style: italic; color: #888"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['company'], ENT_QUOTES, 'ISO-8859-1');?>
 Say:</small>
        <div class="message" style="border: 1px solid #dedede; padding: 15px; border-radius: 5px; background-color: #f9f9f9; margin-bottom: 20px;">
            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['message'], ENT_QUOTES, 'ISO-8859-1');?>

        </div>

        <form method="post" action="<?php echo htmlspecialchars(fn_url("messages.send"), ENT_QUOTES, 'ISO-8859-1');?>
" class="form">
            <input type="hidden" name="recipient" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['company_id'], ENT_QUOTES, 'ISO-8859-1');?>
" />
            <input type="hidden" name="recipient_type" value="V" />
            <input type="hidden" name="parent_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['message_id'], ENT_QUOTES, 'ISO-8859-1');?>
" />
            <div class="ty-control-group">
                <textarea class="ty-input-text-full ty-input-textarea" name="message" rows="5" placeholder="Ketik pesan..." required="required" autofocus="autofocus"></textarea>
            </div>
            <div class="controls">
                <button class="ty-btn ty-btn__secondary" type="button" onclick="self.history.back(-1)">Kembali</button>
                <button class="ty-btn ty-btn__primary ty-float-right" type="submit">Kirim</button>
            </div>
        </form>
    </div>
</div><?php }?><?php }} ?>
