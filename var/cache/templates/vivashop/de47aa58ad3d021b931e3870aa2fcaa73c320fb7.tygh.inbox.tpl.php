<?php /* Smarty version Smarty-3.1.21, created on 2015-08-11 22:55:28
         compiled from "/home/gbadmin/public_html/production/design/themes/vivashop/templates/views/messages/inbox.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6863615155ca1af060b1a2-43342755%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'de47aa58ad3d021b931e3870aa2fcaa73c320fb7' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/themes/vivashop/templates/views/messages/inbox.tpl',
      1 => 1438219702,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '6863615155ca1af060b1a2-43342755',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'messages' => 0,
    'message' => 0,
    '_title' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55ca1af06dec56_85509645',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55ca1af06dec56_85509645')) {function content_55ca1af06dec56_85509645($_smarty_tpl) {?><?php if (!is_callable('smarty_function_set_id')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/function.set_id.php';
?><?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
$_smarty_tpl->tpl_vars["_title"] = new Smarty_variable("Inbox", null, 0);?>
<div class="message-box">
    <table class="ty-table" style="margin-top: 0px !important;">
        <thead>
        <tr>
            <th width="25%">Pengirim</th>
            <th width="55%">Pesan</th>
            <th width="20%">Tanggal</th>
        </tr>
        </thead>

        <tbody>
        <?php if (count($_smarty_tpl->tpl_vars['messages']->value)) {?>
            <?php  $_smarty_tpl->tpl_vars["message"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["message"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['messages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["message"]->key => $_smarty_tpl->tpl_vars["message"]->value) {
$_smarty_tpl->tpl_vars["message"]->_loop = true;
?>
                <?php if (empty($_smarty_tpl->tpl_vars['message']->value['is_read'])) {?>
                    <tr style="cursor: pointer; font-weight: bold" onclick="location.href='index.php?dispatch=messages.view&msg_id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['message_id'], ENT_QUOTES, 'ISO-8859-1');?>
';">
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['company'], ENT_QUOTES, 'ISO-8859-1');?>
</td>
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['message'], ENT_QUOTES, 'ISO-8859-1');?>
</td>
                        <td><?php echo htmlspecialchars(date('F d, Y H:i',$_smarty_tpl->tpl_vars['message']->value['timestamp']), ENT_QUOTES, 'ISO-8859-1');?>
</td>
                    </tr>
                <?php } else { ?>
                    <tr class="" style="cursor: pointer" onclick="location.href='index.php?dispatch=messages.view&msg_id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['message_id'], ENT_QUOTES, 'ISO-8859-1');?>
';">
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['message_id'], ENT_QUOTES, 'ISO-8859-1');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['company'], ENT_QUOTES, 'ISO-8859-1');?>
</td>
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['message'], ENT_QUOTES, 'ISO-8859-1');?>
</td>
                        <td><?php echo htmlspecialchars(date('F d, Y H:i',$_smarty_tpl->tpl_vars['message']->value['timestamp']), ENT_QUOTES, 'ISO-8859-1');?>
</td>
                    </tr>
                <?php }?>
            <?php } ?>
        <?php } else { ?>
            <tr>
                <td colspan="3">Data pesan belum ada !</td>
            </tr>
        <?php }?>
        </tbody>
    </table>
</div>
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo htmlspecialchars($_smarty_tpl->tpl_vars['_title']->value, ENT_QUOTES, 'ISO-8859-1');
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="views/messages/inbox.tpl" id="<?php echo smarty_function_set_id(array('name'=>"views/messages/inbox.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
$_smarty_tpl->tpl_vars["_title"] = new Smarty_variable("Inbox", null, 0);?>
<div class="message-box">
    <table class="ty-table" style="margin-top: 0px !important;">
        <thead>
        <tr>
            <th width="25%">Pengirim</th>
            <th width="55%">Pesan</th>
            <th width="20%">Tanggal</th>
        </tr>
        </thead>

        <tbody>
        <?php if (count($_smarty_tpl->tpl_vars['messages']->value)) {?>
            <?php  $_smarty_tpl->tpl_vars["message"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["message"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['messages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["message"]->key => $_smarty_tpl->tpl_vars["message"]->value) {
$_smarty_tpl->tpl_vars["message"]->_loop = true;
?>
                <?php if (empty($_smarty_tpl->tpl_vars['message']->value['is_read'])) {?>
                    <tr style="cursor: pointer; font-weight: bold" onclick="location.href='index.php?dispatch=messages.view&msg_id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['message_id'], ENT_QUOTES, 'ISO-8859-1');?>
';">
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['company'], ENT_QUOTES, 'ISO-8859-1');?>
</td>
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['message'], ENT_QUOTES, 'ISO-8859-1');?>
</td>
                        <td><?php echo htmlspecialchars(date('F d, Y H:i',$_smarty_tpl->tpl_vars['message']->value['timestamp']), ENT_QUOTES, 'ISO-8859-1');?>
</td>
                    </tr>
                <?php } else { ?>
                    <tr class="" style="cursor: pointer" onclick="location.href='index.php?dispatch=messages.view&msg_id=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['message_id'], ENT_QUOTES, 'ISO-8859-1');?>
';">
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['message_id'], ENT_QUOTES, 'ISO-8859-1');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['company'], ENT_QUOTES, 'ISO-8859-1');?>
</td>
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['message'], ENT_QUOTES, 'ISO-8859-1');?>
</td>
                        <td><?php echo htmlspecialchars(date('F d, Y H:i',$_smarty_tpl->tpl_vars['message']->value['timestamp']), ENT_QUOTES, 'ISO-8859-1');?>
</td>
                    </tr>
                <?php }?>
            <?php } ?>
        <?php } else { ?>
            <tr>
                <td colspan="3">Data pesan belum ada !</td>
            </tr>
        <?php }?>
        </tbody>
    </table>
</div>
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox_title", null, null); ob_start();
echo htmlspecialchars($_smarty_tpl->tpl_vars['_title']->value, ENT_QUOTES, 'ISO-8859-1');
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
}?><?php }} ?>
