<?php /* Smarty version Smarty-3.1.21, created on 2015-08-12 10:58:41
         compiled from "/home/gbadmin/public_html/production/design/backend/templates/views/shippings/components/services/aup.tpl" */ ?>
<?php /*%%SmartyHeaderCode:106684358055cac47170bdb9-79300564%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3ff7d3026ea22fde3ddd2ddfe2dd13f092a6461a' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/backend/templates/views/shippings/components/services/aup.tpl',
      1 => 1438219655,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '106684358055cac47170bdb9-79300564',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'shipping' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55cac47176f9f0_40512421',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55cac47176f9f0_40512421')) {function content_55cac47176f9f0_40512421($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('max_box_weight','ship_width','ship_height','ship_length','ship_aup_use_delivery_confirmation','ship_aup_delivery_confirmation_cost','ship_aup_delivery_confirmation_international_cost','ship_aup_rpi_fee'));
?>
<fieldset>

<div class="control-group">
    <label class="control-label" for="max_weight"><?php echo $_smarty_tpl->__("max_box_weight");?>
</label>
    <div class="controls">
    <input id="max_weight" type="text" name="shipping_data[service_params][max_weight_of_box]" size="30" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['shipping']->value['service_params']['max_weight_of_box'])===null||$tmp==='' ? 0 : $tmp), ENT_QUOTES, 'ISO-8859-1');?>
" />
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="ship_width"><?php echo $_smarty_tpl->__("ship_width");?>
</label>
    <div class="controls">
    <input id="ship_width" type="text" name="shipping_data[service_params][width]" size="30" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipping']->value['service_params']['width'], ENT_QUOTES, 'ISO-8859-1');?>
" />
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="ship_height"><?php echo $_smarty_tpl->__("ship_height");?>
</label>
    <div class="controls">
    <input id="ship_height" type="text" name="shipping_data[service_params][height]" size="30" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipping']->value['service_params']['height'], ENT_QUOTES, 'ISO-8859-1');?>
" />
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="ship_length"><?php echo $_smarty_tpl->__("ship_length");?>
</label>
    <div class="controls">
    <input id="ship_length" type="text" name="shipping_data[service_params][length]" size="30" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipping']->value['service_params']['length'], ENT_QUOTES, 'ISO-8859-1');?>
" />
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="ship_aup_use_delivery_confirmation"><?php echo $_smarty_tpl->__("ship_aup_use_delivery_confirmation");?>
</label>
    <div class="controls">
    <input type="hidden" name="shipping_data[service_params][use_delivery_confirmation]" value="N" />
    <input id="ship_aup_use_delivery_confirmation" type="checkbox" name="shipping_data[service_params][use_delivery_confirmation]" value="Y" <?php if ($_smarty_tpl->tpl_vars['shipping']->value['service_params']['use_delivery_confirmation']=="Y") {?>checked="checked"<?php }?> /></div>
</div>

<div class="control-group">
    <label class="control-label" for="ship_aup_delivery_confirmation_cost"><?php echo $_smarty_tpl->__("ship_aup_delivery_confirmation_cost");?>
</label>
    <div class="controls">
    <input id="ship_aup_delivery_confirmation_cost" type="text" name="shipping_data[service_params][delivery_confirmation_cost]" size="30" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipping']->value['service_params']['delivery_confirmation_cost'], ENT_QUOTES, 'ISO-8859-1');?>
"/>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="ship_aup_delivery_confirmation_international_cost"><?php echo $_smarty_tpl->__("ship_aup_delivery_confirmation_international_cost");?>
</label>
    <div class="controls">
    <input id="ship_aup_delivery_confirmation_international_cost" type="text" name="shipping_data[service_params][delivery_confirmation_international_cost]" size="30" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipping']->value['service_params']['delivery_confirmation_international_cost'], ENT_QUOTES, 'ISO-8859-1');?>
"/>
    </div>
</div>

<div class="control-group">
    <label class="control-label" for="ship_aup_rpi_fee"><?php echo $_smarty_tpl->__("ship_aup_rpi_fee");?>
</label>
    <div class="controls">
    <input id="ship_aup_rpi_fee" type="text" name="shipping_data[service_params][rpi_fee]" size="30" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shipping']->value['service_params']['rpi_fee'], ENT_QUOTES, 'ISO-8859-1');?>
"/>
    </div>
</div>

</fieldset><?php }} ?>
