<?php /* Smarty version Smarty-3.1.21, created on 2015-09-14 15:10:04
         compiled from "/home/gbadmin/public_html/production/design/backend/templates/views/upgrade_center/components/collisions.tpl" */ ?>
<?php /*%%SmartyHeaderCode:27456809255f680dc754bd2-21151479%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3ec7c3dc5faf8b10a2c3d77f177683f62a013b21' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/backend/templates/views/upgrade_center/components/collisions.tpl',
      1 => 1438219660,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '27456809255f680dc754bd2-21151479',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'data' => 0,
    'files' => 0,
    'file_path' => 0,
    'status' => 0,
    'id' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55f680dc7dbdd7_04746470',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55f680dc7dbdd7_04746470')) {function content_55f680dc7dbdd7_04746470($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('text_uc_local_modification','text_uc_changed_files','files','action','text_uc_will_be_changed','text_uc_will_be_deleted','text_uc_agreed_collisions','confirm'));
?>
<table class="table table-condensed">
    <thead>
        <tr>
            <!-- upgrade-center_warning or upgrade-center_error -->
            <th class="upgrade-center_table-title upgrade-center_error">
                <h4><?php echo $_smarty_tpl->__("text_uc_local_modification");?>
</h4>
                <p><?php echo $_smarty_tpl->__("text_uc_changed_files");?>
</p>
            </th>
        </tr>
    </thead>
</table>

<div class="upgrade-center_collisions">
    <table class="table table-condensed">
        <thead>
            <tr>
                <th><?php echo $_smarty_tpl->__("files");?>
</th>
                <th class="right"><?php echo $_smarty_tpl->__("action");?>
</th>
            </tr>
        </thead>
        <tbody>
        <?php  $_smarty_tpl->tpl_vars['files'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['files']->_loop = false;
 $_smarty_tpl->tpl_vars['status'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['data']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['files']->key => $_smarty_tpl->tpl_vars['files']->value) {
$_smarty_tpl->tpl_vars['files']->_loop = true;
 $_smarty_tpl->tpl_vars['status']->value = $_smarty_tpl->tpl_vars['files']->key;
?>
            <?php  $_smarty_tpl->tpl_vars['file_path'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['file_path']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['files']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['file_path']->key => $_smarty_tpl->tpl_vars['file_path']->value) {
$_smarty_tpl->tpl_vars['file_path']->_loop = true;
?>
                <tr>
                    <td>
                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['file_path']->value, ENT_QUOTES, 'ISO-8859-1');?>

                    </td>
                    <td width="10%" class="right">
                        <?php if ($_smarty_tpl->tpl_vars['status']->value=="changed"||$_smarty_tpl->tpl_vars['status']->value=="new") {?>
                            <span class="label label-warning"><?php echo $_smarty_tpl->__("text_uc_will_be_changed");?>
</span>
                        <?php } elseif ($_smarty_tpl->tpl_vars['status']->value=="deleted") {?>
                            <span class="label label-important"><?php echo $_smarty_tpl->__("text_uc_will_be_deleted");?>
</span>
                        <?php }?>
                        
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
</div>

<strong><?php echo $_smarty_tpl->__("text_uc_agreed_collisions");?>
</strong>

<div class="control-group">
    <label class="control-label cm-required" for="skip_collisions_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'ISO-8859-1');?>
"><?php echo $_smarty_tpl->__("confirm");?>
</label>
    <div class="controls">
        <input type="checkbox" id="skip_collisions_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['id']->value, ENT_QUOTES, 'ISO-8859-1');?>
" name="skip_collisions" value="Y">
    </div>
</div><?php }} ?>
