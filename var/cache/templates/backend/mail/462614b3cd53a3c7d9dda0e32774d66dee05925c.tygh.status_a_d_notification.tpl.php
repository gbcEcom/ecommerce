<?php /* Smarty version Smarty-3.1.21, created on 2015-09-04 15:06:57
         compiled from "/home/gbadmin/public_html/production/design/backend/mail/templates/companies/status_a_d_notification.tpl" */ ?>
<?php /*%%SmartyHeaderCode:57502188255e951219015b5-17802779%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '462614b3cd53a3c7d9dda0e32774d66dee05925c' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/backend/mail/templates/companies/status_a_d_notification.tpl',
      1 => 1438219665,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '57502188255e951219015b5-17802779',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'company_data' => 0,
    'status' => 0,
    'reason' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55e9512192db15_76030704',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55e9512192db15_76030704')) {function content_55e9512192db15_76030704($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('hello','text_company_status_changed','reason'));
?>
<?php echo $_smarty_tpl->getSubTemplate ("common/letter_header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<?php echo $_smarty_tpl->__("hello");?>
,<br /><br />

<?php echo $_smarty_tpl->__("text_company_status_changed",array("[company]"=>$_smarty_tpl->tpl_vars['company_data']->value['company_name'],"[status]"=>$_smarty_tpl->tpl_vars['status']->value));?>


<br /><br />

<?php if ($_smarty_tpl->tpl_vars['reason']->value) {?>
<?php echo $_smarty_tpl->__("reason");?>
: <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['reason']->value, ENT_QUOTES, 'ISO-8859-1');?>

<br /><br />
<?php }?>

<?php echo $_smarty_tpl->getSubTemplate ("common/letter_footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
