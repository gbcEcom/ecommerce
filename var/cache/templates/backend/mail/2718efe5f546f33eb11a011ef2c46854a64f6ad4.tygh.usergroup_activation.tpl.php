<?php /* Smarty version Smarty-3.1.21, created on 2015-10-20 10:50:46
         compiled from "/home/gbadmin/public_html/production/design/backend/mail/templates/profiles/usergroup_activation.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5823696075625ba16322770-32006248%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2718efe5f546f33eb11a011ef2c46854a64f6ad4' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/backend/mail/templates/profiles/usergroup_activation.tpl',
      1 => 1438219665,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '5823696075625ba16322770-32006248',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'usergroup_ids' => 0,
    'u_id' => 0,
    'usergroups' => 0,
    'user_data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5625ba16361270_67080101',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5625ba16361270_67080101')) {function content_5625ba16361270_67080101($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('text_usergroup_activated','usergroups','usergroup','username','person_name'));
?>
<?php echo $_smarty_tpl->getSubTemplate ("common/letter_header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<?php echo $_smarty_tpl->__("text_usergroup_activated");?>
<br>
<p>
<table>
<?php if ($_smarty_tpl->tpl_vars['usergroup_ids']->value) {?>
<tr>
    <td><?php if (sizeof($_smarty_tpl->tpl_vars['usergroup_ids']->value)>1) {
echo $_smarty_tpl->__("usergroups");
} else {
echo $_smarty_tpl->__("usergroup");
}?>:</td>
    <td>
        <?php  $_smarty_tpl->tpl_vars['u_id'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['u_id']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['usergroup_ids']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['u_id']->total= $_smarty_tpl->_count($_from);
 $_smarty_tpl->tpl_vars['u_id']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['u_id']->key => $_smarty_tpl->tpl_vars['u_id']->value) {
$_smarty_tpl->tpl_vars['u_id']->_loop = true;
 $_smarty_tpl->tpl_vars['u_id']->iteration++;
 $_smarty_tpl->tpl_vars['u_id']->last = $_smarty_tpl->tpl_vars['u_id']->iteration === $_smarty_tpl->tpl_vars['u_id']->total;
 $_smarty_tpl->tpl_vars['smarty']->value['foreach']['ugroups']['last'] = $_smarty_tpl->tpl_vars['u_id']->last;
?>
            <b><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['usergroups']->value[$_smarty_tpl->tpl_vars['u_id']->value]['usergroup'], ENT_QUOTES, 'ISO-8859-1');?>
</b><?php if (!$_smarty_tpl->getVariable('smarty')->value['foreach']['ugroups']['last']) {?>, <?php }?>
        <?php } ?>
    </td>
</tr>
<?php }?>
<tr>
    <td><?php echo $_smarty_tpl->__("username");?>
:</td>
    <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_data']->value['email'], ENT_QUOTES, 'ISO-8859-1');?>
</td>
</tr>
<tr>
    <td><?php echo $_smarty_tpl->__("person_name");?>
:</td>
    <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_data']->value['firstname'], ENT_QUOTES, 'ISO-8859-1');?>
&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_data']->value['lastname'], ENT_QUOTES, 'ISO-8859-1');?>
</td>
</tr>
</table>
</p>
<?php echo $_smarty_tpl->getSubTemplate ("common/letter_footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
