<?php /* Smarty version Smarty-3.1.21, created on 2015-08-10 23:08:33
         compiled from "/home/gbadmin/public_html/production/design/backend/mail/templates/addons/discussion/notification.tpl" */ ?>
<?php /*%%SmartyHeaderCode:99350435955c8cc81af9580-35513771%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '20148e4dd35ab28d7bc4b3bd08a9599d6d9f8e43' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/backend/mail/templates/addons/discussion/notification.tpl',
      1 => 1438219665,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '99350435955c8cc81af9580-35513771',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'runtime' => 0,
    'object_name' => 0,
    'object_data' => 0,
    'post_data' => 0,
    'url' => 0,
    'auth' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55c8cc81c3b446_09530337',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55c8cc81c3b446_09530337')) {function content_55c8cc81c3b446_09530337($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_replace')) include '/home/gbadmin/public_html/production/app/lib/vendor/smarty/smarty/libs/plugins/modifier.replace.php';
if (!is_callable('smarty_function_set_id')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/function.set_id.php';
?><?php
fn_preload_lang_vars(array('hello','text_new_post_notification','person_name','rating','excellent','very_good','average','fair','poor','message','text_approval_notice','view','hello','text_new_post_notification','person_name','rating','excellent','very_good','average','fair','poor','message','text_approval_notice','view'));
?>
<?php if ($_smarty_tpl->tpl_vars['runtime']->value['customization_mode']['design']=="Y"&&@constant('AREA')=="C") {
$_smarty_tpl->_capture_stack[0][] = array("template_content", null, null); ob_start();
echo $_smarty_tpl->getSubTemplate ("common/letter_header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<?php echo $_smarty_tpl->__("hello");?>
,<br /><br />

<?php echo $_smarty_tpl->__("text_new_post_notification");?>
&nbsp;<b><?php echo $_smarty_tpl->__($_smarty_tpl->tpl_vars['object_name']->value);?>
</b>:&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['object_data']->value['description'], ENT_QUOTES, 'ISO-8859-1');?>

<br /><br />
<b><?php echo $_smarty_tpl->__("person_name");?>
</b>:&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post_data']->value['name'], ENT_QUOTES, 'ISO-8859-1');?>
<br />
<?php if ($_smarty_tpl->tpl_vars['post_data']->value['rating_value']) {?>
<b><?php echo $_smarty_tpl->__("rating");?>
</b>:&nbsp;<?php if ($_smarty_tpl->tpl_vars['post_data']->value['rating_value']=="5") {
echo $_smarty_tpl->__("excellent");
} elseif ($_smarty_tpl->tpl_vars['post_data']->value['rating_value']=="4") {
echo $_smarty_tpl->__("very_good");
} elseif ($_smarty_tpl->tpl_vars['post_data']->value['rating_value']=="3") {
echo $_smarty_tpl->__("average");
} elseif ($_smarty_tpl->tpl_vars['post_data']->value['rating_value']=="2") {
echo $_smarty_tpl->__("fair");
} elseif ($_smarty_tpl->tpl_vars['post_data']->value['rating_value']=="1") {
echo $_smarty_tpl->__("poor");
}?>
<br />
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['post_data']->value['message']) {?>
<b><?php echo $_smarty_tpl->__("message");?>
</b>:<br />
<?php echo nl2br($_smarty_tpl->tpl_vars['post_data']->value['message']);?>

<br /><br />
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['post_data']->value['status']=='N') {?>
<b><?php echo $_smarty_tpl->__("text_approval_notice");?>
</b>
<br />
<?php }?>
<?php echo $_smarty_tpl->__("view");?>
:<br />
<a href="<?php echo htmlspecialchars(smarty_modifier_replace($_smarty_tpl->tpl_vars['url']->value,'&amp;','&'), ENT_QUOTES, 'ISO-8859-1');?>
"><?php echo htmlspecialchars(smarty_modifier_replace($_smarty_tpl->tpl_vars['url']->value,'&amp;','&'), ENT_QUOTES, 'ISO-8859-1');?>
</a>

<?php echo $_smarty_tpl->getSubTemplate ("common/letter_footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);
list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();
if (trim(Smarty::$_smarty_vars['capture']['template_content'])) {
if ($_smarty_tpl->tpl_vars['auth']->value['area']=="A") {?><span class="cm-template-box template-box" data-ca-te-template="addons/discussion/notification.tpl" id="<?php echo smarty_function_set_id(array('name'=>"addons/discussion/notification.tpl"),$_smarty_tpl);?>
"><div class="cm-template-icon icon-edit ty-icon-edit hidden"></div><?php echo Smarty::$_smarty_vars['capture']['template_content'];?>
<!--[/tpl_id]--></span><?php } else {
echo Smarty::$_smarty_vars['capture']['template_content'];
}
}
} else {
echo $_smarty_tpl->getSubTemplate ("common/letter_header.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<?php echo $_smarty_tpl->__("hello");?>
,<br /><br />

<?php echo $_smarty_tpl->__("text_new_post_notification");?>
&nbsp;<b><?php echo $_smarty_tpl->__($_smarty_tpl->tpl_vars['object_name']->value);?>
</b>:&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['object_data']->value['description'], ENT_QUOTES, 'ISO-8859-1');?>

<br /><br />
<b><?php echo $_smarty_tpl->__("person_name");?>
</b>:&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['post_data']->value['name'], ENT_QUOTES, 'ISO-8859-1');?>
<br />
<?php if ($_smarty_tpl->tpl_vars['post_data']->value['rating_value']) {?>
<b><?php echo $_smarty_tpl->__("rating");?>
</b>:&nbsp;<?php if ($_smarty_tpl->tpl_vars['post_data']->value['rating_value']=="5") {
echo $_smarty_tpl->__("excellent");
} elseif ($_smarty_tpl->tpl_vars['post_data']->value['rating_value']=="4") {
echo $_smarty_tpl->__("very_good");
} elseif ($_smarty_tpl->tpl_vars['post_data']->value['rating_value']=="3") {
echo $_smarty_tpl->__("average");
} elseif ($_smarty_tpl->tpl_vars['post_data']->value['rating_value']=="2") {
echo $_smarty_tpl->__("fair");
} elseif ($_smarty_tpl->tpl_vars['post_data']->value['rating_value']=="1") {
echo $_smarty_tpl->__("poor");
}?>
<br />
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['post_data']->value['message']) {?>
<b><?php echo $_smarty_tpl->__("message");?>
</b>:<br />
<?php echo nl2br($_smarty_tpl->tpl_vars['post_data']->value['message']);?>

<br /><br />
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['post_data']->value['status']=='N') {?>
<b><?php echo $_smarty_tpl->__("text_approval_notice");?>
</b>
<br />
<?php }?>
<?php echo $_smarty_tpl->__("view");?>
:<br />
<a href="<?php echo htmlspecialchars(smarty_modifier_replace($_smarty_tpl->tpl_vars['url']->value,'&amp;','&'), ENT_QUOTES, 'ISO-8859-1');?>
"><?php echo htmlspecialchars(smarty_modifier_replace($_smarty_tpl->tpl_vars['url']->value,'&amp;','&'), ENT_QUOTES, 'ISO-8859-1');?>
</a>

<?php echo $_smarty_tpl->getSubTemplate ("common/letter_footer.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);
}?><?php }} ?>
