<?php /* Smarty version Smarty-3.1.21, created on 2015-08-12 10:23:44
         compiled from "/home/gbadmin/public_html/production/design/backend/templates/views/messages/view.tpl" */ ?>
<?php /*%%SmartyHeaderCode:185565169855cabc409d8132-75873017%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2cff5755f46f6d1f8933a8db9ebeb72c040234f3' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/backend/templates/views/messages/view.tpl',
      1 => 1438219662,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '185565169855cabc409d8132-75873017',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    '_REQUEST' => 0,
    'message' => 0,
    'conversation_with' => 0,
    'page_title' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55cabc40ab3ae5_91426610',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55cabc40ab3ae5_91426610')) {function content_55cabc40ab3ae5_91426610($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('messages'));
?>
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>

    <?php $_smarty_tpl->tpl_vars["page_title"] = new Smarty_variable($_smarty_tpl->__("messages"), null, 0);?>

    <?php $_smarty_tpl->_capture_stack[0][] = array("sidebar", null, null); ob_start(); ?>
        <?php echo $_smarty_tpl->getSubTemplate ("views/messages/components/sidebar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

    <?php if (empty($_smarty_tpl->tpl_vars['_REQUEST']->value['mode'])||is_null($_smarty_tpl->tpl_vars['_REQUEST']->value['mode'])) {?>
        <?php $_smarty_tpl->tpl_vars["conversation_with"] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['message']->value['firstname'])." ".((string)$_smarty_tpl->tpl_vars['message']->value['lastname']), null, 0);?>
        <?php $_smarty_tpl->tpl_vars["recipient_type"] = new Smarty_variable("V", null, 0);?>
    <?php } else { ?>
        <?php $_smarty_tpl->tpl_vars["conversation_with"] = new Smarty_variable(((string)$_smarty_tpl->tpl_vars['message']->value['firstname'])." ".((string)$_smarty_tpl->tpl_vars['message']->value['lastname']), null, 0);?>
        <?php $_smarty_tpl->tpl_vars["recipient_type"] = new Smarty_variable("C", null, 0);?>
    <?php }?>

    <div class="message-box" style="background-color: #f9f9f9; padding: 15px; border: 1px solid #d9d9d9">
        <div class="message-header" style="border-bottom: 1px solid #eee">
            <div class="row-fluid">
                <div class="span6">
                    <h6 style="margin: 0px">Percakapan dengan <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['conversation_with']->value, ENT_QUOTES, 'ISO-8859-1');?>
</h6>
                </div>
                <div class="span6">
                    <small class="pull-right"><?php echo htmlspecialchars(date('d F Y, H:i',$_smarty_tpl->tpl_vars['message']->value['timestamp']), ENT_QUOTES, 'ISO-8859-1');?>
</small>
                </div>
            </div>
        </div>
        <div class="message-body" style="padding-top: 15px;">
            <div class="message-content">
                <?php if (is_null($_smarty_tpl->tpl_vars['message']->value['parent_id'])||$_smarty_tpl->tpl_vars['message']->value['parent_id']!=0) {?>
                    <small style="font-style: italic"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['conversation_with']->value, ENT_QUOTES, 'ISO-8859-1');?>
 Say:</small>
                    <pre><?php echo htmlspecialchars(strip_tags($_smarty_tpl->tpl_vars['message']->value['parent_message']), ENT_QUOTES, 'ISO-8859-1');?>
</pre>
                <?php }?>
                <?php if (empty($_smarty_tpl->tpl_vars['_REQUEST']->value['mode'])) {?>
                    <small style="font-style: italic"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['conversation_with']->value, ENT_QUOTES, 'ISO-8859-1');?>
 Say:</small>
                <?php } else { ?>
                    <small style="font-style: italic"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['sender'], ENT_QUOTES, 'ISO-8859-1');?>
 Say:</small>
                <?php }?>
                <pre><?php echo htmlspecialchars(strip_tags($_smarty_tpl->tpl_vars['message']->value['message']), ENT_QUOTES, 'ISO-8859-1');?>
</pre>
            </div>
            <div class="reply-message">
                <form method="post" action="<?php echo htmlspecialchars(fn_url("messages.send"), ENT_QUOTES, 'ISO-8859-1');?>
" class="form">
                    <input type="hidden" name="parent_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['message_id'], ENT_QUOTES, 'ISO-8859-1');?>
" />
                    <input type="hidden" name="sender_id" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['recipient_id'], ENT_QUOTES, 'ISO-8859-1');?>
" />
                    <input type="hidden" name="recipient" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['from'], ENT_QUOTES, 'ISO-8859-1');?>
" />
                    <input type="hidden" name="recipient_type" value="C" />
                    <div class="control-group">
                        <textarea class="cm-wysiwyg input-large" name="message" rows="5" placeholder="Ketik pesan..." required="required" autofocus="autofocus"></textarea>
                    </div>
                    <div class="controls">
                        <button class="btn btn-default" type="button" onclick="self.history.back(-1)">Kembali</button>
                        <button class="btn btn-primary pull-right" type="submit">Kirim</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php echo $_smarty_tpl->getSubTemplate ("common/mainbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->tpl_vars['page_title']->value,'sidebar'=>Smarty::$_smarty_vars['capture']['sidebar'],'content'=>Smarty::$_smarty_vars['capture']['mainbox'],'buttons'=>Smarty::$_smarty_vars['capture']['buttons'],'adv_buttons'=>Smarty::$_smarty_vars['capture']['adv_buttons'],'content_id'=>"messsages_view"), 0);?>
<?php }} ?>
