<?php /* Smarty version Smarty-3.1.21, created on 2015-08-13 06:38:23
         compiled from "/home/gbadmin/public_html/production/design/backend/templates/views/products/components/products_m_update_feature.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3139266855cbd8ef90c047-61969577%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e66edbf6894895a3c1d0981a2ecdf6c3c663c151' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/backend/templates/views/products/components/products_m_update_feature.tpl',
      1 => 1438219661,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '3139266855cbd8ef90c047-61969577',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'feature' => 0,
    'data_name' => 0,
    'over' => 0,
    'field' => 0,
    'suffix' => 0,
    'input_id' => 0,
    'selected' => 0,
    'selected_variant' => 0,
    'var' => 0,
    'pid' => 0,
    'date_id' => 0,
    'settings' => 0,
    'date_extra' => 0,
    'd_meta' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55cbd8efb30802_58901170',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55cbd8efb30802_58901170')) {function content_55cbd8efb30802_58901170($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_enum')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/modifier.enum.php';
?><?php
fn_preload_lang_vars(array('none','none'));
?>
<?php if ($_smarty_tpl->tpl_vars['feature']->value['prefix']) {?><span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['prefix'], ENT_QUOTES, 'ISO-8859-1');?>
</span><?php }?>
<?php if ($_smarty_tpl->tpl_vars['feature']->value['feature_type']==smarty_modifier_enum("ProductFeatures::TEXT_SELECTBOX")||$_smarty_tpl->tpl_vars['feature']->value['feature_type']==smarty_modifier_enum("ProductFeatures::NUMBER_SELECTBOX")||$_smarty_tpl->tpl_vars['feature']->value['feature_type']==smarty_modifier_enum("ProductFeatures::EXTENDED")) {?>
    <?php if ($_smarty_tpl->tpl_vars['feature']->value['use_variant_picker']) {?>
        <?php $_smarty_tpl->tpl_vars["suffix"] = new Smarty_variable(md5($_smarty_tpl->tpl_vars['data_name']->value), null, 0);?>
        
        <?php if ($_smarty_tpl->tpl_vars['over']->value) {?>
            <?php $_smarty_tpl->tpl_vars["input_id"] = new Smarty_variable("field_".((string)$_smarty_tpl->tpl_vars['field']->value)."__".((string)$_smarty_tpl->tpl_vars['feature']->value['feature_id'])."_", null, 0);?>
        <?php } else { ?>
            <?php $_smarty_tpl->tpl_vars["input_id"] = new Smarty_variable("feature_".((string)$_smarty_tpl->tpl_vars['feature']->value['feature_id'])."_".((string)$_smarty_tpl->tpl_vars['suffix']->value), null, 0);?>
        <?php }?>    
        
        <input type="hidden" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data_name']->value, ENT_QUOTES, 'ISO-8859-1');?>
[product_features][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['feature_id'], ENT_QUOTES, 'ISO-8859-1');?>
]" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['input_id']->value, ENT_QUOTES, 'ISO-8859-1');?>
" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['selected']->value)===null||$tmp==='' ? $_smarty_tpl->tpl_vars['feature']->value['variant_id'] : $tmp), ENT_QUOTES, 'ISO-8859-1');?>
" <?php if ($_smarty_tpl->tpl_vars['over']->value) {?> disabled="disabled"<?php }?> />
        <?php if ($_smarty_tpl->tpl_vars['feature']->value['variants'][$_smarty_tpl->tpl_vars['feature']->value['variant_id']]['variant']) {?>
            <?php $_smarty_tpl->tpl_vars["selected_variant"] = new Smarty_variable($_smarty_tpl->tpl_vars['feature']->value['variants'][$_smarty_tpl->tpl_vars['feature']->value['variant_id']]['variant'], null, 0);?>
        <?php } elseif ($_smarty_tpl->tpl_vars['feature']->value['variant_id']) {?>
            <?php $_smarty_tpl->tpl_vars["selected_variant"] = new Smarty_variable(fn_get_product_feature_variant($_smarty_tpl->tpl_vars['feature']->value['variant_id']), null, 0);?>
            <?php $_smarty_tpl->tpl_vars["selected_variant"] = new Smarty_variable($_smarty_tpl->tpl_vars['selected_variant']->value['variant'], null, 0);?>
        <?php } else { ?>
            <?php $_smarty_tpl->tpl_vars["selected_variant"] = new Smarty_variable($_smarty_tpl->__("none"), null, 0);?>
        <?php }?>
        <?php echo $_smarty_tpl->getSubTemplate ("common/ajax_select_object.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('data_url'=>"product_features.get_feature_variants_list?feature_id=".((string)$_smarty_tpl->tpl_vars['feature']->value['feature_id'])."&enter_other=N",'text'=>$_smarty_tpl->tpl_vars['selected_variant']->value,'result_elm'=>$_smarty_tpl->tpl_vars['input_id']->value,'id'=>((string)$_smarty_tpl->tpl_vars['feature']->value['feature_id'])."_selector_".((string)$_smarty_tpl->tpl_vars['suffix']->value)), 0);?>

    <?php } else { ?>
        <select name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data_name']->value, ENT_QUOTES, 'ISO-8859-1');?>
[product_features][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['feature_id'], ENT_QUOTES, 'ISO-8859-1');?>
]" <?php if ($_smarty_tpl->tpl_vars['over']->value) {?>id="field_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value, ENT_QUOTES, 'ISO-8859-1');?>
__<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['feature_id'], ENT_QUOTES, 'ISO-8859-1');?>
_" disabled="disabled" class="elm-disabled"<?php }?>>
            <option value="">-<?php echo $_smarty_tpl->__("none");?>
-</option>
            <?php  $_smarty_tpl->tpl_vars["var"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["var"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['feature']->value['variants']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["var"]->key => $_smarty_tpl->tpl_vars["var"]->value) {
$_smarty_tpl->tpl_vars["var"]->_loop = true;
?>
            <option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['var']->value['variant_id'], ENT_QUOTES, 'ISO-8859-1');?>
" <?php if ($_smarty_tpl->tpl_vars['var']->value['variant_id']==$_smarty_tpl->tpl_vars['feature']->value['variant_id']) {?>selected="selected"<?php }?>><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['var']->value['variant'], ENT_QUOTES, 'ISO-8859-1');?>
</option>
            <?php } ?>
        </select>
    <?php }?>
<?php } elseif ($_smarty_tpl->tpl_vars['feature']->value['feature_type']==smarty_modifier_enum("ProductFeatures::MULTIPLE_CHECKBOX")) {?>
        <input type="hidden" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data_name']->value, ENT_QUOTES, 'ISO-8859-1');?>
[product_features][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['feature_id'], ENT_QUOTES, 'ISO-8859-1');?>
]" value="" <?php if ($_smarty_tpl->tpl_vars['over']->value) {?>id="field_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value, ENT_QUOTES, 'ISO-8859-1');?>
__<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['feature_id'], ENT_QUOTES, 'ISO-8859-1');?>
_" disabled="disabled"<?php }?> />
        <?php  $_smarty_tpl->tpl_vars["var"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["var"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['feature']->value['variants']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["var"]->key => $_smarty_tpl->tpl_vars["var"]->value) {
$_smarty_tpl->tpl_vars["var"]->_loop = true;
?>
            <div class="select-field">
                <input type="checkbox" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data_name']->value, ENT_QUOTES, 'ISO-8859-1');?>
[product_features][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['feature_id'], ENT_QUOTES, 'ISO-8859-1');?>
][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['var']->value['variant_id'], ENT_QUOTES, 'ISO-8859-1');?>
]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['var']->value['variant_id'], ENT_QUOTES, 'ISO-8859-1');?>
" class="checkbox<?php if ($_smarty_tpl->tpl_vars['over']->value) {?> elm-disabled<?php }?>" id="field_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value, ENT_QUOTES, 'ISO-8859-1');?>
__<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['feature_id'], ENT_QUOTES, 'ISO-8859-1');?>
_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['var']->value['variant_id'], ENT_QUOTES, 'ISO-8859-1');?>
_<?php echo htmlspecialchars(md5($_smarty_tpl->tpl_vars['data_name']->value), ENT_QUOTES, 'ISO-8859-1');?>
" <?php if ($_smarty_tpl->tpl_vars['over']->value) {?> disabled="disabled"<?php }?> <?php if ($_smarty_tpl->tpl_vars['var']->value['selected']) {?>checked="checked"<?php }?> />
                <label for="field_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value, ENT_QUOTES, 'ISO-8859-1');?>
__<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['feature_id'], ENT_QUOTES, 'ISO-8859-1');?>
_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['var']->value['variant_id'], ENT_QUOTES, 'ISO-8859-1');?>
_<?php echo htmlspecialchars(md5($_smarty_tpl->tpl_vars['data_name']->value), ENT_QUOTES, 'ISO-8859-1');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['var']->value['variant'], ENT_QUOTES, 'ISO-8859-1');?>
</label>
            </div>
        <?php } ?>
<?php } elseif ($_smarty_tpl->tpl_vars['feature']->value['feature_type']==smarty_modifier_enum("ProductFeatures::SINGLE_CHECKBOX")) {?>
    <input type="hidden" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data_name']->value, ENT_QUOTES, 'ISO-8859-1');?>
[product_features][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['feature_id'], ENT_QUOTES, 'ISO-8859-1');?>
]" value="N" <?php if ($_smarty_tpl->tpl_vars['over']->value) {?>disabled="disabled" id="field_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value, ENT_QUOTES, 'ISO-8859-1');?>
__<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['feature_id'], ENT_QUOTES, 'ISO-8859-1');?>
_copy"<?php }?> />
    <input type="checkbox" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data_name']->value, ENT_QUOTES, 'ISO-8859-1');?>
[product_features][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['feature_id'], ENT_QUOTES, 'ISO-8859-1');?>
]" value="Y" <?php if ($_smarty_tpl->tpl_vars['over']->value) {?>id="field_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value, ENT_QUOTES, 'ISO-8859-1');?>
__<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['feature_id'], ENT_QUOTES, 'ISO-8859-1');?>
_" disabled="disabled" class="elm-disabled"<?php }?> <?php if ($_smarty_tpl->tpl_vars['feature']->value['value']=="Y") {?>checked="checked"<?php }?> />
<?php } elseif ($_smarty_tpl->tpl_vars['feature']->value['feature_type']==smarty_modifier_enum("ProductFeatures::DATE")) {?>
    <?php if ($_smarty_tpl->tpl_vars['over']->value) {?>
        <?php $_smarty_tpl->tpl_vars["date_id"] = new Smarty_variable("field_".((string)$_smarty_tpl->tpl_vars['field']->value)."__".((string)$_smarty_tpl->tpl_vars['feature']->value['feature_id'])."_", null, 0);?>
        <?php $_smarty_tpl->tpl_vars["date_extra"] = new Smarty_variable(" disabled=\"disabled\"", null, 0);?>
        <?php $_smarty_tpl->tpl_vars["d_meta"] = new Smarty_variable("input-text-disabled", null, 0);?>
    <?php } else { ?>
        <?php $_smarty_tpl->tpl_vars["date_id"] = new Smarty_variable("date_".((string)$_smarty_tpl->tpl_vars['pid']->value).((string)$_smarty_tpl->tpl_vars['feature']->value['feature_id']), null, 0);?>
        <?php $_smarty_tpl->tpl_vars["date_extra"] = new Smarty_variable('', null, 0);?>
        <?php $_smarty_tpl->tpl_vars["d_meta"] = new Smarty_variable('', null, 0);?>
    <?php }?>
    <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['value'], ENT_QUOTES, 'ISO-8859-1');
echo $_smarty_tpl->getSubTemplate ("common/calendar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('date_id'=>$_smarty_tpl->tpl_vars['date_id']->value,'date_name'=>((string)$_smarty_tpl->tpl_vars['data_name']->value)."[product_features][".((string)$_smarty_tpl->tpl_vars['feature']->value['feature_id'])."]",'date_val'=>$_smarty_tpl->tpl_vars['feature']->value['value_int'],'start_year'=>$_smarty_tpl->tpl_vars['settings']->value['Company']['company_start_year'],'extra'=>$_smarty_tpl->tpl_vars['date_extra']->value,'date_meta'=>$_smarty_tpl->tpl_vars['d_meta']->value), 0);?>

<?php } else { ?>
    <input type="text" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data_name']->value, ENT_QUOTES, 'ISO-8859-1');?>
[product_features][<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['feature_id'], ENT_QUOTES, 'ISO-8859-1');?>
]" value="<?php if ($_smarty_tpl->tpl_vars['feature']->value['feature_type']==smarty_modifier_enum("ProductFeatures::NUMBER_FIELD")) {
if ($_smarty_tpl->tpl_vars['feature']->value['value_int']!='') {
echo htmlspecialchars(floatval($_smarty_tpl->tpl_vars['feature']->value['value_int']), ENT_QUOTES, 'ISO-8859-1');
}
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['value'], ENT_QUOTES, 'ISO-8859-1');
}?>" <?php if ($_smarty_tpl->tpl_vars['over']->value) {?> id="field_<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value, ENT_QUOTES, 'ISO-8859-1');?>
__<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['feature_id'], ENT_QUOTES, 'ISO-8859-1');?>
_" disabled="disabled"<?php }?> class="input-text <?php if ($_smarty_tpl->tpl_vars['over']->value) {?>input-text-disabled<?php }?> <?php if ($_smarty_tpl->tpl_vars['feature']->value['feature_type']==smarty_modifier_enum("ProductFeatures::NUMBER_FIELD")) {?>cm-value-decimal<?php }?>" />
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['feature']->value['suffix']) {?><span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['suffix'], ENT_QUOTES, 'ISO-8859-1');?>
</span><?php }?>
<input type="hidden" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['data_name']->value, ENT_QUOTES, 'ISO-8859-1');?>
[active_features][]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['feature']->value['feature_id'], ENT_QUOTES, 'ISO-8859-1');?>
" /><?php }} ?>
