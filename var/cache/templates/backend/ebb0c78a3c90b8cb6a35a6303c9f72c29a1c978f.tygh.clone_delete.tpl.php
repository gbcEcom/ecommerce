<?php /* Smarty version Smarty-3.1.21, created on 2015-09-22 17:07:16
         compiled from "/home/gbadmin/public_html/production/design/backend/templates/buttons/clone_delete.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8677702935601285401ef28-69105410%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ebb0c78a3c90b8cb6a35a6303c9f72c29a1c978f' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/backend/templates/buttons/clone_delete.tpl',
      1 => 1438219654,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '8677702935601285401ef28-69105410',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'href_clone' => 0,
    'no_confirm' => 0,
    'microformats' => 0,
    'href_delete' => 0,
    'delete_target_id' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_560128540998f4_40424411',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_560128540998f4_40424411')) {function content_560128540998f4_40424411($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('remove','remove'));
?>
<?php if ($_smarty_tpl->tpl_vars['href_clone']->value) {?>
<a class="clone-item cm-tooltip" title="<?php echo $_smarty_tpl->__("remove");?>
" href="<?php echo htmlspecialchars(fn_url($_smarty_tpl->tpl_vars['href_clone']->value), ENT_QUOTES, 'ISO-8859-1');?>
"><i class="icon-remove"></i></a>
<?php }?>
<a class="delete-item cm-tooltip <?php if (!$_smarty_tpl->tpl_vars['no_confirm']->value) {?>cm-confirm<?php }
if ($_smarty_tpl->tpl_vars['microformats']->value) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['microformats']->value, ENT_QUOTES, 'ISO-8859-1');
}?>" title="<?php echo $_smarty_tpl->__("remove");?>
" <?php if ($_smarty_tpl->tpl_vars['href_delete']->value) {?>href="<?php echo htmlspecialchars(fn_url($_smarty_tpl->tpl_vars['href_delete']->value), ENT_QUOTES, 'ISO-8859-1');?>
"<?php }?> <?php if ($_smarty_tpl->tpl_vars['delete_target_id']->value) {?>data-ca-target-id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['delete_target_id']->value, ENT_QUOTES, 'ISO-8859-1');?>
"<?php }?>><i class="icon-remove"></i></a><?php }} ?>
