<?php /* Smarty version Smarty-3.1.21, created on 2015-08-13 06:38:23
         compiled from "/home/gbadmin/public_html/production/design/backend/templates/views/products/components/products_m_update_features.tpl" */ ?>
<?php /*%%SmartyHeaderCode:172368072055cbd8ef827b70-61479387%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7cbd5963c096524b56f79e74e91f0e6e76df75d2' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/backend/templates/views/products/components/products_m_update_features.tpl',
      1 => 1438219661,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '172368072055cbd8ef827b70-61479387',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'features_search' => 0,
    'product_id' => 0,
    'product_features' => 0,
    'pf' => 0,
    'over' => 0,
    'field' => 0,
    'subfeature' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55cbd8ef8fc859_40527092',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55cbd8ef8fc859_40527092')) {function content_55cbd8ef8fc859_40527092($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_enum')) include '/home/gbadmin/public_html/production/app/functions/smarty_plugins/modifier.enum.php';
?><?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('search'=>$_smarty_tpl->tpl_vars['features_search']->value,'div_id'=>"product_features_pagination_".((string)$_smarty_tpl->tpl_vars['product_id']->value),'current_url'=>fn_url("products.get_features?product_id=".((string)$_smarty_tpl->tpl_vars['product_id']->value)."&multiple=1&over=".((string)$_smarty_tpl->tpl_vars['over']->value)."&data_name=".((string)$_smarty_tpl->tpl_vars['data_name']->value)),'disable_history'=>true), 0);?>


<table >
<?php  $_smarty_tpl->tpl_vars["pf"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["pf"]->_loop = false;
 $_smarty_tpl->tpl_vars["feature_id"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['product_features']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["pf"]->key => $_smarty_tpl->tpl_vars["pf"]->value) {
$_smarty_tpl->tpl_vars["pf"]->_loop = true;
 $_smarty_tpl->tpl_vars["feature_id"]->value = $_smarty_tpl->tpl_vars["pf"]->key;
?>
<?php if ($_smarty_tpl->tpl_vars['pf']->value['feature_type']!=smarty_modifier_enum("ProductFeatures::GROUP")) {?>
<tr>

    <?php if ($_smarty_tpl->tpl_vars['over']->value==true) {?>
    <td><label class="checkbox" for="elements-switcher-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value, ENT_QUOTES, 'ISO-8859-1');?>
__<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pf']->value['feature_id'], ENT_QUOTES, 'ISO-8859-1');?>
_"><input type="checkbox" id="elements-switcher-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value, ENT_QUOTES, 'ISO-8859-1');?>
__<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pf']->value['feature_id'], ENT_QUOTES, 'ISO-8859-1');?>
_" />&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pf']->value['description'], ENT_QUOTES, 'ISO-8859-1');?>
:&nbsp;</label></td>
    <?php } else { ?>
    <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pf']->value['description'], ENT_QUOTES, 'ISO-8859-1');?>
:</td>
    <?php }?>

    <td >
        <?php echo $_smarty_tpl->getSubTemplate ("views/products/components/products_m_update_feature.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('feature'=>$_smarty_tpl->tpl_vars['pf']->value,'pid'=>$_smarty_tpl->tpl_vars['product_id']->value), 0);?>

    </td>
</tr>
<?php }?>
<?php } ?>
<?php  $_smarty_tpl->tpl_vars["pf"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["pf"]->_loop = false;
 $_smarty_tpl->tpl_vars["feature_id"] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['product_features']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["pf"]->key => $_smarty_tpl->tpl_vars["pf"]->value) {
$_smarty_tpl->tpl_vars["pf"]->_loop = true;
 $_smarty_tpl->tpl_vars["feature_id"]->value = $_smarty_tpl->tpl_vars["pf"]->key;
?>
<?php if ($_smarty_tpl->tpl_vars['pf']->value['feature_type']==smarty_modifier_enum("ProductFeatures::GROUP")&&$_smarty_tpl->tpl_vars['pf']->value['subfeatures']) {?>
<tr>
    <td colspan="2"><span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pf']->value['description'], ENT_QUOTES, 'ISO-8859-1');?>
</span></td>
</tr>
<?php  $_smarty_tpl->tpl_vars['subfeature'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['subfeature']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['pf']->value['subfeatures']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['subfeature']->key => $_smarty_tpl->tpl_vars['subfeature']->value) {
$_smarty_tpl->tpl_vars['subfeature']->_loop = true;
?>
<tr>

    <?php if ($_smarty_tpl->tpl_vars['over']->value==true) {?>
    <td class="nowrap"><label class="checkbox" for="elements-switcher-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value, ENT_QUOTES, 'ISO-8859-1');?>
__<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subfeature']->value['feature_id'], ENT_QUOTES, 'ISO-8859-1');?>
_"><input type="checkbox" id="elements-switcher-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['field']->value, ENT_QUOTES, 'ISO-8859-1');?>
__<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subfeature']->value['feature_id'], ENT_QUOTES, 'ISO-8859-1');?>
_"/>&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subfeature']->value['description'], ENT_QUOTES, 'ISO-8859-1');?>
</label></td>
    <?php } else { ?>
    <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subfeature']->value['description'], ENT_QUOTES, 'ISO-8859-1');?>
:</td>
    <?php }?>

    <td><?php echo $_smarty_tpl->getSubTemplate ("views/products/components/products_m_update_feature.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('feature'=>$_smarty_tpl->tpl_vars['subfeature']->value,'pid'=>$_smarty_tpl->tpl_vars['product_id']->value), 0);?>
</td>
</tr>
<?php } ?>
<?php }?>
<?php } ?>
</table>

<?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('search'=>$_smarty_tpl->tpl_vars['features_search']->value,'div_id'=>"product_features_pagination_".((string)$_smarty_tpl->tpl_vars['product_id']->value)), 0);?>

<?php }} ?>
