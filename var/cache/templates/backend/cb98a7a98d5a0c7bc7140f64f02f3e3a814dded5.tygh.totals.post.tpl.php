<?php /* Smarty version Smarty-3.1.21, created on 2015-08-19 10:03:50
         compiled from "/home/gbadmin/public_html/production/design/backend/templates/addons/reward_points/hooks/order_management/totals.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:205694488555d3f2168aba08-28197695%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cb98a7a98d5a0c7bc7140f64f02f3e3a814dded5' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/backend/templates/addons/reward_points/hooks/order_management/totals.post.tpl',
      1 => 1438219648,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '205694488555d3f2168aba08-28197695',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'cart' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55d3f2168e1d15_53305248',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55d3f2168e1d15_53305248')) {function content_55d3f2168e1d15_53305248($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('points','points_in_use','points','delete'));
?>
<?php if ($_smarty_tpl->tpl_vars['cart']->value['points_info']['reward']) {?>
    <tr>
        <td><?php echo $_smarty_tpl->__("points");?>
:</td>
        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart']->value['points_info']['reward'], ENT_QUOTES, 'ISO-8859-1');?>
</td>
    </tr>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['cart']->value['points_info']['in_use']) {?>
    <tr>
        <td class="nowrap"><?php echo $_smarty_tpl->__("points_in_use");?>
&nbsp;(<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart']->value['points_info']['in_use']['points'], ENT_QUOTES, 'ISO-8859-1');?>
&nbsp;<?php echo $_smarty_tpl->__("points");?>
)&nbsp;<a class="cm-post" href="<?php echo htmlspecialchars(fn_url("order_management.delete_points_in_use"), ENT_QUOTES, 'ISO-8859-1');?>
"><i class="icon-trash" title="<?php echo $_smarty_tpl->__("delete");?>
"></i></a>:</td>
        <td><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['cart']->value['points_info']['in_use']['cost']), 0);?>
</td>
    </tr>
<?php }?><?php }} ?>
