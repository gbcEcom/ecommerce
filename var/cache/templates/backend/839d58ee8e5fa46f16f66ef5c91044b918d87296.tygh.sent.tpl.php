<?php /* Smarty version Smarty-3.1.21, created on 2015-10-20 11:30:27
         compiled from "/home/gbadmin/public_html/production/design/backend/templates/views/messages/sent.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8169480785625c3632a9709-91614826%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '839d58ee8e5fa46f16f66ef5c91044b918d87296' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/backend/templates/views/messages/sent.tpl',
      1 => 1438219662,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '8169480785625c3632a9709-91614826',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'auth' => 0,
    'messages' => 0,
    'message' => 0,
    'page_title' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_5625c3633805b3_73931888',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5625c3633805b3_73931888')) {function content_5625c3633805b3_73931888($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('messages'));
?>
<?php $_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>

    <?php $_smarty_tpl->tpl_vars["page_title"] = new Smarty_variable($_smarty_tpl->__("messages"), null, 0);?>

    <?php $_smarty_tpl->_capture_stack[0][] = array("sidebar", null, null); ob_start(); ?>
        <?php echo $_smarty_tpl->getSubTemplate ("views/messages/components/sidebar.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

    <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <div class="message-box">
        <table class="table table-hover" style="font-size: 12px;">
            <thead>
            <tr style="background-color: #f9f9f9">
                <?php if ($_smarty_tpl->tpl_vars['auth']->value['user_type']=='A') {?>
                <th width="15%">From</th>
                <?php }?>
                <th width="15%">To</th>
                <th width="55%">Message</th>
                <th width="15%">Received</th>
            </tr>
            </thead>

            <tbody>
            <?php if (count($_smarty_tpl->tpl_vars['messages']->value)) {?>
                <?php  $_smarty_tpl->tpl_vars["message"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["message"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['messages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["message"]->key => $_smarty_tpl->tpl_vars["message"]->value) {
$_smarty_tpl->tpl_vars["message"]->_loop = true;
?>
                    <?php $_smarty_tpl->tpl_vars["msg_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['message']->value['message_id'], null, 0);?>
                    <tr class="" style="cursor: pointer" onclick="location.href='<?php echo htmlspecialchars(fn_url("messages.view&mode=outbox&msg_id=".((string)$_smarty_tpl->tpl_vars['msg_id']->value)), ENT_QUOTES, 'ISO-8859-1');?>
';">
                        <?php if ($_smarty_tpl->tpl_vars['auth']->value['user_type']=='A') {?>
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['sender'], ENT_QUOTES, 'ISO-8859-1');?>
</td>
                        <?php }?>
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['firstname'], ENT_QUOTES, 'ISO-8859-1');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['lastname'], ENT_QUOTES, 'ISO-8859-1');?>
</td>
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value['message'], ENT_QUOTES, 'ISO-8859-1');?>
</td>
                        <td><?php echo htmlspecialchars(date('F d, Y H:i',$_smarty_tpl->tpl_vars['message']->value['timestamp']), ENT_QUOTES, 'ISO-8859-1');?>
</td>
                    </tr>
                <?php } ?>
            <?php }?>
            </tbody>
        </table>
        <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    </div>

<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php echo $_smarty_tpl->getSubTemplate ("common/mainbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>$_smarty_tpl->tpl_vars['page_title']->value,'sidebar'=>Smarty::$_smarty_vars['capture']['sidebar'],'content'=>Smarty::$_smarty_vars['capture']['mainbox'],'buttons'=>Smarty::$_smarty_vars['capture']['buttons'],'adv_buttons'=>Smarty::$_smarty_vars['capture']['adv_buttons'],'content_id'=>"messsages_inbox"), 0);?>
<?php }} ?>
