<?php /* Smarty version Smarty-3.1.21, created on 2015-08-11 09:53:53
         compiled from "/home/gbadmin/public_html/production/design/backend/templates/addons/dropdown_image/hooks/categories/detailed_content.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:39428001455c963c1f402d7-10133167%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd7b40dc7c952d507aa1f7fa60f5d2956b9c559bb' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/backend/templates/addons/dropdown_image/hooks/categories/detailed_content.post.tpl',
      1 => 1438219651,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '39428001455c963c1f402d7-10133167',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'category_data' => 0,
    'et_dd_image' => 0,
    'array_name' => 0,
    'all_disabled' => 0,
    'current_image_width' => 0,
    'current_image_height' => 0,
    'menu_disabled' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55c963c2114bb4_95263849',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55c963c2114bb4_95263849')) {function content_55c963c2114bb4_95263849($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('et_dd_image','active','et_dd_image_width','et_dd_image_height','et_dd_url','et_dd_custom_menu_settings','et_dd_offset_right','et_dd_offset_bottom','et_dd_menu_width','et_dd_min_menu_height'));
?>
<?php echo $_smarty_tpl->getSubTemplate ("common/subheader.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>__("et_dd_image"),'target'=>"#et_dd_setting"), 0);?>

<?php $_smarty_tpl->tpl_vars['array_name'] = new Smarty_variable('category_data', null, 0);?>
<?php $_smarty_tpl->tpl_vars['all_disabled'] = new Smarty_variable('', null, 0);?>
<?php $_smarty_tpl->tpl_vars['menu_disabled'] = new Smarty_variable('', null, 0);?>

<?php if ($_smarty_tpl->tpl_vars['category_data']->value['main_pair']) {?>
    <?php $_smarty_tpl->tpl_vars["current_image_width"] = new Smarty_variable("(Current image width: ".((string)$_smarty_tpl->tpl_vars['category_data']->value['main_pair']['detailed']['image_x'])." px)", null, 0);?>
    <?php $_smarty_tpl->tpl_vars["current_image_height"] = new Smarty_variable("(Current image height: ".((string)$_smarty_tpl->tpl_vars['category_data']->value['main_pair']['detailed']['image_y'])." px)", null, 0);?>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['et_dd_image']->value['dd_status']!="Y") {?>
    <?php $_smarty_tpl->tpl_vars['all_disabled'] = new Smarty_variable("disabled", null, 0);?>
<?php } elseif ($_smarty_tpl->tpl_vars['et_dd_image']->value['custom_menu_settings']!="Y") {?>
    <?php $_smarty_tpl->tpl_vars['menu_disabled'] = new Smarty_variable("disabled", null, 0);?>
<?php }?>

<fieldset>
    <div id="et_dd_setting" class="in collapse">
        <input type="hidden" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['array_name']->value, ENT_QUOTES, 'ISO-8859-1');?>
[et_dd_id]" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['et_dd_image']->value['dropdown_image_id'], ENT_QUOTES, 'ISO-8859-1');?>
">

<!-- Activate settings -->
        <div class="control-group">
            <label class="control-label" for="et_dd_active">
                <?php echo $_smarty_tpl->__("active");?>
:
            </label>
            <div class="controls">
                <input type="hidden" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['array_name']->value, ENT_QUOTES, 'ISO-8859-1');?>
[et_dd_active]" value="N" />
                <input type="checkbox" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['array_name']->value, ENT_QUOTES, 'ISO-8859-1');?>
[et_dd_active]" id="et_dd_active" value="Y" <?php if ($_smarty_tpl->tpl_vars['et_dd_image']->value['dd_status']=="Y") {?>checked="checked"<?php }?> onclick="Tygh.$.disable_elms(['et_dd_image_width','et_dd_image_height','et_dd_url','et_dd_custom_menu_settings'], !this.checked); Tygh.$.disable_elms(['et_dd_offset_right','et_dd_offset_bottom','et_dd_menu_width','et_dd_menu_min_height'], ((!document.getElementById('et_dd_custom_menu_settings').checked && !document.getElementById('et_dd_custom_menu_settings').disabled)) || !this.checked);" >
            </div>
        </div>

<!-- Image settings -->
        <div class="control-group">
            <label for="et_dd_image_width" class="control-label">
                <?php echo $_smarty_tpl->__("et_dd_image_width");?>
:
            </label>
            <div class="controls">
                <input type="text" id="et_dd_image_width" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['array_name']->value, ENT_QUOTES, 'ISO-8859-1');?>
[et_dd_image_width]" size="10" maxlength="4" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['et_dd_image']->value['image_width'])===null||$tmp==='' ? "0" : $tmp), ENT_QUOTES, 'ISO-8859-1');?>
" class="input-micro" <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['all_disabled']->value, ENT_QUOTES, 'ISO-8859-1');?>
>
                <span> &nbsp; px <?php if ($_smarty_tpl->tpl_vars['current_image_width']->value) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['current_image_width']->value, ENT_QUOTES, 'ISO-8859-1');
}?></span>
            </div>
        </div>
        <div class="control-group">
            <label for="image_height" class="control-label">
                <?php echo $_smarty_tpl->__("et_dd_image_height");?>
:
            </label>
            <div class="controls">
                <input type="text" id="et_dd_image_height" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['array_name']->value, ENT_QUOTES, 'ISO-8859-1');?>
[et_dd_image_height]" size="10" maxlength="4" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['et_dd_image']->value['image_height'])===null||$tmp==='' ? "0" : $tmp), ENT_QUOTES, 'ISO-8859-1');?>
" class="input-micro" <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['all_disabled']->value, ENT_QUOTES, 'ISO-8859-1');?>
>
                <span> &nbsp; px <?php if ($_smarty_tpl->tpl_vars['current_image_height']->value) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['current_image_height']->value, ENT_QUOTES, 'ISO-8859-1');
}?></span>
            </div>
        </div>

<!-- URL -->
        <div class="control-group">
            <label for="et_dd_url" class="control-label">
                <?php echo $_smarty_tpl->__("et_dd_url");?>
:
            </label>
            <div class="controls">
                <input type="text" id="et_dd_url" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['array_name']->value, ENT_QUOTES, 'ISO-8859-1');?>
[et_dd_url]" size="10" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['et_dd_image']->value['url'])===null||$tmp==='' ? '' : $tmp), ENT_QUOTES, 'ISO-8859-1');?>
" class="input-large" <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['all_disabled']->value, ENT_QUOTES, 'ISO-8859-1');?>
>
            </div>
        </div>

<!-- Custom menu settings -->
        <div class="control-group">
            <label class="control-label" for="et_dd_custom_menu_settings">
                <?php echo $_smarty_tpl->__("et_dd_custom_menu_settings");?>
:
            </label>
            <div class="controls">
                <input type="hidden" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['array_name']->value, ENT_QUOTES, 'ISO-8859-1');?>
[et_dd_custom_menu_settings]" value="N" />
                <input type="checkbox" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['array_name']->value, ENT_QUOTES, 'ISO-8859-1');?>
[et_dd_custom_menu_settings]" id="et_dd_custom_menu_settings" value="Y" <?php if ($_smarty_tpl->tpl_vars['et_dd_image']->value['custom_menu_settings']=="Y") {?>checked="checked"<?php }?> onclick="Tygh.$.disable_elms(['et_dd_offset_right','et_dd_offset_bottom','et_dd_menu_width','et_dd_menu_min_height'], !this.checked); " <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['all_disabled']->value, ENT_QUOTES, 'ISO-8859-1');?>
>
            </div>
        </div>

<!-- Offset Image settings -->
        <div class="control-group">
            <label for="et_dd_offset_right" class="control-label">
                <?php echo $_smarty_tpl->__("et_dd_offset_right");?>
:
            </label>
            <div class="controls">
                <input type="text" id="et_dd_offset_right" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['array_name']->value, ENT_QUOTES, 'ISO-8859-1');?>
[et_dd_offset_right]" size="10" maxlength="4" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['et_dd_image']->value['offset_right'])===null||$tmp==='' ? "0" : $tmp), ENT_QUOTES, 'ISO-8859-1');?>
" class="input-micro" <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['all_disabled']->value, ENT_QUOTES, 'ISO-8859-1');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['menu_disabled']->value, ENT_QUOTES, 'ISO-8859-1');?>
>
                <span> &nbsp; px</span>
            </div>
        </div>
        <div class="control-group">
            <label for="image_height" class="control-label">
                <?php echo $_smarty_tpl->__("et_dd_offset_bottom");?>
:
            </label>
            <div class="controls">
                <input type="text" id="et_dd_offset_bottom" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['array_name']->value, ENT_QUOTES, 'ISO-8859-1');?>
[et_dd_offset_bottom]" size="10" maxlength="4" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['et_dd_image']->value['offset_bottom'])===null||$tmp==='' ? "0" : $tmp), ENT_QUOTES, 'ISO-8859-1');?>
" class="input-micro" <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['all_disabled']->value, ENT_QUOTES, 'ISO-8859-1');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['menu_disabled']->value, ENT_QUOTES, 'ISO-8859-1');?>
>
                <span> &nbsp; px</span>
            </div>
        </div>

<!-- Menu width settings -->
        <div class="control-group">
            <label for="et_dd_menu_width" class="control-label">
                <?php echo $_smarty_tpl->__("et_dd_menu_width");?>
:
            </label>
            <div class="controls">
                <input type="text" id="et_dd_menu_width" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['array_name']->value, ENT_QUOTES, 'ISO-8859-1');?>
[et_dd_menu_width]" size="10" maxlength="4" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['et_dd_image']->value['menu_width'])===null||$tmp==='' ? "0" : $tmp), ENT_QUOTES, 'ISO-8859-1');?>
" class="input-micro" <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['all_disabled']->value, ENT_QUOTES, 'ISO-8859-1');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['menu_disabled']->value, ENT_QUOTES, 'ISO-8859-1');?>
>
                <span> &nbsp; px</span>
            </div>
        </div>
        
<!-- Menu min height settings -->
        <div class="control-group">
            <label for="et_dd_menu_min_height" class="control-label">
                <?php echo $_smarty_tpl->__("et_dd_min_menu_height");?>
:
            </label>
            <div class="controls">
                <input type="text" id="et_dd_menu_min_height" name="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['array_name']->value, ENT_QUOTES, 'ISO-8859-1');?>
[et_dd_min_menu_height]" size="10" maxlength="4" value="<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['et_dd_image']->value['menu_min_height'])===null||$tmp==='' ? "0" : $tmp), ENT_QUOTES, 'ISO-8859-1');?>
" class="input-micro" <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['all_disabled']->value, ENT_QUOTES, 'ISO-8859-1');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['menu_disabled']->value, ENT_QUOTES, 'ISO-8859-1');?>
>
                <span> &nbsp; px</span>
            </div>
        </div>
    </div>
</fieldset><?php }} ?>
