<?php /* Smarty version Smarty-3.1.21, created on 2015-09-01 16:16:10
         compiled from "/home/gbadmin/public_html/production/design/backend/templates/views/negotiations/manage.tpl" */ ?>
<?php /*%%SmartyHeaderCode:134173120455e56cdad4df15-36713099%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ba1fad4978b467c47a009f751de5d910ef4cd937' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/backend/templates/views/negotiations/manage.tpl',
      1 => 1441098918,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '134173120455e56cdad4df15-36713099',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'histories' => 0,
    'nego' => 0,
    'nego_id' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55e56cdae15d04_55564613',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55e56cdae15d04_55564613')) {function content_55e56cdae15d04_55564613($_smarty_tpl) {?><?php $_smarty_tpl->_capture_stack[0][] = array("mainbox", null, null); ob_start(); ?>

    <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

    <table class="table table-middle table-striped table-hover" style="font-size: 12px;">
        <thead>
            <tr style="background-color: #e5e5e5">
                <th width="10%">Tanggal</th>
                <th width="15%">Customer</th>
                <th width="20%">Produk</th>
                <th width="10%">Harga</th>
                <th width="10%">Nego</th>
                <th width="5%">Status</th>
                <th width="10%"></th>
            </tr>
        </thead>
        <tbody>
            <?php if (count($_smarty_tpl->tpl_vars['histories']->value)) {?>
                <?php  $_smarty_tpl->tpl_vars["nego"] = new Smarty_Variable; $_smarty_tpl->tpl_vars["nego"]->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['histories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars["nego"]->key => $_smarty_tpl->tpl_vars["nego"]->value) {
$_smarty_tpl->tpl_vars["nego"]->_loop = true;
?>
                    <?php $_smarty_tpl->tpl_vars["product_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['nego']->value['product_id'], null, 0);?>
                    <?php $_smarty_tpl->tpl_vars["nego_id"] = new Smarty_variable($_smarty_tpl->tpl_vars['nego']->value['nego_id'], null, 0);?>
                    <tr>
                        <td><?php echo htmlspecialchars(date('d M Y',$_smarty_tpl->tpl_vars['nego']->value['timestamp']), ENT_QUOTES, 'ISO-8859-1');?>
</td>
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nego']->value['b_firstname'], ENT_QUOTES, 'ISO-8859-1');?>
</td>
                        <td><a href="<?php echo htmlspecialchars(fn_url("products.update&product_id=".((string)$_smarty_tpl->tpl_vars['product_id']->value)), ENT_QUOTES, 'ISO-8859-1');?>
" target="_blank"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nego']->value['product'], ENT_QUOTES, 'ISO-8859-1');?>
</a></td>
                        <td>Rp. <?php echo htmlspecialchars(number_format($_smarty_tpl->tpl_vars['nego']->value['price'],0,',','.'), ENT_QUOTES, 'ISO-8859-1');?>
</td>
                        <td>Rp. <?php echo htmlspecialchars(number_format($_smarty_tpl->tpl_vars['nego']->value['amount'],0,',','.'), ENT_QUOTES, 'ISO-8859-1');?>
</td>
                        <td><span class="label label-info"><?php if ($_smarty_tpl->tpl_vars['nego']->value['status']==0) {?> Menunggu <?php } elseif ($_smarty_tpl->tpl_vars['nego']->value['status']==1) {?> Diterima <?php } else { ?> Ditolak <?php }?></span></td>
                        <td style="text-align: right">
                            <a href="<?php echo htmlspecialchars(fn_url("negotiations.process&nego_id=".((string)$_smarty_tpl->tpl_vars['nego_id']->value)."&status=2"), ENT_QUOTES, 'ISO-8859-1');?>
" class="btn btn-danger btn-small <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nego']->value['status'] ? 'disabled' : '', ENT_QUOTES, 'ISO-8859-1');?>
">TOLAK</a>
                            <a href="<?php echo htmlspecialchars(fn_url("negotiations.process&nego_id=".((string)$_smarty_tpl->tpl_vars['nego_id']->value)."&status=1"), ENT_QUOTES, 'ISO-8859-1');?>
" class="btn btn-success btn-small <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['nego']->value['status'] ? 'disabled' : '', ENT_QUOTES, 'ISO-8859-1');?>
">TERIMA</a>
                        </td>
                    </tr>
                <?php } ?>
            <?php } else { ?>
                <tr>
                    <td colspan="7">Tidak ada sejarah tawar produk</td>
                </tr>
            <?php }?>
        </tbody>
    </table>
    <?php echo $_smarty_tpl->getSubTemplate ("common/pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>

<?php echo $_smarty_tpl->getSubTemplate ("common/mainbox.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('title'=>"Nego Harga",'content'=>Smarty::$_smarty_vars['capture']['mainbox'],'sidebar'=>Smarty::$_smarty_vars['capture']['sidebar'],'buttons'=>Smarty::$_smarty_vars['capture']['buttons'],'content_id'=>"manage_negotiations"), 0);?>
<?php }} ?>
