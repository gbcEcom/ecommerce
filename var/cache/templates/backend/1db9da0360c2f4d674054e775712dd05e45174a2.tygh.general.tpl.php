<?php /* Smarty version Smarty-3.1.21, created on 2015-09-15 09:21:14
         compiled from "/home/gbadmin/public_html/production/design/backend/templates/views/upgrade_center/components/general.tpl" */ ?>
<?php /*%%SmartyHeaderCode:201642820355f7809ace1e32-07168158%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1db9da0360c2f4d674054e775712dd05e45174a2' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/backend/templates/views/upgrade_center/components/general.tpl',
      1 => 1438219660,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '201642820355f7809ace1e32-07168158',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'validator_name' => 0,
    'data' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55f7809ad25754_77809863',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55f7809ad25754_77809863')) {function content_55f7809ad25754_77809863($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('upgrade_center.validation_issue','upgrade_center.validator_fail_result'));
?>
<table class="table table-condensed">
    <thead>
        <tr>
            <!-- upgrade-center_warning or upgrade-center_error -->
            <th class="upgrade-center_table-title upgrade-center_error">
                <h4><?php echo $_smarty_tpl->__("upgrade_center.validation_issue");?>
</h4>
                <p><?php echo $_smarty_tpl->__("upgrade_center.validator_fail_result",array("[validator_name]"=>$_smarty_tpl->tpl_vars['validator_name']->value));?>
</p>
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                <?php echo $_smarty_tpl->tpl_vars['data']->value;?>

            </td>
        </tr>
    </tbody>
</table><?php }} ?>
