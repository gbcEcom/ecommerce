<?php /* Smarty version Smarty-3.1.21, created on 2015-08-19 10:03:50
         compiled from "/home/gbadmin/public_html/production/design/backend/templates/addons/reward_points/hooks/order_management/totals_extra.pre.tpl" */ ?>
<?php /*%%SmartyHeaderCode:194529380755d3f2166151d7-02308300%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '16539dec21496a007fcfefd565fb057f3e87c1cc' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/backend/templates/addons/reward_points/hooks/order_management/totals_extra.pre.tpl',
      1 => 1438219648,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '194529380755d3f2166151d7-02308300',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'cart' => 0,
    'user_points' => 0,
    'user_info' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55d3f216645fd5_06604975',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55d3f216645fd5_06604975')) {function content_55d3f216645fd5_06604975($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('points_to_use','available','maximum'));
?>
<?php if ($_smarty_tpl->tpl_vars['cart']->value['points_info']['total_price']&&$_smarty_tpl->tpl_vars['user_points']->value) {?>
<div class="control-group">
    <label for="points_to_use" class="control-label"><?php echo $_smarty_tpl->__("points_to_use");?>
:</label>
    <div class="controls">
        <input type="text" name="points_to_use" id="points_to_use" size="20" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart']->value['points_info']['in_use']['points'], ENT_QUOTES, 'ISO-8859-1');?>
" />
        <p class="help-block">(<?php echo $_smarty_tpl->__("available");?>
:&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['user_info']->value['points'], ENT_QUOTES, 'ISO-8859-1');?>
&nbsp;<?php echo htmlspecialchars((($tmp = @$_smarty_tpl->tpl_vars['user_points']->value)===null||$tmp==='' ? "0" : $tmp), ENT_QUOTES, 'ISO-8859-1');?>
&nbsp;/&nbsp;<?php echo $_smarty_tpl->__("maximum");?>
:&nbsp;<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cart']->value['points_info']['total_price'], ENT_QUOTES, 'ISO-8859-1');?>
)</p>
    </div>
</div>
<?php }?><?php }} ?>
