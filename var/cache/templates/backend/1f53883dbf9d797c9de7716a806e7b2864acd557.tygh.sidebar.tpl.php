<?php /* Smarty version Smarty-3.1.21, created on 2015-08-12 10:23:33
         compiled from "/home/gbadmin/public_html/production/design/backend/templates/views/messages/components/sidebar.tpl" */ ?>
<?php /*%%SmartyHeaderCode:184947537555cabc35e07780-51744198%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1f53883dbf9d797c9de7716a806e7b2864acd557' => 
    array (
      0 => '/home/gbadmin/public_html/production/design/backend/templates/views/messages/components/sidebar.tpl',
      1 => 1438219662,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '184947537555cabc35e07780-51744198',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'count' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_55cabc35e15023_65590160',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55cabc35e15023_65590160')) {function content_55cabc35e15023_65590160($_smarty_tpl) {?><div class="sidebar-row" id="views">
    <h6>Menu</h6>
    <ul class="nav nav-list nav-messages">
        <li><a href="<?php echo htmlspecialchars(fn_url("messages.inbox"), ENT_QUOTES, 'ISO-8859-1');?>
"><i class="icon-inbox"></i>Inbox <small class="label label-info"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['count']->value, ENT_QUOTES, 'ISO-8859-1');?>
</small></a></li>
        <li><a href="<?php echo htmlspecialchars(fn_url("messages.sent"), ENT_QUOTES, 'ISO-8859-1');?>
"><i class="icon-envelope"></i>Sent</a></li>
        <li><a href="#"><i class="icon-remove"></i>Trash</a></li>
    </ul>
</div><?php }} ?>
