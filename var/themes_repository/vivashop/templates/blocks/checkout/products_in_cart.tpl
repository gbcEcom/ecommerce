<div id="checkout_info_products_{$block.snapping_id}">
    <ul class="order-product-list">
    {hook name="block_checkout:cart_items"}
    {foreach from=$cart_products key="key" item="product" name="cart_products"}
        {hook name="block_checkout:cart_products"}
            {if !$cart.products.$key.extra.parent}
            <li class="clearfix">
                <a href="{"products.view?product_id=`$product.product_id`"|fn_url}" class="product-image animate scale ty-float-left ty-order-products__a">
                {include file="common/image.tpl" images=$product.main_pair image_width="67" image_height="67" class="primary active"}
                </a>

                <div class="ty-float-left vs-order-product-details">
                <div class="vs-order-product-name"><a href="{"products.view?product_id=`$product.product_id`"|fn_url}" class="product-name">{$product.product nofilter}</a>{if !$product.exclude_from_calculate}{include file="buttons/button.tpl" but_href="checkout.delete?cart_id=`$key`&redirect_mode=`$runtime.mode`" but_meta="ty-order-products__item-delete delete" but_target_id="cart_status*" but_role="delete" but_name="delete_cart_item"}{/if}</div>

                <div class="ty-order-products__price">
                    {$product.amount}&nbsp;x&nbsp;{include file="common/price.tpl" value=$product.display_price}
                </div>
                {include file="common/options_info.tpl" product_options=$product.product_options no_block=true}
                {hook name="block_checkout:product_extra"}{/hook}
                </div>
            </li>
            {/if}
        {/hook}
    {/foreach}
    {/hook}
    </ul>
<!--checkout_info_products_{$block.snapping_id}--></div>