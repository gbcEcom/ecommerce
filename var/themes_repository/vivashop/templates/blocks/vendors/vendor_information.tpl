{** block-description:block_vendor_information **}

<div class="ty-vendor-information">
    <a class="vs_vendor_link" href="{"companies.view?company_id=`$vendor_info.company_id`"|fn_url}">{$vendor_info.company}</a><br/>
    <span>{$vendor_info.company_description nofilter}</span>
</div>