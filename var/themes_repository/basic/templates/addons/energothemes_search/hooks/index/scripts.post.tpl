{script src="js/addons/energothemes_search/jquery.customSelect.min.js"}

<script type="text/javascript">
//<![CDATA[
(function(_, $) {
    $.ceEvent('on', 'ce.commoninit', function(context) {
    	if ($('.energo-searchbox').data("rendered") != true){
    		$('.energo-searchbox').data("rendered",true);
	    	$('.energo-searchbox').customSelect({
	    		customClass:'energo-searchbox-span'
	    	});
	    	$('.energo-searchbox-span').append('<i class="icon-right-open"></i>');
	    }
    });
}(Tygh, Tygh.$));
//]]>
</script>
