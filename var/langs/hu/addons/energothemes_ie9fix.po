msgid ""
msgstr "Project-Id-Version: cs-cart-latest\n"
"Language-Team: English\n"
"Language: en_US\n"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: crowdin.net\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Last-Translator: energothemes <office@energothemes.com>\n"
"PO-Revision-Date: 2014-07-02 01:53-0400\n"

msgctxt "Addons::name::energothemes_ie9fix"
msgid "EnergoThemes - IE 9 FIX"
msgstr "EnergoThemes - IE 9 FIX"

msgctxt "Addons::description::energothemes_ie9fix"
msgid "EnergoThemes IE 9 CSS fix that bypases the 4095 rules limit (http://support.microsoft.com/kb/262161). It also comes with the browser-update.org script notifying IE 9 and below users to upgrade."
msgstr "EnergoThemes IE 9 CSS fix that bypases the 4095 rules limit (http://support.microsoft.com/kb/262161). It also comes with the browser-update.org script notifying IE 9 and below users to upgrade."

